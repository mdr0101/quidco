TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_freemium_hierarchy;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_freemium_hierarchy (freemium_hierarchy_id
                                                              ,di_domain_id
                                                              ,freemium_type_l1_id
                                                              ,freemium_type_l1_name
                                                              ,freemium_type_l2_id
                                                              ,freemium_type_l2_name
                                                              ,freemium_type_l3_id
                                                              ,freemium_type_l3_name
                                                              ,user_type)
 VALUES ( -1, -1, -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 ,   -1)
       ,(  1,  1,  1, 'System' ,  1, 'System'        ,  1, 'System'                  ,    2)
       ,(  2,  1,  2, 'Quidco' ,  2, 'Quidco Basic'  ,  2, 'Quidco Basic Unconfirmed',    3)
       ,(  3,  1,  2, 'Quidco' ,  2, 'Quidco Basic'  ,  3, 'Quidco Basic Confirmed'  ,    4)
       ,(  4,  1,  2, 'Quidco' ,  3, 'Quidco Premium',  4, 'Quidco Premium'          ,    5)
 ;
