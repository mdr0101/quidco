TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_mapping_rules;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_mapping_rules (rule_id
                                                         ,di_domain_id
                                                         ,table_name
                                                         ,column_name
                                                         ,mapping_rule
                                                         ,value_int
                                                         ,value_string
                                                         ,value_datetime
                                                         ,description)
 VALUES (   1,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_STATUS'           ,  NULL, 'denied'           , NULL        , 'Transaction with this status is not valid. (1: pending, 2:added, 3:validated, 4:received, 5:denied, 6:paid, 7:claimed)')
       ,(   2,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'SPEND_MAX_LIMIT'          , 50000, NULL               , NULL        , 'Transaction amount above this value is not valid'                                                                      )
       ,(   3,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'COMMISSION_MAX_LIMIT'     , 50000, NULL               , NULL        , 'Commission above this value is not valid transaction'                                                                  )
       ,(   4,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'USER_COMMISSION_MAX_LIMIT', 50000, NULL               , NULL        , 'User commission above this value is not valid transaction'                                                             )
       ,(   5,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_NETWORK'          ,  NULL, 'QCO'              , NULL        , 'This network is invalid'                                                                                               )
       ,(   6,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_NETWORK_EXCEPTION',  NULL, '7969, 7975, 7976' , NULL        , '(Ocado shosp) This merchant is valid despite the INVALID NETWORK rules'                                                )
       ,(   7,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'MIN_USER_ID'              ,    63, NULL               , NULL        , 'User ID below this one has no valid transaction'                                                                       ) 
       ,(   8,  1, 'DI_WRK_TRANSACTION', 'IS_PAYPAL_BONUS'     , 'MERCHANT_ID'              , 10145, NULL               , NULL        , 'Purchase with this merchant is marked as PayPal bonus purchase. Some reports need to exclude these purchases.'         )
       ,(   9,  2, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_STATUS'           ,  NULL, 'denied'           , NULL        , 'Purchase with this merchant is marked as PayPal bonus purchase. Some reports need to exclude these purchases.'         )
       ,(  10,  2, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'SPEND_MAX_LIMIT'          , 10000, NULL               , NULL        , 'Transaction amount above this value is not valid'                                                                      )
       ,(  11,  2, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'COMMISSION_MAX_LIMIT'     , 10000, NULL               , NULL        , 'Commission above this value is not valid transaction'                                                                  )
       ,(  12,  2, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'USER_COMMISSION_MAX_LIMIT', 10000, NULL               , NULL        , 'User commission above this value is not valid transaction'                                                             )
	   
	   ,(  13,  1, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'SPEND_MAX_LIMIT'			, 50000, NULL               , NULL        , 'Transaction amount above this value is outlier transaction'                                                             )
	   ,(  14,  1, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'COMMISSION_MAX_LIMIT'     , 50000, NULL               , NULL        , 'Commission above this value is outlier transaction'                                                                    )
       ,(  15,  1, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'USER_COMMISSION_MAX_LIMIT', 50000, NULL               , NULL        , 'User commission above this value is outlier transaction'                                                               )       
	   
	   ,(  16,  2, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'SPEND_MAX_LIMIT'			, 50000, NULL               , NULL        , 'Transaction amount above this value is outlier transaction'                                                            )
	   ,(  17,  2, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'COMMISSION_MAX_LIMIT'     , 50000, NULL               , NULL        , 'Commission above this value is outlier transaction'                                                                    )
       ,(  18,  2, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'USER_COMMISSION_MAX_LIMIT', 50000, NULL               , NULL        , 'User commission above this value is outlier transaction'                                                               )       

       ,(  19,  1, 'DI_DIM_MERCHANT'   , 'IS_INSTORE'          , 'INSTORE_PATTERN'          ,  NULL, 'in-store'         , NULL        , 'String pattern in the merchant name to identify if it is instore merchant'                                             )
       ,(  20,  1, 'DI_DIM_MERCHANT'   , 'IS_INSTORE'          , 'RECEIPT_UPLOAD_PATTERN'   ,  NULL, 'receipt upload'   , NULL        , 'String pattern in the merchant name to identify if it is instore (receipt-upload) merchant'                            )
       ,(  21, -1, 'DI_DIM_MERCHANT'   , 'INSTORE_TYPE_NAME'   , 'INSTORE'                  ,  NULL, 'In-Store'         , NULL        , 'Name of Instore type merchants'                                                                                        )
       ,(  22, -1, 'DI_DIM_MERCHANT'   , 'INSTORE_TYPE_NAME'   , 'RECEIPT_UPLOAD'           ,  NULL, 'Receipt Upload'   , NULL        , 'Name of Receipt Upload type merchants'                                                                                 )
       ,(  23, -1, 'DI_DIM_MERCHANT'   , 'INSTORE_TYPE_NAME'   , 'NOT_INSTORE'              ,  NULL, 'Not In-Store'     , NULL        , 'Name of Not Instore merchants'                                                                                         )

       ,(  24,  1, 'DI_DIM_USER_HIST'  , 'SIGNUP_METHOD_NAME'  , 'QUIDCO'                   ,     1, 'Quidco'           , NULL        , 'Signup method mapping - via normal Quidco join page'                                                                   )
       ,(  25,  1, 'DI_DIM_USER_HIST'  , 'SIGNUP_METHOD_NAME'  , 'FACEBOOK'                 ,     2, 'Facebook'         , NULL        , 'Signup method mapping - via Facebook'                                                                                  )
       ,(  26,  1, 'DI_DIM_USER_HIST'  , 'SIGNUP_METHOD_NAME'  , 'PAYPAL'                   ,     3, 'PayPal'           , NULL        , 'Signup method mapping - via PayPal'                                                                                    )

       ,(  27,  1, 'DI_FACT_CLICK'     , 'IS_VALID'            , 'MERCHANT_ID'              ,    -1, NULL               , NULL        , 'Quidco clicks are not valid if Merchant is Unknown'                                     						                    )
       ,(  28,  1, 'DI_FACT_CLICK'     , 'IS_VALID'            , 'PARENT_TYPE'   			      ,  NULL, 'mobile-checking-transaction', NULL, 'Quidco clicks are not valid if the parent_type is mobile-checking-transaction'          )

       ,(  29,  1, 'DI_WRK_TRANSACTION', 'SPEND_IMPUTED'       , 'SPEND_IMPUTED_CUTOFF_DATE',  NULL, NULL               , '2015-07-01', 'Start date when the SPEND_IMPUTED_SQL logic needs to be applied on the transactions'                                   )

       ,(  30,  1, 'DI_WRK_TRANSACTION', 'SPEND_IMPUTED'       , 'SPEND_IMPUTED_SQL'        ,  NULL, 'SELECT y.transaction_id
      ,y.amount
      ,CASE
         WHEN y.spend_imputed > 99999999.99 THEN 99999999.99
         ELSE y.spend_imputed
       END spend_imputed
      ,y.is_noncashback
  FROM (SELECT x.transaction_id, x.amount

      -- -----------------------------------------------------------------------
      -- VAT correction after 2013
      -- -----------------------------------------------------------------------
      ,CASE
         WHEN (x.vat_lookup_id IS     NULL AND spend_imputed  > 0)
           OR (x.vat_lookup_id IS NOT NULL AND amount         = 0)
           OR (x.vat_lookup_id IS NOT NULL AND merchant_l4_id = 123 AND x.amount <= 200)
              THEN spend_imputed * 1.027
              
         WHEN x.vat_lookup_id  IS NOT NULL THEN COALESCE(x.spend_imputed, x.amount)
         
         WHEN x.merchant_l4_id NOT IN (232, 229) AND x.spend_imputed = 0 THEN x.amount
         
         ELSE COALESCE(x.spend_imputed, x.amount)
         
       END spend_imputed
      ,x.is_noncashback
       
 FROM (SELECT stg.transaction_id
             ,stg.amount
             ,m.merchant_hierarchy_l4_id merchant_l4_id
             ,si_merchant_vat.lookup_id  vat_lookup_id
             ,CASE
      				  WHEN network = ''QCO'' THEN 0 -- mobile check in
      				  WHEN network = ''PP'' AND merchant_id = 10145 THEN 0 -- PayPal bonus (10145)

        				WHEN si_merchant.lookup_id IS NOT NULL THEN si_merchant.spend_imputed -- STEP 5 and 6

        				WHEN stg.amount  = 0  AND si_merchant_hierarchy.lookup_id IS NOT NULL THEN si_merchant_hierarchy.spend_imputed -- STEP 1 and STEP 4
        				WHEN stg.amount <= 20 AND m.merchant_hierarchy_l4_id = 123 THEN 180.00 -- Christopher Catchpole Special
        				WHEN m.merchant_hierarchy_l4_id = 229 THEN 0 -- STEP 2 (Car Insurance Comparison)
        				WHEN m.merchant_hierarchy_l4_id IN (167, 268) AND stg.commission  > 25   AND stg.commission <= 50  THEN 480 -- STEP 3
        				WHEN m.merchant_hierarchy_l4_id IN (167, 268) AND stg.commission  > 50   AND stg.commission <= 100 THEN 600 -- STEP 3
        				WHEN m.merchant_hierarchy_l4_id IN (167, 268) AND stg.commission  > 100                            THEN 720 -- STEP 3
        				WHEN m.merchant_hierarchy_l4_id IN (167, 268) AND stg.commission  > 5    AND stg.commission <= 25  THEN 180 -- STEP 3
        				WHEN m.merchant_hierarchy_l4_id IN (167, 268) AND stg.commission <= 5                              THEN 120 -- STEP 3

                -- ------------------------------------------------------------------
                -- Note: there are no merchants in category 191 any more, so the next
                -- two lines don''t do anything
                -- ------------------------------------------------------------------
        				WHEN m.merchant_hierarchy_l4_id = 191 AND stg.commission <= 5 THEN 120 -- STEP 3
        				WHEN m.merchant_hierarchy_l4_id = 191 AND stg.commission  > 5 THEN 180 -- STEP 3
        				 
                -- ------------------------------------------------------------------
                -- Special rule for Utility Comparison (was previously in step 2
                -- above, but that logic has been removed) */
                -- ------------------------------------------------------------------
        				WHEN m.merchant_hierarchy_l4_id = 232 AND stg.commission >= 10 AND stg.commission <= 20 THEN 1265 -- single fuel 
        				WHEN m.merchant_hierarchy_l4_id = 232 AND stg.commission >  20                          THEN 2530 -- dual fuel   
                
                -- ------------------------------------------------------------------
                -- Merchant VAT corrections
                -- ------------------------------------------------------------------
        				WHEN si_merchant_vat.lookup_id IS NOT NULL THEN (stg.amount * 1.20)
                   
        			END AS spend_imputed
             ,stg.is_noncashback
         FROM (SELECT transaction_id
                     ,amount
                     ,network
                     ,merchant_id
                     ,date
                     ,commission
                     ,0 is_noncashback
                 FROM di_staging.stg_quidco_com_qco_transbeta_states
                WHERE transaction_state_id IN (SELECT max(transaction_state_id)
                                                 FROM di_staging.stg_quidco_com_qco_transbeta_states
                                                GROUP BY transaction_id)
                UNION
                SELECT noncashback_transaction_id transaction_id
                      ,amount
                      ,network
                      ,merchant_id
                      ,date
                      ,0 commission
                      ,1 is_noncashback
                  FROM di_staging.stg_quidco_com_qco_noncashback_transactions) stg
          
              -- ----------------------------------------------------------------------
              -- Join to DI_DIM_MERCHANT to get the hierarchy l4 id
              -- ----------------------------------------------------------------------
              LEFT JOIN di_warehouse.di_dim_merchant m
                     ON m.di_domain_id = 1
                    AND m.domain_merchant_id = stg.merchant_id
                    
              -- ----------------------------------------------------------------------
              -- Get merchant based rules
              -- ----------------------------------------------------------------------
              LEFT JOIN di_warehouse.di_cfg_mapping_rules_spend_imputed si_merchant
                     ON si_merchant.di_domain_id = 1
                    AND si_merchant.rule_category = ''MERCHANT''
                    AND si_merchant.lookup_id = stg.merchant_id
                    AND si_merchant.month = MONTH(date)
                    
              -- ----------------------------------------------------------------------
              -- Get merchant hierarchy l4 based rules
              -- ----------------------------------------------------------------------
              LEFT JOIN di_warehouse.di_cfg_mapping_rules_spend_imputed si_merchant_hierarchy
                     ON si_merchant_hierarchy.di_domain_id = 1
                    AND si_merchant_hierarchy.rule_category = ''MERCHANT_HIERARCHY_L4''
                    AND si_merchant_hierarchy.lookup_id = (SELECT merchant_hierarchy_l4_id
                                                             FROM di_warehouse.di_cfg_merchant_hierarchy_mapping
                                                            WHERE di_domain_id = 1
                                                              AND domain_merchant_id = stg.merchant_id)
                    AND si_merchant_hierarchy.month = MONTH(date)
                    
               -- ----------------------------------------------------------------------
               -- Get merchant VAT based rules
               -- ----------------------------------------------------------------------
               LEFT JOIN di_warehouse.di_cfg_mapping_rules_spend_imputed si_merchant_vat
                      ON si_merchant_vat.di_domain_id = 1
                     AND si_merchant_vat.rule_category = ''MERCHANT_VAT''
                     AND si_merchant_vat.lookup_id = stg.merchant_id
 ) x) y;'
 , NULL, 'Custom SQL to calculate transaction spend imputed values')
        ,(  31,  1, 'DI_WRK_USER_SEGMENT_LIFECYCLE', 'LIFECYCLE_STATUS'       , 'LIFECYCLE_STATUS_SQL'        ,  NULL, 'SELECT di_domain_id
      ,domain_user_id
      ,CASE
        WHEN DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) < 14 then 1
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d''))) <=89 AND (valid_purchases=0) then 2
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 90) AND (valid_purchases=0) then 3
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (last_valid_purchase_date_id >= DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then 4
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (last_valid_purchase_date_id < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then 5
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id >= DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then 6
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then 7
        END as ''lifecycle_status_id''
      ,CASE
        WHEN DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) < 14 then ''Welcome''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d''))) <=89 AND (valid_purchases=0) then ''Lead''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 90) AND (valid_purchases=0) then ''Lapsed - No Purchases''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (last_valid_purchase_date_id >= DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then ''Engage''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (last_valid_purchase_date_id < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then ''Engage - Lapsed''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id >= DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then ''Retain''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then ''Lapsed - Purchases''
        END as ''lifecycle_status_name''
      ,purchases
FROM di_wrk_user_main
WHERE di_domain_id = 1;'
 , NULL, 'Custom SQL to calculate Lifecycle statuses (Quidco.com)')
        ,(  32,  2, 'DI_WRK_USER_SEGMENT_LIFECYCLE', 'LIFECYCLE_STATUS'       , 'LIFECYCLE_STATUS_SQL'        ,  NULL, 'SELECT di_domain_id
      ,domain_user_id
      ,CASE
        WHEN DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) < 14 then 1
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d''))) <=89 AND (valid_purchases=0) then 2
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 90) AND (valid_purchases=0) then 3
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(last_valid_purchase_date_id, ''%Y/%m/%d'')) < 365) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then 4
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(last_valid_purchase_date_id, ''%Y/%m/%d'')) >= 365) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then 5
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id >= DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then 6
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then 7
        END as ''lifecycle_status_id''
      ,CASE
        WHEN DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) < 14 then ''Welcome''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d''))) <=89 AND (valid_purchases=0) then ''Lead''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 90) AND (valid_purchases=0) then ''Lapsed - No Purchases''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(last_valid_purchase_date_id, ''%Y/%m/%d'')) < 365) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then ''Engage''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (DATEDIFF(current_date, DATE_FORMAT(last_valid_purchase_date_id, ''%Y/%m/%d'')) >= 365) AND ((valid_purchases = 1) OR (valid_purchases=2) OR (valid_purchases=3))then ''Engage - Lapsed''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id >= DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then ''Retain''
        WHEN (DATEDIFF(current_date, DATE_FORMAT(registration_date_id, ''%Y/%m/%d'')) >= 14) AND (valid_purchases > 3) AND (last_valid_purchase_date_id < DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR), ''%Y%m%d'')) then ''Lapsed - Purchases''
        END as ''lifecycle_status_name''
      ,purchases
FROM di_wrk_user_main
WHERE di_domain_id = 2;'
 , NULL, 'Custom SQL to calculate Lifecycle statuses (Qipu.de)')
       ,(   33,  1, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'EXCEPTION_VISA_USER'           ,  22, NULL           , NULL        , 'Transations for user_id = 22 is Valid (22 is VISA user)')

	     ,(   34,  1, 'DI_WRK_USER_SEGMENT_RFV', 'RFV_GROUP'      , 'RFV_SEGMENT_SQL'               ,  NULL, 'SELECT domain_user_id
      ,rfv_group_id
      ,CASE rfv_group_id
         WHEN 50 THEN ''Welcome and Engage''
         WHEN 60 THEN ''Retain and Develop''
         WHEN 70 THEN ''Recovery and Churn''
         ELSE ''Unknown''
       END rfv_group_name
      ,rfv_subgroup_id
      ,CASE rfv_subgroup_id
         WHEN 51 THEN ''Receding, not purchased''
         WHEN 52 THEN ''Inbetweener, not purchased''
         WHEN 53 THEN ''Looking and Listening, not purchased''
         WHEN 54 THEN ''Receding, purchased''
         WHEN 55 THEN ''Inbetweener, purchased''
         WHEN 56 THEN ''Looking and Listening, purchased''
         WHEN 61 THEN ''Devoted''
         WHEN 62 THEN ''Regular''
         WHEN 63 THEN ''Big Ticket''
         WHEN 64 THEN ''Standard''
         WHEN 65 THEN ''Casual''
         WHEN 66 THEN ''Fading''
         WHEN 67 THEN ''Just Starting''
         WHEN 71 THEN ''Hero to Zero''
         WHEN 72 THEN ''Perished''
         WHEN 73 THEN ''Passing Through''
         WHEN 74 THEN ''Not Lost Yet''
         WHEN 75 THEN ''Register and Forget''
         WHEN 76 THEN ''What''''s Quidco?''
         ELSE ''Unknown''
       END rfv_subgroup_name
  FROM (SELECT domain_user_id
              ,rfv_group_id_old
              ,rfv_group_name_old
              ,rfv_subgroup_id_old
              ,rfv_subgroup_name_old
              ,rfv_group_id
              ,CASE rfv_group_id
                 WHEN 50 THEN
                   CASE
                     WHEN     3m_valid_purchases = 0 AND
                          (   (we_recency_bucket = 1 AND we_frequency_bucket = 1 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 1 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 2 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 3 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 1 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 2 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 2 AND we_value_bucket = 1 )) THEN 51
                     WHEN     3m_valid_purchases = 0 AND
                          (   (we_recency_bucket = 1 AND we_frequency_bucket = 1 AND we_value_bucket = 3 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 1 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 1 AND we_value_bucket = 3 )
                           OR (we_recency_bucket = 3 AND we_frequency_bucket = 1 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 3 AND we_frequency_bucket = 1 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 3 AND we_frequency_bucket = 1 AND we_value_bucket = 3 )) THEN 52
                     WHEN     3m_valid_purchases = 0                                                        THEN 53
                     WHEN     3m_valid_purchases > 0 AND
                          (   (we_recency_bucket = 1 AND we_frequency_bucket = 1 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 1 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 2 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 3 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 1 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 2 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 1 AND we_frequency_bucket = 2 AND we_value_bucket = 1 )) THEN 54
                     WHEN     3m_valid_purchases > 0 AND
                          (   (we_recency_bucket = 1 AND we_frequency_bucket = 1 AND we_value_bucket = 3 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 1 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 2 AND we_frequency_bucket = 1 AND we_value_bucket = 3 )
                           OR (we_recency_bucket = 3 AND we_frequency_bucket = 1 AND we_value_bucket = 1 )
                           OR (we_recency_bucket = 3 AND we_frequency_bucket = 1 AND we_value_bucket = 2 )
                           OR (we_recency_bucket = 3 AND we_frequency_bucket = 1 AND we_value_bucket = 3 )) THEN 55
                     WHEN     3m_valid_purchases > 0                                                        THEN 56

                     ELSE 54
                   END
                 WHEN 60 THEN
                   CASE
                     WHEN    (rd_recency_bucket = 1 AND rd_frequency_bucket = 1 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 1 AND rd_value_bucket = 1) 
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 1 AND rd_value_bucket = 1) THEN 61
                     WHEN    (rd_recency_bucket = 1 AND rd_frequency_bucket = 1 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 1 AND rd_value_bucket = 3) 
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 1 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 2 AND rd_value_bucket = 2) 
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 2 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 1 AND rd_value_bucket = 2) 
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 1 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 1 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 2 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 2 AND rd_value_bucket = 3) 
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 1 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 1 AND rd_value_bucket = 4) THEN 62
                     WHEN    (rd_recency_bucket = 1 AND rd_frequency_bucket = 2 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 3 AND rd_value_bucket = 1) 
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 3 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 4 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 2 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 3 AND rd_value_bucket = 1) 
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 3 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 4 AND rd_value_bucket = 1) THEN 63
                     WHEN    (rd_recency_bucket = 3 AND rd_frequency_bucket = 1 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 2 AND rd_value_bucket = 1) 
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 2 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 2 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 3 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 3 AND rd_value_bucket = 2) 
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 1 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 1 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 1 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 2 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 2 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 2 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 3 AND rd_value_bucket = 1)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 3 AND rd_value_bucket = 2) THEN 64
                     WHEN    (rd_recency_bucket = 1 AND rd_frequency_bucket = 2 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 3 AND rd_value_bucket = 3) 
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 3 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 4 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 4 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 1 AND rd_frequency_bucket = 4 AND rd_value_bucket = 4) 
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 2 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 3 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 3 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 4 AND rd_value_bucket = 2)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 4 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 2 AND rd_frequency_bucket = 4 AND rd_value_bucket = 4) THEN 65
                     WHEN    (rd_recency_bucket = 3 AND rd_frequency_bucket = 2 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 3 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 3 AND rd_value_bucket = 4) 
                          OR (rd_recency_bucket = 3 AND rd_frequency_bucket = 4 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 1 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 2 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 3 AND rd_value_bucket = 3)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 3 AND rd_value_bucket = 4)
                          OR (rd_recency_bucket = 4 AND rd_frequency_bucket = 4 AND rd_value_bucket = 4) THEN 66
                     ELSE 67
                   END
                 WHEN 70 THEN
                   CASE
                     WHEN    (rc_recency_bucket = 1 AND rc_frequency_bucket = 2 AND rc_value_bucket = 2)
                          OR (rc_recency_bucket = 1 AND rc_frequency_bucket = 2 AND rc_value_bucket = 3)
                          OR (rc_recency_bucket = 2 AND rc_frequency_bucket = 2 AND rc_value_bucket = 2) 
                          OR (rc_recency_bucket = 2 AND rc_frequency_bucket = 2 AND rc_value_bucket = 3)
                          OR (rc_recency_bucket = 3 AND rc_frequency_bucket = 2 AND rc_value_bucket = 2)
                          OR (rc_recency_bucket = 3 AND rc_frequency_bucket = 2 AND rc_value_bucket = 3)
                          OR (rc_recency_bucket = 4 AND rc_frequency_bucket = 2 AND rc_value_bucket = 2)
                          OR (rc_recency_bucket = 4 AND rc_frequency_bucket = 2 AND rc_value_bucket = 3) THEN 71
                     WHEN    (rc_recency_bucket = 1 AND rc_frequency_bucket = 1 AND rc_value_bucket = 2)
                          OR (rc_recency_bucket = 1 AND rc_frequency_bucket = 1 AND rc_value_bucket = 3)
                          OR (rc_recency_bucket = 2 AND rc_frequency_bucket = 1 AND rc_value_bucket = 2) 
                          OR (rc_recency_bucket = 2 AND rc_frequency_bucket = 1 AND rc_value_bucket = 3)
                          OR (rc_recency_bucket = 3 AND rc_frequency_bucket = 1 AND rc_value_bucket = 2)
                          OR (rc_recency_bucket = 3 AND rc_frequency_bucket = 1 AND rc_value_bucket = 3)
                          OR (rc_recency_bucket = 4 AND rc_frequency_bucket = 1 AND rc_value_bucket = 2)
                          OR (rc_recency_bucket = 4 AND rc_frequency_bucket = 1 AND rc_value_bucket = 3) THEN 72
                     WHEN    (rc_recency_bucket = 4 AND rc_frequency_bucket = 1 AND rc_value_bucket = 1) THEN 73
                     WHEN    (rc_recency_bucket = 1 AND rc_frequency_bucket = 2 AND rc_value_bucket = 1)
                          OR (rc_recency_bucket = 2 AND rc_frequency_bucket = 2 AND rc_value_bucket = 1)
                          OR (rc_recency_bucket = 3 AND rc_frequency_bucket = 2 AND rc_value_bucket = 1) 
                          OR (rc_recency_bucket = 4 AND rc_frequency_bucket = 2 AND rc_value_bucket = 1) THEN 74
                     WHEN    (rc_recency_bucket = 2 AND rc_frequency_bucket = 1 AND rc_value_bucket = 1)
                          OR (rc_recency_bucket = 3 AND rc_frequency_bucket = 1 AND rc_value_bucket = 1) THEN 75
                     ELSE 76
                   END
               END rfv_subgroup_id
               
               ,1y_valid_purchases
               ,1y_days_since_last_valid_purchase
               ,1y_avg_daily_valid_purchase
               ,1y_avg_daily_user_commission
               ,1y_avg_daily_spend_imputed
               ,3y_valid_purchases
               ,we_recency_bucket
               ,we_frequency_bucket
               ,we_value_bucket
               ,rd_recency_bucket
               ,rd_frequency_bucket
               ,rd_value_bucket
               ,rc_recency_bucket
               ,rc_frequency_bucket
               ,rc_value_bucket
               ,3y_avg_daily_click
               ,3y_logins
               ,3y_email_activity
          FROM (SELECT domain_user_id
                      ,rfv_group_id
                      ,3m_valid_purchases
                      ,1y_valid_purchases
                      ,1y_days_since_last_valid_purchase
                      ,1y_avg_daily_valid_purchase
                      ,1y_avg_daily_user_commission
                      ,1y_avg_daily_spend_imputed
                      ,3y_valid_purchases
                      ,3y_avg_daily_click
                      ,3y_logins
                      ,3y_email_activity
                      
                      -- --------------------------------------------------------------------------------------
                      -- W&E RFV bands
                      -- --------------------------------------------------------------------------------------
                      ,CASE
                         WHEN rfv_group_id = 50 AND 3m_days_since_last_login > 56 THEN 1
                         WHEN rfv_group_id = 50 AND 3m_days_since_last_login BETWEEN 25 AND 56 THEN 2
                         WHEN rfv_group_id = 50 AND 3m_days_since_last_login < 25 THEN 3
                       END we_recency_bucket
                      ,CASE
                         WHEN rfv_group_id = 50 AND 3m_email_activity > 16 THEN 3
                         WHEN rfv_group_id = 50 AND 3m_email_activity BETWEEN 6 AND 16 THEN 2
                         WHEN rfv_group_id = 50 AND 3m_email_activity < 6 THEN 1
                       END we_frequency_bucket
                      ,CASE
                         WHEN rfv_group_id = 50 AND 3m_avg_daily_login > 0.1529412 THEN 3
                         WHEN rfv_group_id = 50 AND 3m_avg_daily_login BETWEEN 0.0348837 AND 0.1529412 THEN 2
                         WHEN rfv_group_id = 50 AND 3m_avg_daily_login < 0.0348837 THEN 1
                       END we_value_bucket
                      
                      -- --------------------------------------------------------------------------------------
                      -- R&D RFV bands
                      -- --------------------------------------------------------------------------------------
                      ,CASE
                         WHEN rfv_group_id = 60 AND 1y_days_since_last_valid_purchase BETWEEN 1  AND  31 THEN 1
                         WHEN rfv_group_id = 60 AND 1y_days_since_last_valid_purchase BETWEEN 32 AND  91 THEN 2
                         WHEN rfv_group_id = 60 AND 1y_days_since_last_valid_purchase BETWEEN 92 AND 173 THEN 3
                         ELSE 4
                       END rd_recency_bucket
                      ,CASE
                         WHEN rfv_group_id = 60 AND 1y_avg_daily_valid_purchase > 0.0426136 THEN 1
                         WHEN rfv_group_id = 60 AND 1y_avg_daily_valid_purchase BETWEEN 0.0130293 AND 0.0426136 THEN 2
                         WHEN rfv_group_id = 60 AND 1y_avg_daily_valid_purchase BETWEEN 0.0046729 AND 0.0130293 THEN 3
                         ELSE 4
                       END rd_frequency_bucket
                      ,CASE LEAST(rd_value_fig_1, rd_value_fig_2, rd_value_fig_3, rd_value_fig_4)
                         WHEN rd_value_fig_1 THEN 1
                         WHEN rd_value_fig_2 THEN 2
                         WHEN rd_value_fig_3 THEN 3
                         ELSE 4
                       END rd_value_bucket
                      
                      -- --------------------------------------------------------------------------------------
                      -- R&C RFV bands
                      -- --------------------------------------------------------------------------------------
                      ,CASE
                         WHEN rfv_group_id = 70 AND LEAST(3y_days_since_last_valid_purchase
                                                         ,3y_days_since_last_click
                                                         ,3y_days_since_last_login
                                                         ,3y_days_since_last_email_activity)        > 637 THEN 1
                         WHEN rfv_group_id = 70 AND LEAST(3y_days_since_last_valid_purchase
                                                         ,3y_days_since_last_click
                                                         ,3y_days_since_last_login
                                                         ,3y_days_since_last_email_activity)        BETWEEN 322 AND 637 THEN 2
                         WHEN rfv_group_id = 70 AND LEAST(3y_days_since_last_valid_purchase
                                                         ,3y_days_since_last_click
                                                         ,3y_days_since_last_login
                                                         ,3y_days_since_last_email_activity)        BETWEEN 137 AND 321 THEN 3
                         ELSE 4
                       END rc_recency_bucket
                      ,CASE
                         WHEN rfv_group_id = 70 AND 3y_avg_daily_click < 0.0476443 THEN 1
                         ELSE 2
                       END rc_frequency_bucket
                      ,CASE
                         WHEN rfv_group_id = 70 AND 3y_valid_purchases < 5 THEN 1
                         WHEN rfv_group_id = 70 AND 3y_valid_purchases BETWEEN 5 AND 29 THEN 2
                         ELSE 3
                       END rc_value_bucket   
                       
                      ,rfv_group_id_old
                      ,rfv_group_name_old
                      ,rfv_subgroup_id_old
                      ,rfv_subgroup_name_old
                  FROM (SELECT u.rfv_group_id      rfv_group_id_old
                              ,u.rfv_group_name    rfv_group_name_old
                              ,u.rfv_subgroup_id   rfv_subgroup_id_old
                              ,u.rfv_subgroup_name rfv_subgroup_name_old
                              ,u.domain_user_id

                              -- ---------------------------------------------------------------------------------------
                              -- Define RFV groups
                              -- ---------------------------------------------------------------------------------------
                              ,CASE
                                 WHEN DATEDIFF(DATE_FORMAT(NOW(), ''%Y%m01''), u.registration_date_id) <= 90 THEN 50
                                 WHEN DATEDIFF(DATE_FORMAT(NOW(), ''%Y%m01''), u.registration_date_id)  > 90
                                      AND COALESCE(1y_valid_purchases, 0) > 0       THEN 60
                                 ELSE 70
                               END rfv_group_id
                               
                               -- ---------------------------------------------------------------------------------------
                               -- R&D statistical figures
                               -- ---------------------------------------------------------------------------------------
                              ,POWER((LN(1y_avg_daily_user_commission) - -0.6200973), 2) + POWER((LN(1y_avg_daily_spend_imputed) -  2.2164475), 2) rd_value_fig_1
                              ,POWER((LN(1y_avg_daily_user_commission) - -1.9160101), 2) + POWER((LN(1y_avg_daily_spend_imputed) -  0.7600694), 2) rd_value_fig_2
                              ,POWER((LN(1y_avg_daily_user_commission) - -3.6117930), 2) + POWER((LN(1y_avg_daily_spend_imputed) - -0.7918324), 2) rd_value_fig_3
                              ,POWER((LN(1y_avg_daily_user_commission) - -5.7561987), 2) + POWER((LN(1y_avg_daily_spend_imputed) - -2.5037102), 2) rd_value_fig_4
                              
                              -- ---------------------------------------------------------------------------------------
                              -- Convert every NULL to valid numbers
                              -- ---------------------------------------------------------------------------------------
                              ,COALESCE(3m_valid_purchases                ,     0) 3m_valid_purchases
                              ,COALESCE(3m_days_since_last_valid_purchase , 42457) 3m_days_since_last_valid_purchase
                              ,COALESCE(3m_avg_daily_valid_purchase       ,     0) 3m_avg_daily_valid_purchase
                              ,COALESCE(3m_avg_daily_user_commission      ,     0) 3m_avg_daily_user_commission
                              ,COALESCE(3m_avg_daily_spend_imputed        ,     0) 3m_avg_daily_spend_imputed
                              ,COALESCE(1y_valid_purchases                ,     0) 1y_valid_purchases
                              ,COALESCE(1y_days_since_last_valid_purchase , 42457) 1y_days_since_last_valid_purchase
                              ,COALESCE(1y_avg_daily_valid_purchase       ,     0) 1y_avg_daily_valid_purchase
                              ,COALESCE(1y_avg_daily_user_commission      ,     0) 1y_avg_daily_user_commission
                              ,COALESCE(1y_avg_daily_spend_imputed        ,     0) 1y_avg_daily_spend_imputed
                              ,COALESCE(3y_valid_purchases                ,     0) 3y_valid_purchases
                              ,COALESCE(3y_days_since_last_valid_purchase , 42457) 3y_days_since_last_valid_purchase
                              ,COALESCE(3y_avg_daily_valid_purchase       ,     0) 3y_avg_daily_valid_purchase
                              ,COALESCE(3y_avg_daily_user_commission      ,     0) 3y_avg_daily_user_commission
                              ,COALESCE(3y_avg_daily_spend_imputed        ,     0) 3y_avg_daily_spend_imputed
                              ,COALESCE(3y_clicks                         ,     0) 3y_clicks
                              ,COALESCE(3y_days_since_last_click          , 42457) 3y_days_since_last_click
                              ,COALESCE(3y_avg_daily_click                ,     0) 3y_avg_daily_click
                              ,COALESCE(3m_logins                         ,     0) 3m_logins
                              ,COALESCE(3m_days_since_last_login          , 42457) 3m_days_since_last_login
                              ,COALESCE(3m_avg_daily_login                ,     0) 3m_avg_daily_login
                              ,COALESCE(3y_logins                         ,     0) 3y_logins
                              ,COALESCE(3y_days_since_last_login          , 42457) 3y_days_since_last_login
                              ,COALESCE(3y_avg_daily_login                ,     0) 3y_avg_daily_login
                              ,COALESCE(3m_email_activity                 ,     0) 3m_email_activity
                              ,COALESCE(3m_days_since_last_email_activity , 42457) 3m_days_since_last_email_activity
                              ,COALESCE(3m_avg_daily_email_activity       ,     0) 3m_avg_daily_email_activity
                              ,COALESCE(3y_email_activity                 ,     0) 3y_email_activity
                              ,COALESCE(3y_days_since_last_email_activity , 42457) 3y_days_since_last_email_activity
                              ,COALESCE(3y_avg_daily_email_activity       ,     0) 3y_avg_daily_email_activity
                               
                          -- -------------------------------------------------------------------------------------------
                          -- Get every user who needs to be RFV scored
                          -- -------------------------------------------------------------------------------------------
                          FROM di_dim_user u
                                   
                               -- --------------------------------------------------------------------------------------
                               -- Join purchase statistics from the last 3 months, 1 year and 3 years
                               -- --------------------------------------------------------------------------------------
                               LEFT JOIN (SELECT p.di_user_id
                                                
                                                -- -----------------------------------------------
                                                -- 3 Months statistics for W&E
                                                -- -----------------------------------------------
                                                ,   SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN 1 ELSE 0 END) 3m_valid_purchases
                                                ,   MIN(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN DATEDIFF(DATE_FORMAT(NOW(), ''%Y%m01''), COALESCE(p.purchase_date_id, 19000101)) ELSE NULL END) 3m_days_since_last_valid_purchase
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         3m_avg_daily_valid_purchase
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN p.user_commission ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         3m_avg_daily_user_commission
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN p.spend_imputed ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         3m_avg_daily_spend_imputed
                                                 
                                                 -- -----------------------------------------------
                                                 -- 1 Year statistics for R&D
                                                 -- -----------------------------------------------
                                                ,   SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN 1 ELSE 0 END) 1y_valid_purchases
                                                ,   MIN(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN DATEDIFF(DATE_FORMAT(NOW(), ''%Y%m01''), COALESCE(p.purchase_date_id, 19000101)) ELSE NULL END) 1y_days_since_last_valid_purchase
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         1y_avg_daily_valid_purchase
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN p.user_commission ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         1y_avg_daily_user_commission
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN p.spend_imputed ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 YEAR)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         1y_avg_daily_spend_imputed
                                                
                                                -- -----------------------------------------------
                                                -- 3 Years statistics for R&C
                                                -- -----------------------------------------------
                                                ,   SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN 1 ELSE 0 END) 3y_valid_purchases
                                                ,   MIN(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN DATEDIFF(DATE_FORMAT(NOW(), ''%Y%m01''), COALESCE(p.purchase_date_id, 19000101)) ELSE NULL END) 3y_days_since_last_valid_purchase
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         3y_avg_daily_valid_purchase
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN p.user_commission ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         3y_avg_daily_user_commission
                                                ,COALESCE(
                                                    SUM(CASE WHEN p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'') THEN p.spend_imputed ELSE 0 END)
                                                  / DATEDIFF(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                                                                                                               ,u.registration_datetime))
                                                 , 0)                                                         3y_avg_daily_spend_imputed
                                            FROM di_fact_purchase p
                                                ,di_dim_user u
                                           WHERE p.di_domain_id = 1
                                             AND u.di_user_id = p.di_user_id
                                             AND p.purchase_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') 
                                                                        AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'')
                                             AND p.is_valid = 1
                                           GROUP BY p.di_user_id) p
                                      ON p.di_user_id = u.di_user_id
                                     
                               -- --------------------------------------------------------------------------------------
                               -- Join click statistics from the last 3 years
                               -- --------------------------------------------------------------------------------------
                               LEFT JOIN (SELECT c.di_user_id
                               
                                                -- -----------------------------------------------
                                                -- 3 Years statistics for R&C
                                                -- -----------------------------------------------
                                                ,COUNT(*)                                                     3y_clicks
                                                ,COALESCE(DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), MAX(c.click_date_id)), 19000101)    3y_days_since_last_click
                                                ,COALESCE(
                                                    COUNT(*)
                                                  / DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                                                                         ,u.registration_datetime))
                                                 , 0)                                                         3y_avg_daily_click
                                            FROM di_fact_click c
                                                ,di_dim_user u
                                           WHERE c.di_domain_id = 1
                                             AND c.is_valid = 1
                                             AND u.di_user_id = c.di_user_id
                                             AND c.click_date_id BETWEEN DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR), ''%Y%m%d'') 
                                                                     AND DATE_FORMAT(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 1 DAY), ''%Y%m%d'')
                                           GROUP BY c.di_user_id) c
                                      ON c.di_user_id = u.di_user_id
                                      
                               -- --------------------------------------------------------------------------------------
                               -- Join login statistics from the last 3 months and 3 years
                               -- --------------------------------------------------------------------------------------
                               LEFT JOIN (SELECT ul.user_id
                               
                                                -- -----------------------------------------------
                                                -- 3 Months statistics for W&E
                                                -- -----------------------------------------------
                                                ,   SUM(CASE WHEN ul.login_date BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END) 3m_logins
                                                ,   MIN(CASE WHEN ul.login_date BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), COALESCE(ul.login_date, 19000101)) ELSE NULL END) 3m_days_since_last_login
                                                ,COALESCE(
                                                    SUM(CASE WHEN ul.login_date BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY)
                                                                                                         ,u.registration_datetime))
                                                 , 0)                                                         3m_avg_daily_login
                                                 
                                                -- -----------------------------------------------
                                                -- 3 Years statistics for R&C
                                                -- -----------------------------------------------
                                                ,   SUM(CASE WHEN ul.login_date BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END) 3y_logins
                                                ,   MIN(CASE WHEN ul.login_date BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), COALESCE(ul.login_date, 19000101)) ELSE NULL END) 3y_days_since_last_login
                                                ,COALESCE(
                                                    SUM(CASE WHEN ul.login_date BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                                                                         ,u.registration_datetime))
                                                 , 0)                                                         3y_avg_daily_login
                                           FROM quidco_mambo.qco_user_logins ul
                                                LEFT JOIN di_dim_user u
                                                       ON u.di_domain_id = 1
                                                      AND u.domain_user_id = ul.user_id
                                          WHERE ul.login_date >= DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                            AND ul.login_date < DATE(DATE_FORMAT(NOW(), ''%Y%m01''))
                                          GROUP BY ul.user_id) ul
                                      ON ul.user_id = u.domain_user_id
                                      
                               -- --------------------------------------------------------------------------------------
                               -- Join email opened statistics from the last 3 months and 3 years
                               -- --------------------------------------------------------------------------------------
                               LEFT JOIN (SELECT e.user_id
                                          
                                                -- -----------------------------------------------
                                                -- 3 Months statistics for W&E
                                                -- -----------------------------------------------
                                                ,   SUM(CASE WHEN e.activity_datetime BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END) 3m_email_activity
                                                ,   MIN(CASE WHEN e.activity_datetime BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), COALESCE(e.activity_datetime, 19000101)) ELSE NULL END) 3m_days_since_last_email_activity
                                                ,COALESCE(
                                                    SUM(CASE WHEN e.activity_datetime BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 90 DAY)
                                                                                                         ,u.registration_datetime))
                                                 , 0)                                                         3m_avg_daily_email_activity
                                          
                                                -- -----------------------------------------------
                                                -- 3 Years statistics for R&C
                                                -- -----------------------------------------------
                                                ,   SUM(CASE WHEN e.activity_datetime BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END) 3y_email_activity
                                                ,   MIN(CASE WHEN e.activity_datetime BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), COALESCE(e.activity_datetime, 19000101)) ELSE NULL END) 3y_days_since_last_email_activity
                                                ,COALESCE(
                                                    SUM(CASE WHEN e.activity_datetime BETWEEN DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR) AND DATE(DATE_FORMAT(NOW(), ''%Y%m01'')) THEN 1 ELSE 0 END)
                                                  / DATEDIFF(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), GREATEST(DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                                                                         ,u.registration_datetime))
                                                 , 0)                                                         3y_avg_daily_email_activity
                                            FROM (SELECT user_id
                                                        ,opened_datetime activity_datetime
                                                    FROM quidco_mambo.qco_email_opened eo
                                                   WHERE eo.email_opened_id >  46000000 -- Indexed column 1st April 2013
                                                     AND eo.opened_datetime >= DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                     AND eo.opened_datetime < DATE(DATE_FORMAT(NOW(), ''%Y%m01''))
                                                     AND eo.live = 1
                                                   UNION ALL
                                                  SELECT user_id 
                                                        ,email_click_datetime activity_datetime
                                                    FROM quidco_mambo.qco_email_click ec
                                                   WHERE ec.email_click_datetime >= DATE_SUB(DATE(DATE_FORMAT(NOW(), ''%Y%m01'')), INTERVAL 3 YEAR)
                                                     AND ec.email_click_datetime < DATE(DATE_FORMAT(NOW(), ''%Y%m01''))) e
                                                 LEFT JOIN di_dim_user u
                                                        ON u.di_domain_id = 1
                                                       AND u.domain_user_id = e.user_id
                                           GROUP BY e.user_id) ea
                                      ON ea.user_id = u.domain_user_id

                         WHERE u.di_domain_id = 1                        
                         GROUP BY u.di_user_id) user_fact_click_base_aggr
                  ) y
        ) z'
, NULL        , 'Custom SQL for RFV Scoring')
 ,(   35,  2, 'DI_WRK_TRANSACTION', 'IS_VALID', 'INVALID_NETWORK_STATUS',  NULL, 'denied', NULL, 'is_valid rule for purchases with denied network_status.')
 ,(   36,  2, 'DI_WRK_TRANSACTION', 'IS_VALID', 'INVALID_NETWORK',  NULL, 'QP', NULL, 'This network is invalid in QIPU')
 ,(   37,  2, 'DI_WRK_TRANSACTION', 'IS_VALID', 'INVALID_STATUS_BLOCKED',  NULL, 'blocked', NULL, 'is_valid rule for purchases with blocked transaction.')
 ,( 38,  200, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_STATUS_DECLINED'    	,  NULL, 'declined'         , NULL        , 'Transaction with this status is not valid. 																		   ')
 ,( 39,  200, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_STATUS_DECLINED_BLOCKED'     	,  NULL, 'declined-blocked' , NULL        , 'Transaction with this status is not valid. 																		   ')
 ,( 40,  200, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'SPEND_MAX_LIMIT'          	, 50000, NULL               , NULL        , 'Transaction amount above this value is not valid'                                                                      )
 ,( 41,  200, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'COMMISSION_MAX_LIMIT'     	, 50000, NULL               , NULL        , 'Commission above this value is not valid transaction'                                                                  )
 ,( 42,  200, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'USER_COMMISSION_MAX_LIMIT'	, 50000, NULL               , NULL        , 'User commission above this value is not valid transaction'                                                             )
 ,( 43,  200, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'SPEND_MAX_LIMIT'			, 50000, NULL               , NULL        , 'Transaction amount above this value is outlier transaction'                                                            )
 ,( 44,  200, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'COMMISSION_MAX_LIMIT'     	, 50000, NULL               , NULL        , 'Commission above this value is outlier transaction'                                                                    )
 ,( 45,  200, 'DI_WRK_TRANSACTION', 'IS_OUTLIER'          , 'USER_COMMISSION_MAX_LIMIT'	, 50000, NULL               , NULL        , 'User commission above this value is outlier transaction'                                                               )       
 ,( 46,  200, 'DI_WRK_TRANSACTION', 'IS_VALID'            , 'INVALID_NETWORK'           ,  NULL, 'SHP'              , NULL        , 'This network is invalid'                                                                                               )
 ,( 47,  1, 'DI_WRK_USER_MERCHANT_CLUSTER_FRAGMENTS', 'MERCHANT_CLUSTER_FRAGMENTS'            , 'MERCHANT_CLUSTER_FRAGMENTS_SQL'    	,  NULL, 'SELECT di_domain_id
      ,domain_user_id
      ,tenure
      ,freemium_type_l2_id
      ,quidco_opinions
      ,mobile_app
      ,clicksnap_user
      ,last_12_month_valid_transactions
      ,CASE
           WHEN tenure > 1 THEN LOG(last_12_month_valid_transactions)
                 ELSE LOG(last_12_month_valid_transactions / tenure)
         END last_12_month_log_norm_trans
      ,last_12_month_avg_cashback_per_valid_trans
      ,last_12_month_std_cashback_per_valid_trans
      ,CASE
         WHEN instores = 0 AND onlines > 0                                                                                 THEN 1
         WHEN instores > 0 AND onlines > 0 AND COALESCE(instores / (last_12_month_avg_cashback_per_valid_trans),0) < 0.175371534802864 THEN 2
         WHEN instores > 0 AND onlines > 0 AND COALESCE(instores / (last_12_month_avg_cashback_per_valid_trans),0) > 0.175371534802864 THEN 3
         WHEN instores > 0 AND onlines = 0 THEN 4
       END online_instore
       
       -- ---------------------------------------------------------------------------------------------
       -- Cluster fractions
       -- ---------------------------------------------------------------------------------------------
      ,ROUND(merchant_cluster_1_trans   / last_12_month_valid_transactions, 3) cluster_1_fraction
      ,ROUND(merchant_cluster_2_trans   / last_12_month_valid_transactions, 3) cluster_2_fraction
      ,ROUND(merchant_cluster_3_trans   / last_12_month_valid_transactions, 3) cluster_3_fraction
      ,ROUND(merchant_cluster_4_trans   / last_12_month_valid_transactions, 3) cluster_4_fraction
      ,ROUND(merchant_cluster_5_trans   / last_12_month_valid_transactions, 3) cluster_5_fraction
      ,ROUND(merchant_cluster_6_trans   / last_12_month_valid_transactions, 3) cluster_6_fraction
      ,ROUND(merchant_cluster_7_trans   / last_12_month_valid_transactions, 3) cluster_7_fraction
      ,ROUND(merchant_cluster_8_trans   / last_12_month_valid_transactions, 3) cluster_8_fraction
      ,ROUND(merchant_cluster_9_trans   / last_12_month_valid_transactions, 3) cluster_9_fraction
      ,ROUND(merchant_cluster_10_trans  / last_12_month_valid_transactions, 3) cluster_10_fraction
      ,ROUND(merchant_cluster_11_trans  / last_12_month_valid_transactions, 3) cluster_11_fraction
      ,ROUND(merchant_cluster_12_trans  / last_12_month_valid_transactions, 3) cluster_12_fraction
      ,ROUND(merchant_cluster_13_trans  / last_12_month_valid_transactions, 3) cluster_13_fraction
      ,ROUND(merchant_cluster_14_trans  / last_12_month_valid_transactions, 3) cluster_14_fraction
      ,ROUND(merchant_cluster_15_trans  / last_12_month_valid_transactions, 3) cluster_15_fraction
      ,ROUND(merchant_cluster_16_trans  / last_12_month_valid_transactions, 3) cluster_16_fraction
      ,ROUND(merchant_cluster_17_trans  / last_12_month_valid_transactions, 3) cluster_17_fraction
      ,ROUND(merchant_cluster_18_trans  / last_12_month_valid_transactions, 3) cluster_18_fraction
      ,ROUND(merchant_cluster_19_trans  / last_12_month_valid_transactions, 3) cluster_19_fraction
      ,ROUND(merchant_cluster_20_trans  / last_12_month_valid_transactions, 3) cluster_20_fraction
      ,ROUND(merchant_cluster_21_trans  / last_12_month_valid_transactions, 3) cluster_21_fraction
      ,ROUND(merchant_cluster_22_trans  / last_12_month_valid_transactions, 3) cluster_22_fraction
      ,ROUND(merchant_cluster_23_trans  / last_12_month_valid_transactions, 3) cluster_23_fraction
      ,ROUND(merchant_cluster_24_trans  / last_12_month_valid_transactions, 3) cluster_24_fraction
      ,ROUND(merchant_cluster_25_trans  / last_12_month_valid_transactions, 3) cluster_25_fraction
      ,ROUND(merchant_cluster_26_trans  / last_12_month_valid_transactions, 3) cluster_26_fraction
      ,ROUND(merchant_cluster_27_trans  / last_12_month_valid_transactions, 3) cluster_27_fraction
      
  FROM (SELECT p.di_domain_id
              ,u.domain_user_id                                                   domain_user_id
              ,MIN(u.registration_date_id)                                        registration_date_id
              ,SQRT(ABS(DATEDIFF(MIN(u.registration_date_id), NOW())) / 365)      tenure
              ,MIN(u.freemium_type_l2_id)                                         freemium_type_l2_id
              
              -- ---------------------------------------------------------------------------------------
              -- Flag users who signed up to Quidco Opinions      - TODO
              -- Flag users who have downloaded the mobile app    - TODO
              -- Flag users users who have signed up to ClickSnap - TODO
              -- ---------------------------------------------------------------------------------------
              ,0                                                                  quidco_opinions
              ,0                                                                  mobile_app
              ,0                                                                  clicksnap_user
              
              -- ---------------------------------------------------------------------------------------------
              -- Number of transactions, Average Cashback per transaction & STD cashback per transaction
              -- ---------------------------------------------------------------------------------------------
              ,COUNT(t.di_transaction_id)                                         last_12_month_valid_transactions
              ,ROUND(SUM(t.user_commission) / COUNT(t.di_transaction_id), 3)      last_12_month_avg_cashback_per_valid_trans
                  ,ROUND(STDDEV(t.user_commission)                       , 3)         last_12_month_std_cashback_per_valid_trans
              ,SUM(m.is_instore)                                                  instores
              ,COUNT(t.di_transaction_id) - SUM(m.is_instore)                     onlines
              
              -- ---------------------------------------------------------------------------------------------
              -- Put transactions into merchant cluster buckets
              -- ---------------------------------------------------------------------------------------------
              ,SUM(CASE WHEN mc.merchant_cluster_id =  1 THEN 1 ELSE 0 END)        merchant_cluster_1_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  2 THEN 1 ELSE 0 END)        merchant_cluster_2_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  3 THEN 1 ELSE 0 END)        merchant_cluster_3_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  4 THEN 1 ELSE 0 END)        merchant_cluster_4_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  5 THEN 1 ELSE 0 END)        merchant_cluster_5_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  6 THEN 1 ELSE 0 END)        merchant_cluster_6_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  7 THEN 1 ELSE 0 END)        merchant_cluster_7_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  8 THEN 1 ELSE 0 END)        merchant_cluster_8_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id =  9 THEN 1 ELSE 0 END)        merchant_cluster_9_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 10 THEN 1 ELSE 0 END)        merchant_cluster_10_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 11 THEN 1 ELSE 0 END)        merchant_cluster_11_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 12 THEN 1 ELSE 0 END)        merchant_cluster_12_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 13 THEN 1 ELSE 0 END)        merchant_cluster_13_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 14 THEN 1 ELSE 0 END)        merchant_cluster_14_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 15 THEN 1 ELSE 0 END)        merchant_cluster_15_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 16 THEN 1 ELSE 0 END)        merchant_cluster_16_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 17 THEN 1 ELSE 0 END)        merchant_cluster_17_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 18 THEN 1 ELSE 0 END)        merchant_cluster_18_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 19 THEN 1 ELSE 0 END)        merchant_cluster_19_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 20 THEN 1 ELSE 0 END)        merchant_cluster_20_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 21 THEN 1 ELSE 0 END)        merchant_cluster_21_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 22 THEN 1 ELSE 0 END)        merchant_cluster_22_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 23 THEN 1 ELSE 0 END)        merchant_cluster_23_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 24 THEN 1 ELSE 0 END)        merchant_cluster_24_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 25 THEN 1 ELSE 0 END)        merchant_cluster_25_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 26 THEN 1 ELSE 0 END)        merchant_cluster_26_trans
              ,SUM(CASE WHEN mc.merchant_cluster_id = 27 THEN 1 ELSE 0 END)        merchant_cluster_27_trans
              
          -- ---------------------------------------------------------------------------------------------
          -- Raw transactions of
          --   1) Users for whom a row has been updated since the quidco_reporting table last updated
          --   2) Last 12 months transactions
          --   3) Only valid transactions  
          --   4) Exclude cobrand users
          -- ---------------------------------------------------------------------------------------------
          FROM di_fact_purchase p
              ,di_wrk_transaction t
              ,di_dim_user u
              ,di_dim_merchant m
               LEFT JOIN di_lkp_merchant_cluster mc
                      ON mc.di_domain_id       = m.di_domain_id
                     AND mc.domain_merchant_id = m.domain_merchant_id
         WHERE t.di_purchase_id    = p.di_purchase_id
           AND t.di_domain_id      = p.di_domain_id
           AND u.di_user_id        = p.di_user_id
           AND m.di_merchant_id    = p.di_merchant_id
           AND p.di_domain_id      = 1
           AND p.purchase_date_id >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -12 MONTH), ''%Y%m%d'')
           AND t.is_valid          = 1
           AND u.is_cobrand        = 0
           
           -- ------------------------------------------------------------------
           -- Rescore only the updated users since the previous run
           -- ------------------------------------------------------------------
           AND u.di_last_updated_date >= (SELECT value_datetime
                                            FROM di_etl_job_params
                                           WHERE job_name = ''load_dwh_QUIDCO_COM_DI_WRK_USER_MERCHANT_CLUSTER_FRAGMENTS''
                                             AND job_param_name = ''last_run_date'')
         GROUP BY p.di_domain_id
                 ,u.domain_user_id) x

'         , NULL        , 'Custom SQL to calculate user merchant cluster fragments')
       ,( 48,  1, 'DI_WRK_USER_SEGMENT_BEHAVIOUR', 'BEHAVIOURAL_SEGMENT'            , 'BEHAVIOURAL_SEGMENT_SQL'    	,  NULL, 'SELECT di_domain_id
      ,domain_user_id                                   domain_user_id
      ,MAX(behavioural_segment_id_at_max_likelyhood)    behavioural_segment_id
      ,MAX(behavioural_segment_name_at_max_likelyhood)  behavioural_segment_name
   FROM (SELECT di_domain_id
               ,domain_user_id
               ,behavioural_segment_id
               ,likelyhood
              
               -- -----------------------------------------------------------------------
               -- Rank likelyhood - Analytical functions are not supported by MySQL :(
               -- -----------------------------------------------------------------------
               ,@likelyhood_rank := CASE
                                      WHEN @prev_domain_user_id != domain_user_id THEN 1
                                      ELSE @likelyhood_rank := @likelyhood_rank + 1
                                    END likelyhood_rank
                                    
               ,@behavioural_segment_id_at_max_likelyhood := CASE
                                      WHEN @likelyhood_rank = 1 THEN behavioural_segment_id
                                      ELSE @behavioural_segment_id_at_max_likelyhood
                                   END  behavioural_segment_id_at_max_likelyhood
                                   
               ,@behavioural_segment_name_at_max_likelyhood := CASE
                                      WHEN @likelyhood_rank = 1 THEN behavioural_segment_name
                                      ELSE @behavioural_segment_name_at_max_likelyhood
                                   END  behavioural_segment_name_at_max_likelyhood
                                  
               ,@prev_domain_user_id := domain_user_id prev_domain_user_id
               
          FROM (SELECT mc.di_domain_id
                      ,mc.domain_user_id
                      ,s.behavioural_segment_id
                      ,s.behavioural_segment_name
                      ,  fn_get_gaussian_kernal(s.mean_tenure                 , SQRT(s.var_tenure)                 , mc.tenure                 )
                       * fn_get_gaussian_kernal(s.mean_avg_cashback_per_trans , SQRT(s.var_avg_cashback_per_trans) , mc.last_12_month_avg_cashback_per_valid_trans )
                       * fn_get_gaussian_kernal(s.mean_log_norm_trans         , SQRT(s.var_log_norm_trans)         , mc.last_12_month_log_norm_trans         )
                       * fn_get_gaussian_kernal(s.mean_std_cashback_per_trans , SQRT(s.var_std_cashback_per_trans) , mc.last_12_month_std_cashback_per_valid_trans )
                       * fn_get_gaussian_kernal(s.mean_cluster_1_fraction     , SQRT(s.var_cluster_1_fraction)     , mc.cluster_1_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_2_fraction     , SQRT(s.var_cluster_2_fraction)     , mc.cluster_2_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_3_fraction     , SQRT(s.var_cluster_3_fraction)     , mc.cluster_3_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_4_fraction     , SQRT(s.var_cluster_4_fraction)     , mc.cluster_4_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_5_fraction     , SQRT(s.var_cluster_5_fraction)     , mc.cluster_5_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_6_fraction     , SQRT(s.var_cluster_6_fraction)     , mc.cluster_6_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_7_fraction     , SQRT(s.var_cluster_7_fraction)     , mc.cluster_7_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_8_fraction     , SQRT(s.var_cluster_8_fraction)     , mc.cluster_8_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_9_fraction     , SQRT(s.var_cluster_9_fraction)     , mc.cluster_9_fraction     )
                       * fn_get_gaussian_kernal(s.mean_cluster_10_fraction    , SQRT(s.var_cluster_10_fraction)    , mc.cluster_10_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_11_fraction    , SQRT(s.var_cluster_11_fraction)    , mc.cluster_11_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_12_fraction    , SQRT(s.var_cluster_12_fraction)    , mc.cluster_12_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_13_fraction    , SQRT(s.var_cluster_13_fraction)    , mc.cluster_13_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_14_fraction    , SQRT(s.var_cluster_14_fraction)    , mc.cluster_14_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_15_fraction    , SQRT(s.var_cluster_15_fraction)    , mc.cluster_15_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_16_fraction    , SQRT(s.var_cluster_16_fraction)    , mc.cluster_16_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_17_fraction    , SQRT(s.var_cluster_17_fraction)    , mc.cluster_17_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_18_fraction    , SQRT(s.var_cluster_18_fraction)    , mc.cluster_18_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_19_fraction    , SQRT(s.var_cluster_19_fraction)    , mc.cluster_19_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_20_fraction    , SQRT(s.var_cluster_20_fraction)    , mc.cluster_20_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_21_fraction    , SQRT(s.var_cluster_21_fraction)    , mc.cluster_21_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_22_fraction    , SQRT(s.var_cluster_22_fraction)    , mc.cluster_22_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_23_fraction    , SQRT(s.var_cluster_23_fraction)    , mc.cluster_23_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_24_fraction    , SQRT(s.var_cluster_24_fraction)    , mc.cluster_24_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_25_fraction    , SQRT(s.var_cluster_25_fraction)    , mc.cluster_25_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_26_fraction    , SQRT(s.var_cluster_26_fraction)    , mc.cluster_26_fraction    )
                       * fn_get_gaussian_kernal(s.mean_cluster_27_fraction    , SQRT(s.var_cluster_27_fraction)    , mc.cluster_27_fraction    )
                       * CASE
                           WHEN mc.freemium_type_l2_id = 3  THEN s.user_type_value1
                           ELSE                                  s.user_type_value2
                         END
                       * CASE
                           WHEN mc.quidco_opinions = 0  THEN s.quidco_opinions_value1
                           ELSE                              s.quidco_opinions_value2
                         END
                       * CASE
                           WHEN mc.clicksnap_user  = 0  THEN s.clicksnap_user_value1
                           ELSE                              s.clicksnap_user_value2
                         END
                       * CASE
                           WHEN mc.mobile_app      = 0  THEN s.mobile_app_value1
                           ELSE                              s.mobile_app_value2
                         END
                       * CASE
                           WHEN mc.online_instore  = 1  THEN s.online_instore_value1
                           WHEN mc.online_instore  = 2  THEN s.online_instore_value2
                           WHEN mc.online_instore  = 3  THEN s.online_instore_value3
                           WHEN mc.online_instore  = 4  THEN s.online_instore_value4
                           ELSE                              0
                         END
                        * s.mixing_prob  likelyhood
                  FROM di_wrk_user_merchant_cluster_fragments mc
                      ,di_lkp_segment_behaviour s
                 WHERE mc.di_domain_id = 1
                   AND mc.di_last_updated_date >= (SELECT value_datetime
                                                     FROM di_etl_job_params
                                                    WHERE job_name = ''load_dwh_QUIDCO_COM_DI_WRK_USER_SEGMENT_BEHAVIOUR''
                                                      AND job_param_name = ''last_run_date'')
               ) x
              ,(SELECT @prev_domain_user_id                         := 0
                      ,@likelyhood_rank                             := 0
                      ,@behavioural_segment_id_at_max_likelyhood    := -1
                      ,@behavioural_segment_name_at_max_likelyhood  := ''Unknown'') init_vars
          ORDER BY domain_user_id
                  ,likelyhood DESC
   ) x
   GROUP BY di_domain_id
           ,domain_user_id
'         , NULL        , 'Custom SQL to calculate user behavioural segments')
;
