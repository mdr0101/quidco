TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_signup_method;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_signup_method (di_cfg_signup_method_id
                                              ,signup_method_id
                                              ,signup_method_name
                                              ,di_created_date)
 VALUES ( -1, -1, 'Unknown'  , '1971-01-01')
       ,(  1,  1, 'Join page', '1971-01-01')
       ,(  2,  2, 'Facebook' , '1971-01-01')
       ,(  3,  3, 'PayPal'   , '1971-01-01')
       ,(  4,  4, 'Open ID'  , '1971-01-01');