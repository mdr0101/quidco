/* Static Data: di_dim_activation_source */
TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_activation_source;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_activation_source (	activation_source_id,
																activation_source_name
															 )
 VALUES 
		(0, 'online')
	   ,(1, 'mobile')
;