TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_domain;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_domain (di_domain_id
                                                  ,di_domain_name
                                                  ,domain_go_live_date_id
                                                  ,domain_description
                                                  ,currency_code
                                                  ,currency_name)
 VALUES (  -1, 'Unknown'   ,       -1, 'Unknown'              , 'N/A', 'Unknown')
       ,(   1, 'quidco.com', 20050101, 'http://www.quidco.com', 'GBP', 'United Kingdom Pound')
       ,(   2, 'qipu.de'   , 20140601, 'http://www.qipu.de'   , 'EUR', 'Euro Member Countries')
       ,( 200, 'shoop.fr'  , 20160301, 'http://www.shoop.fr'  , 'EUR', 'Euro Member Countries')
;
