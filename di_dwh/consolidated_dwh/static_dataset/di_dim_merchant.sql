TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_merchant;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_merchant (di_merchant_id
                                     ,di_merchant_name
                                     ,di_domain_id
                                     ,domain_merchant_id
                                     ,domain_merchant_name
                                     ,merchant_cluster_id
                                     ,merchant_cluster_name
                                     ,merchant_hierarchy_l1_id
                                     ,merchant_hierarchy_l1_name									 
                                     ,merchant_hierarchy_l2_id
                                     ,merchant_hierarchy_l2_name
                                     ,merchant_hierarchy_l3_id
                                     ,merchant_hierarchy_l3_name									 
                                     ,merchant_hierarchy_l4_id
                                     ,merchant_hierarchy_l4_name
                                     ,is_instore
                                     ,instore_type_id									 
									 ,instore_type_name
                                     ,has_instore_control_group
                                     ,is_fast_payment
                                     ,di_created_date
                                     ,di_last_updated_date
                                     ,job_run_id					 
									 )
 VALUES -- ---------------------------------------------------------------------
        -- Quidco.com DI_DIM_MERCHANT - Default 'Unknown' values for FACTS
        -- ---------------------------------------------------------------------
        -- QUIDCO.COM Unknown Merchant default values
		
    --  di_merchant_id	di_merchant_name	 			di_domain_id	domain_merchant_id	domain_merchant_name	merchant_cluster_id	       merchant_cluster_name	merchant_hierarchy_l1_id	merchant_hierarchy_l1_name	merchant_hierarchy_l2_id	merchant_hierarchy_l2_name	merchant_hierarchy_l3_id	merchant_hierarchy_l3_name	merchant_hierarchy_l4_id	merchant_hierarchy_l4_name	is_instore	instore_type_id	instore_type_name	has_instore_control_group	      is_fast_payment           di_created_date			    di_last_updated_date	  job_run_id
		(	        -1,	      'Unknown',						 -1,		     999999999,			   'Unknown',				     -1,				  'Unknown',				         -1,		  		    'Unknown', 				         -1,					'Unknown',					     -1,					'Unknown', 					     -1,					'Unknown', 			0,			     -1,		'Unknown',			               0,   0,        '1971/01/01 00:00:00',	           '1971/01/01 00:00:00',	  -1)
 ;
