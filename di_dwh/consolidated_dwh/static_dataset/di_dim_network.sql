TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_network;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_network (di_network_id
                                     ,di_network_name
                                     ,di_domain_id
                                     ,domain_network
                                     ,network_group_id
                                     ,network_group_name
                                     ,contact
                                     ,email
                                     ,currency
                                     ,is_card_linked_network
                                     ,di_created_date									 
                                     ,di_last_updated_date
                                     ,job_run_id					 
									 )
 VALUES -- ---------------------------------------------------------------------
        -- Quidco.com DI_DIM_Network - Default 'Unknown' values for FACTS
        -- ---------------------------------------------------------------------
		(		-1                          -- di_network_id
        ,'Unknown'                  -- di_network_name
        ,-1                         -- di_domain_id
        ,'Unknown'                  -- domain_network
        ,-1                         -- network_group_id
        ,'Unknown'                  -- network_group_name
        ,'Unknown'                  -- contact
        ,'Unknown'                  -- email
        ,0                          -- currency
        ,0                          -- is_card_linked_network
        ,'1971/01/01 00:00:00'      -- di_created_date
        ,'1971/01/01 00:00:00'      -- di_last_updated_date
        ,-1)                        -- job_run_id
 ;