TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_purchase_status;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_purchase_status (di_purchase_status_id
                                                           ,di_domain_id
                                                           ,purchase_status_id
                                                           ,purchase_status_name
                                                           ,purchase_status_group_id
                                                           ,purchase_status_group_name)
 VALUES -- ---------------------------------------------------------------------
        -- Quidco.com Purchase Status
        -- ---------------------------------------------------------------------
        -- QUIDCO.COM Purchase Statuses
       ( -1,  -1, -1,    'Unknown'                    , -1, 'Unknown'   )
      ,(  1,   1,  1,    'pending'                    ,  1, 'Tracked'   )
      ,(  2,   1,  2,    'added'                      ,  1, 'Tracked'   )
      ,(  3,   1,  3,    'validated'                  ,  2, 'Validated' )
      ,(  4,   1,  4,    'received'                   ,  5, 'Received'  )
      ,(  5,   1,  5,    'denied'                     ,  3, 'Declined'  )
      ,(  6,   1,  6,    'paid'                       ,  6, 'Paid'      )
      ,(  7,   1,  7,    'claimed'                    ,  7, 'Claimed'   )
      
      -- QIPU.DE Purchase Statuses
      ,(  8,   2,  1,    'pending'                    ,  1, 'Tracked'   )
      ,(  9,   2,  2,    'added'                      ,  1, 'Tracked'   )
      ,( 10,   2,  3,    'validated'                  ,  2, 'Validated' )
      ,( 11,   2,  4,    'received'                   ,  5, 'Received'  )
      ,( 12,   2,  5,    'denied'                     ,  3, 'Declined'  )
      ,( 13,   2,  6,    'paid'                       ,  6, 'Paid'      )
      ,( 14,   2,  7,    'marked-as-received'         ,  5, 'Received'  )
      ,( 15,   2,  8,    'tracked-blocked'            ,  1, 'Tracked'   )
      ,( 16,   2,  9,    'validated-blocked'          ,  2, 'Validated' )
      ,( 17,   2, 10,    'claimed'                    ,  7, 'Claimed'   )
      
      -- Q-Platform Purchase Statuses
      ,( 18, 200,  1,    'tracked'                    ,  1, 'Tracked'   )
      ,( 19, 200,  2,    'validated'                  ,  2, 'Validated' )
      ,( 20, 200,  3,    'wallet-error'               ,  2, 'Validated' )
      ,( 21, 200,  4,    'validated-fraudcheck'       ,  2, 'Validated' )
      ,( 22, 200,  5,    'validated-fraudcheck-ready' ,  2, 'Validated' )
      ,( 23, 200,  6,    'validated-wallet-pending'   ,  2, 'Validated' )
      ,( 24, 200,  7,    'validated-wallet-request'   ,  2, 'Validated' )
      ,( 25, 200,  8,    'validated-fasterpaying'     ,  2, 'Validated' )
      ,( 26, 200,  9,    'confirmed-prepaid'          ,  4, 'Confirmed' )
      ,( 27, 200, 10,    'confirmed-goodwill'         ,  4, 'Confirmed' )
      ,( 28, 200, 11,    'confirmed-manual'           ,  4, 'Confirmed' )
      ,( 29, 200, 12,    'confirmed-fasterpaying'     ,  4, 'Confirmed' )
      ,( 30, 200, 13,    'confirmed-blocked'          ,  4, 'Confirmed' )
      ,( 31, 200, 14,    'confirmed-payments-in'      ,  4, 'Confirmed' )
      ,( 32, 200, 15,    'declined'                   ,  3, 'Declined'  )
      ,( 33, 200, 16,    'tracked-blocked'            ,  1, 'Tracked'   )
      ,( 34, 200, 17,    'declined-blocked'           ,  3, 'Declined'  )
      ,( 35, 200, 18,    'validated-blocked'          ,  2, 'Validated' )
;
