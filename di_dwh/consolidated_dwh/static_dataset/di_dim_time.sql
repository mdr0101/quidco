TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_time;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_time (time_id
                                                ,start_time
                                                ,end_time
                                                ,part_of_day_id
                                                ,part_of_day_name)
 VALUES (-1,'-1','-1',-1,'Unknown')
       ,(1,'00:00','00:59',1,'Night')
       ,(2,'01:00','01:59',1,'Night')
       ,(3,'02:00','02:59',1,'Night')
       ,(4,'03:00','03:59',1,'Night')
       ,(5,'04:00','04:59',1,'Night')
       ,(6,'05:00','05:59',2,'Morning')
       ,(7,'06:00','06:59',2,'Morning')
       ,(8,'07:00','07:59',2,'Morning')
       ,(9,'08:00','08:59',2,'Morning')
       ,(10,'09:00','09:59',2,'Morning')
       ,(11,'10:00','10:59',2,'Morning')
       ,(12,'11:00','11:59',2,'Morning')
       ,(13,'12:00','12:59',3,'Afternoon')
       ,(14,'13:00','13:59',3,'Afternoon')
       ,(15,'14:00','14:59',3,'Afternoon')
       ,(16,'15:00','15:59',3,'Afternoon')
       ,(17,'16:00','16:59',3,'Afternoon')
       ,(18,'17:00','17:59',4,'Evening')
       ,(19,'18:00','18:59',4,'Evening')
       ,(20,'19:00','19:59',4,'Evening')
       ,(21,'20:00','20:59',4,'Evening')
       ,(22,'21:00','21:59',1,'Night')
       ,(23,'22:00','22:59',1,'Night')
       ,(24,'23:00','23:59',1,'Night');
