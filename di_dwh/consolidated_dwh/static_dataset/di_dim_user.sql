TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_user;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_dim_user (di_user_id
												 ,di_user_name
												 ,di_domain_id
												 ,domain_user_id
												 ,registration_date_id
												 ,registration_time_id
												 ,registration_datetime
												 ,email
												 ,authentication_datetime
												 ,authentication_date_id
												 ,authentication_time_id
												 ,is_authenticated
												 ,is_paypal_linked
												 ,is_facebook_linked
												 ,is_card_linked
												 ,is_mobile_linked
												 ,is_quidco_opinions_linked
												 ,quidco_opinions_register_date_id
												 ,is_mailable
												 ,signup_method_id
												 ,signup_method_name
												 ,is_cobrand
												 ,cobrand_id
												 ,cobrand_name
												 ,was_cobrand
												 ,first_purchase_date_id
												 ,last_purchase_date_id
												 ,last_visit_datetime
												 ,last_visit_date_id
												 ,last_visit_time_id
												 ,transactions
												 ,valid_transactions
												 ,purchases
												 ,valid_purchases
												 ,wallet_balance
												 ,first_valid_purchase_date_id
												 ,last_valid_purchase_date_id
												 ,first_non_pp_bonus_purchase_date_id
												 ,last_non_pp_bonus_purchase_date_id
												 ,non_pp_bonus_transactions
												 ,non_pp_bonus_purchases
												 ,rfv_group_id
												 ,rfv_group_name
												 ,rfv_subgroup_id
												 ,rfv_subgroup_name
												 ,lifecycle_status_id
												 ,lifecycle_status_name
												 ,behavioural_segment_id
												 ,behavioural_segment_name
												 ,freemium_type_l1_id
												 ,freemium_type_l1_name
												 ,freemium_type_l2_id
												 ,freemium_type_l2_name
												 ,freemium_type_l3_id
												 ,freemium_type_l3_name
												 ,registration_method_id
												 ,registration_method_name
												 ,registration_method_attr_1
												 ,registration_method_attr_2
												 ,registration_method_attr_3
												 ,registration_source_is_migrated
												 ,registration_source_l1_id
												 ,registration_source_l1_name
												 ,registration_source_l2_id
												 ,registration_source_l2_name
												 ,registration_source_l3_id
												 ,registration_source_l3_name
												 ,registration_source_l4_id
												 ,registration_source_l4_name
												 ,dob_id
												 ,age
												 ,age_band_id
												 ,age_band_name
												 ,gender
												 ,marital_status_id
												 ,marital_status_name
												 ,income_band_id
												 ,income_band_name
												 ,occupation_code
												 ,occupation_name
												 ,postcode
												 ,area_code
												 ,area_name
												 ,region_code
												 ,region_name
												 ,nr_of_children
												 ,di_created_date
												 ,di_last_updated_date
												 ,job_run_id)
 VALUES -- ---------------------------------------------------------------------
        -- Default 'Unknown' values for FACTS
        -- ---------------------------------------------------------------------
          (-1                     -- di_user_id
          ,'Unknown'              -- di_user_name
          ,-1                     -- di_domain_id
          ,0                      -- domain_user_id
          ,-1                     -- registration_date_id
          ,-1                     -- registration_time_id
          ,'0000-00-00 00:00:00'  -- registration_datetime
          ,'Unknown'              -- email
          ,'0000-00-00 00:00:00'  -- authentication_datetime
          ,-1                     -- authentication_date_id
          ,-1                     -- authentication_time_id
          ,0                      -- is_authenticated
          ,0                      -- is_paypal_linked
          ,0                      -- is_facebook_linked
          ,0                      -- is_card_linked
          ,0                      -- is_mobile_linked
          ,0                      -- is_quidco_opinions_linked
          ,-1                     -- quidco_opinions_register_date_id
          ,0                      -- is_mailable
          ,-1                     -- signup_method_id
          ,'Unknown'              -- signup_method_name
          ,0                      -- is_cobrand
          ,-1                     -- cobrand_id
          ,'Unknown'              -- cobrand_name
          ,0                      -- was_cobrand
          ,-1                     -- first_purchase_date_id
          ,-1                     -- last_purchase_date_id
          ,'0000-00-00 00:00:00'  -- last_visit_datetime
          ,-1                     -- last_visit_date_id
          ,-1                     -- last_visit_time_id
          ,0                      -- transactions
          ,0                      -- valid_transactions
          ,0                      -- purchases
          ,0                      -- valid_purchases
          ,0                      -- wallet_balance
          ,-1                     -- first_valid_purchase_date_id
          ,-1                     -- last_valid_purchase_date_id
          ,-1                     -- first_non_pp_bonus_purchase_date_id
          ,-1                     -- last_non_pp_bonus_purchase_date_id
          ,0                      -- non_pp_bonus_transactions
          ,0                      -- non_pp_bonus_purchases
          ,-1                     -- rfv_group_id
          ,'Unknown'              -- rfv_group_name
          ,-1                     -- rfv_subgroup_id
          ,'Unknown'              -- rfv_subgroup_name
          ,-1                     -- lifecycle_status_id
          ,'Unknown'              -- lifecycle_status_name
          ,-1                     -- behavioural_segment_id
          ,'Unknown'              -- behavioural_segment_name
          ,-1                     -- freemium_type_l1_id
          ,'Unknown'              -- freemium_type_l1_name
          ,-1                     -- freemium_type_l2_id
          ,'Unknown'              -- freemium_type_l2_name
          ,-1                     -- freemium_type_l3_id
          ,'Unknown'              -- freemium_type_l3_name
          ,-1                     -- registration_method_id
          ,'Unknown'              -- registration_method_name
          ,''                     -- registration_method_attr_1
          ,''                     -- registration_method_attr_2
          ,''                     -- registration_method_attr_3
          ,0                      -- registration_source_is_migrated
          ,-1                     -- registration_source_l1_id
          ,'Unknown'              -- registration_source_l1_name
          ,-1                     -- registration_source_l2_id
          ,'Unknown'              -- registration_source_l2_name
          ,-1                     -- registration_source_l3_id
          ,'Unknown'              -- registration_source_l3_name
          ,-1                     -- registration_source_l4_id
          ,'Unknown'              -- registration_source_l4_name
          ,-1                     -- dob_id
          ,NULL                   -- age
          ,-1                     -- age_band_id
          ,'Unknown'              -- age_band_name
          ,'Unknown'              -- gender
          ,-1                     -- marital_status_id
          ,'Unknown'              -- marital_status_name
          ,-1                     -- income_band_id
          ,'Unknown'              -- income_band_name
          ,'Unknown'              -- occupation_code
          ,'Unknown'              -- occupation_name
          ,'Unknown'              -- postcode
          ,'Unknown'              -- area_code
          ,'Unknown'              -- area_name
          ,'Unknown'              -- region_code
          ,'Unknown'              -- region_name
          ,0                      -- nr_of_children
          ,'1971/01/01 00:00:00'  -- di_created_date
          ,'1971/01/01 00:00:00'  -- di_last_updated_date
          ,-1                     -- job_run_id
 );
