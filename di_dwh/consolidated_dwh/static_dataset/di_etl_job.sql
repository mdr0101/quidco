TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_etl_jobs;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_etl_jobs (job_id
                        ,job_group
                        ,job_type
                        ,job_name
                        ,run_priority
                        ,is_active,is_next_run_active)
 VALUES 
 
 		-- ---------------------------------------------------------------------
        -- MSM Nightly Jobs
        -- ---------------------------------------------------------------------
 
 		(   1,  'MSM_NIGHTLY'            , 'JOB_GROUP', 	'COMMON_NIGHTLY_STG'			                 ,  10, 1,1)
	   ,(   2,  'MSM_NIGHTLY'            , 'JOB_GROUP', 	'SHOOP_FR_NIGHTLY'                               ,  20, 1,1)
	   ,(   3,  'MSM_NIGHTLY'            , 'JOB_GROUP', 	'QIPU_NIGHTLY'                                   ,  30, 1,1)	   
	   ,(   4,  'MSM_NIGHTLY'            , 'JOB_GROUP', 	'QUIDCO_NIGHTLY'                                 ,  40, 1,1)	   
	   ,(   5,  'MSM_NIGHTLY'            , 'JOB_GROUP', 	'COMMON_NIGHTLY_DWH'			                 ,  50, 1,1)
 
 		-- ---------------------------------------------------------------------
        -- Common Stage Nightly Jobs
        -- ---------------------------------------------------------------------
	   ,(   6,   'COMMON_NIGHTLY_STG'            , 'JOB', 'load_stg_WALLET_USER_BALANCES'                     ,  10, 1,1)
       ,(   7,  'COMMON_NIGHTLY_STG'            , 'JOB', 'load_stg_WALLET_USER_LEDGERS'                      ,  20, 1,1)
		
		
		-- ---------------------------------------------------------------------
        -- Quidco.com Nightly Jobs
        -- ---------------------------------------------------------------------
       ,(  8,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_MERCHANTS'                                   ,  10, 1,1)
       ,(  9,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_NETWORKS'                                    ,  20, 1,1)
       ,(  10,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_WRK_TRANSACTION'                             ,  30, 1,1)
       ,(  11,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_WRK_USER_MAIN'                               ,  40, 1,1)
       ,(  12,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_WRK_USER_REG_SOURCE'                         ,  50, 1,1)
       ,(  13,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_WRK_USER_COBRAND'                            ,  60, 1,1)
       ,(  14,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_WRK_USER_MAILABLE'                           ,  70, 1,1)
       ,(  15,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_WRK_USER_SEGMENT_LIFECYCLE'                  ,  80, 1,1)
       ,(  16,  'QUIDCO_NIGHTLY'            , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_DIM_USER_HIST'               ,  90, 1,1)
       ,(  17,  'QUIDCO_NIGHTLY'            , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_DIM_USER'                    , 100, 1,1)
	   ,(  18,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'INSTORE_ACTIVATIONS'                    			, 110, 1,1) -- in-store activations
       ,(  19,  'QUIDCO_NIGHTLY'            , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_FACT_PURCHASE'               , 120, 1,1)
       ,(  20,  'QUIDCO_NIGHTLY'            , 'JOB_GROUP', 'QUIDCO_CLICKS'                                      , 130, 1,1)
	   

       -- ---------------------------------------------------------------------
       -- QUIDCO Independent job groups 
       -- ---------------------------------------------------------------------
       ,(  21,  'QUIDCO_MERCHANTS'          , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_MERCHANTS'                        ,  10, 1,1)
       ,(  22,  'QUIDCO_MERCHANTS'          , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_PAYMENTS_ON_STATUS_PROGRAMS'      ,  20, 1,1)
       ,(  23,  'QUIDCO_MERCHANTS'          , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_DIM_MERCHANT_HIST'                 ,  30, 1,1)
       ,(  24,  'QUIDCO_MERCHANTS'          , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_DIM_MERCHANT'                      ,  40, 1,1)

       ,(  25,  'QUIDCO_NETWORKS'           , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_NETWORKS'                         ,  10, 1,1)
       ,(  26,  'QUIDCO_NETWORKS'           , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_DIM_NETWORK'                       ,  20, 1,1)
       
       ,(  27,  'QUIDCO_CLICKS'             , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_CLICKS'                           ,  10, 1,1)
       ,(  28,  'QUIDCO_CLICKS'             , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_LINK_CLICKS'                      ,  20, 1,1)
       ,(  29,  'QUIDCO_CLICKS'             , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_FACT_CLICK'                        ,  30, 1,1)

       ,(  30,  'QUIDCO_WRK_TRANSACTION'    , 'JOB'      , 'load_stg_QUIDCO_COM_TRANSACTIONS'                 		  ,  10, 1,1)
       ,(  31,  'QUIDCO_WRK_TRANSACTION'    , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_NONCASHBACK_TRANSACTION'          ,  20, 1,1)
       ,(  32,  'QUIDCO_WRK_TRANSACTION'    , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_WRK_TRANSACTION'                   ,  30, 1,1)
	   
       ,(  33,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS'                            ,  10, 1,1)
       ,(  34,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_COBRAND_USER_KEYS_INACTIVE'	      ,  20, 1,1)
       ,(  35,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS_EMAIL_ACTIVATION'    	      ,  30, 1,1)
       ,(  36,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_REGISTRATION_PATHS'        	      ,  40, 1,1)
       ,(  37,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS_VISITS'                     ,  50, 1,1)
       ,(  38,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_RTV_CARDS'                        ,  60, 1,1)
       ,(  39,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_MOBILE_USERS'                     ,  70, 1,1)
       ,(  40,  'QUIDCO_WRK_USER_MAIN'      , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_WRK_USER_MAIN'                     ,  80, 1,1)
	   
       ,(  41,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS'                            ,  10, 0,1)
       ,(  42,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USER_DEMOGRAPHICS'                ,  20, 1,1)
       ,(  43,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_stg_QUIDCO_COM_QCO_REFERRAL_CAMPAIGNS'               ,  30, 1,1)
       ,(  44,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_stg_QUIDCO_COM_QCO_REFERRALS'                        ,  40, 1,1)
       ,(  45,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_stg_QUIDCO_COM_QCO_REFERRERS'                        ,  50, 1,1)
       ,(  46,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USER_REFERRALS'                   ,  60, 1,1)
       ,(  47,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_dwh_QUIDCO_COM_DI_LKP_REGISTRATION_SOURCE_HIERARCHY' ,  70, 1,1)
       ,(  48,  'QUIDCO_WRK_USER_REG_SOURCE', 'JOB'      , 'load_dwh_QUIDCO_COM_DI_WRK_USER_REG_SOURCE'               ,  80, 1,1)
       
       ,(  49,  'QUIDCO_WRK_USER_COBRAND'   , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS'                            ,  10, 0,1)
       ,(  50,  'QUIDCO_WRK_USER_COBRAND'   , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_LKP_COBRAND'                       ,  20, 1,1)
       ,(  51,  'QUIDCO_WRK_USER_COBRAND'   , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_WRK_USER_COBRAND'                  ,  30, 1,1)
	   
	   ,(  52,  'INSTORE_ACTIVATIONS'   	, 'JOB'      , 'load_stg_QUIDCO_COM_STG_ACTIVATION_TRANSACTIONS'          ,  10, 1,1) -- in-store activations
       ,(  53,  'INSTORE_ACTIVATIONS'   	, 'JOB'      , 'load_dwh_DI_FACT_ACTIVATION_TRANSACTION'       			  ,  20, 1,1) -- in-store activations
	   
       ,(  54,  'QUIDCO_WRK_USER_MAILABLE'  , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS'                            ,  10, 0,1)
       ,(  55,  'QUIDCO_WRK_USER_MAILABLE'  , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS_EMAIL_ACTIVATION'           ,  20, 1,1)
       ,(  56,  'QUIDCO_WRK_USER_MAILABLE'  , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_MAILER_USER_PREFERENCE'           ,  30, 1,1)
       ,(  57,  'QUIDCO_WRK_USER_MAILABLE'  , 'JOB'      , 'load_stg_QUIDCO_COM_QCO_USERS_ACCESS_RIGHTS'              ,  40, 1,1)
       ,(  58,  'QUIDCO_WRK_USER_MAILABLE'  , 'JOB'      , 'load_dwh_QUIDCO_COM_DI_WRK_USER_MAILABLE'                 ,  50, 1,1)

       ,(  59,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_QCO_USERS'                            ,  10, 1,1)
       ,(  60,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_QCO_USER_DEMOGRAPHICS'                ,  20, 1,1)
       ,(  61,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_QCO_SCIENTIA_DEMOGRAPHICS'            ,  30, 1,1)
       ,(  62,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_QCO_SCIENTIA_NAME_ADDRESS'            ,  40, 1,1)
       ,(  63,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_QCO_SEOPIA_ALL'                       ,  50, 1,1)
       ,(  64,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_QCO_USER_PROFILE_CINT'                ,  60, 1,1)
       ,(  65,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_CINT_USER_INFO'                       ,  70, 1,1)
       ,(  66,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_CINT_PROFILE_QUESTIONS'               ,  80, 1,1)
       ,(  67,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_stg_QUIDCO_COM_CINT_PROFILE_ANSWERS'                 ,  90, 1,1)
       ,(  68,  'QUIDCO_WRK_USER_DEMOGRAPHICS', 'JOB'    , 'load_dwh_QUIDCO_COM_DI_WRK_USER_DEMOGRAPHICS'             , 100, 1,1)

       ,(  69,  'QUIDCO_WRK_USER_SEGMENT_RFV' , 'JOB'    , 'load_dwh_QUIDCO_COM_DI_WRK_USER_SEGMENT_RFV'              		,  10, 1,1)
       ,(  70,  'QUIDCO_WRK_USER_SEGMENT_LIFECYCLE', 'JOB'      , 'load_dwh_QUIDCO_COM_DI_WRK_USER_SEGMENT_LIFECYCLE' 		,  10, 1,1)
       ,(  71,  'QUIDCO_LKP_MERCHANT_CLUSTER' , 'JOB'    , 'load_dwh_QUIDCO_COM_DI_LKP_MERCHANT_CLUSTER'                  	,  10, 1,1)
       ,(  72,  'QUIDCO_WRK_USER_SEGMENT_BEHAVIOUR', 'JOB', 'load_dwh_QUIDCO_COM_DI_WRK_USER_MERCHANT_CLUSTER_FRAGMENTS'	,  10, 1,1)
       ,(  73,  'QUIDCO_WRK_USER_SEGMENT_BEHAVIOUR', 'JOB', 'load_dwh_QUIDCO_COM_DI_WRK_USER_SEGMENT_BEHAVIOUR'         	,  20, 1,1)
        

       -- ---------------------------------------------------------------------
       -- Qipu.de jobs
       -- ---------------------------------------------------------------------
       ,(  74,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_CLICKS'                         		,  10, 1,1)
       ,(  75,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_MERCHANTS'                      		,  20, 1,1)
       ,(  76,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_NETWORKS'                       		,  30, 1,1)
       ,(  77,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_TRANSACTION_STATES'             		,  40, 1,1)
	   ,(  78,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_TRANSACTION_STATES_VALIDATION' 			,  50, 1,1)
       ,(  79,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_USERS'                          		,  60, 1,1)
	   ,(  80,  'QIPU_NIGHTLY'    , 'JOB', 'load_stg_QIPU_DE_CB_USERS_VISITS'                      		,  70, 1,1)
       ,(  81,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_DIM_MERCHANT_HIST'              		,  80, 1,1)
       ,(  82,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_DIM_MERCHANT'                   		,  90, 1,1)
       ,(  83,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_DIM_NETWORK'                    		, 100, 1,1)
       ,(  84,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_WRK_TRANSACTION'                		, 110, 1,1)
       ,(  85,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_WRK_USER_MAIN'                  		, 120, 1,1)
       ,(  86,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_DIM_USER_HIST'                  		, 130, 1,1)
       ,(  87,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_DIM_USER'                       		, 140, 1,1)
       ,(  88,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_FACT_PURCHASE'                  		, 150, 1,1)
       ,(  89,  'QIPU_NIGHTLY'    , 'JOB', 'load_dwh_QIPU_DE_DI_FACT_CLICK'                     		, 160, 1,1)
	   

	   
	   -- ---------------------------------------------------------------------
       -- Shoop.fr Nightly jobs
       -- ---------------------------------------------------------------------
	   ,(  90,  'SHOOP_FR_NIGHTLY'            	  , 'JOB_GROUP', 'SHOOP_FR_NETWORKS'                	,  10, 1,1)
	   ,(  91,  'SHOOP_FR_NIGHTLY'            	  , 'JOB_GROUP', 'SHOOP_FR_MERCHANTS'                	,  20, 1,1)
       ,(  92,  'SHOOP_FR_NIGHTLY'            	  , 'JOB_GROUP', 'SHOOP_FR_WRK_TRANSACTION'             ,  30, 1,1)
	   ,(  93,  'SHOOP_FR_NIGHTLY'            	  , 'JOB_GROUP', 'SHOOP_FR_WRK_USER_MAIN'               ,  40, 1,1)
       ,(  94,  'SHOOP_FR_NIGHTLY'            	  , 'JOB_GROUP', 'SHOOP_FR_WRK_USER_REG_SOURCE'	        ,  50, 1,1)
	   ,(  95,  'SHOOP_FR_NIGHTLY'            	  , 'JOB_GROUP', 'SHOOP_FR_WRK_USER_DEMOGRAPHICS'       ,  60, 1,1)
	   ,(  96,  'SHOOP_FR_NIGHTLY'            	  , 'JOB'      , 'load_dwh_SHOOP_FR_DI_DIM_USER_HIST'   ,  70, 1,1)
       ,(  97,  'SHOOP_FR_NIGHTLY'            	  , 'JOB'      , 'load_dwh_SHOOP_FR_DI_DIM_USER'       	,  80, 1,1)
	   ,(  98,  'SHOOP_FR_NIGHTLY'                , 'JOB'      , 'load_dwh_SHOOP_FR_DI_FACT_PURCHASE'   ,  90, 1,1)
	   ,(  99,  'SHOOP_FR_NIGHTLY'                , 'JOB_GROUP', 'SHOOP_FR_CLICKS'		                , 100, 1,1)
	   

	     -- ---------------------------------------------------------------------
       -- Shoop.fr Independent job groups 
       -- ---------------------------------------------------------------------
	   ,(  100,  'SHOOP_FR_NETWORKS'          	  , 'JOB'      , 'load_stg_SHOOP_FR_CONTENT_NETWORKS'                 	,  10, 1,1)
	   ,(  101,  'SHOOP_FR_NETWORKS'          	  , 'JOB'      , 'load_dwh_SHOOP_FR_DI_DIM_NETWORK'                   	,  20, 1,1)
	 
	   ,(  102,  'SHOOP_FR_MERCHANTS'          	  , 'JOB'      , 'load_stg_SHOOP_FR_CONTENT_MERCHANTS'                	,  10, 1,1)
	   ,(  103,  'SHOOP_FR_MERCHANTS'          	  , 'JOB'      , 'load_dwh_SHOOP_FR_DI_DIM_MERCHANT_HIST'             	,  20, 1,1)
	   ,(  104,  'SHOOP_FR_MERCHANTS'          	  , 'JOB'      , 'load_dwh_SHOOP_FR_DI_DIM_MERCHANT'                  	,  30, 1,1)
	   
	   ,(  105,  'SHOOP_FR_WRK_TRANSACTION'    	  , 'JOB'      , 'load_stg_SHOOP_FR_TRANSACTION_TRANSACTIONS'         	,  10, 1,1)
       ,(  106,  'SHOOP_FR_WRK_TRANSACTION'        , 'JOB'      , 'load_stg_SHOOP_FR_TRANSACTION_STATUS_DATES'         	,  20, 1,1)
	   ,(  107,  'SHOOP_FR_WRK_TRANSACTION'        , 'JOB'      , 'load_dwh_SHOOP_FR_DI_WRK_TRANSACTION'         		,  30, 1,1)
       
  	   ,(  108,  'SHOOP_FR_WRK_USER_MAIN'          , 'JOB'      , 'load_stg_SHOOP_FR_USER_USERS'                       	,  10, 1,1)
  	   ,(  109,  'SHOOP_FR_WRK_USER_MAIN'          , 'JOB'      , 'load_stg_SHOOP_FR_USER_AUTHENTICATION'                ,  20, 1,1)
       ,(  110,  'SHOOP_FR_WRK_USER_MAIN'          , 'JOB'      , 'load_dwh_SHOOP_FR_DI_WRK_USER_MAIN'                 	,  30, 1,1)
  	   
  	   ,(  111,  'SHOOP_FR_WRK_USER_REG_SOURCE'    , 'JOB'      , 'load_stg_SHOOP_FR_USER_USERS'           				,  10, 0,1)
       ,(  112,  'SHOOP_FR_WRK_USER_REG_SOURCE'    , 'JOB'      , 'load_stg_SHOOP_FR_USER_REGISTRATION_SOURCE_TRACKING'	,  20, 1,1)
  	   ,(  113,  'SHOOP_FR_WRK_USER_REG_SOURCE'    , 'JOB'      , 'load_dwh_SHOOP_FR_DI_WRK_USER_REG_SOURCE'           	,  30, 1,1)
  	   
       ,(  114,  'SHOOP_FR_WRK_USER_DEMOGRAPHICS'  , 'JOB'      , 'load_stg_SHOOP_FR_USER_USERS'           				,  10, 0,1)
       ,(  115,  'SHOOP_FR_WRK_USER_DEMOGRAPHICS'  , 'JOB'      , 'load_stg_SHOOP_FR_USER_PROFILES'        				,  20, 1,1)
  	   ,(  116,  'SHOOP_FR_WRK_USER_DEMOGRAPHICS'  , 'JOB'      , 'load_dwh_SHOOP_FR_DI_WRK_USER_DEMOGRAPHICS'         	,  30, 1,1)
  	   
  	   ,(  117,  'SHOOP_FR_CLICKS'  				  , 'JOB'      , 'load_stg_SHOOP_FR_TRACKING_CLICKS'              		,  10, 1,1)
  	   ,(  118,  'SHOOP_FR_CLICKS'                 , 'JOB'      , 'load_dwh_SHOOP_FR_DI_FACT_CLICK'	                    ,  20, 1,1)

    
	 		-- ---------------------------------------------------------------------
        -- Common DWH Nightly Jobs
        -- ---------------------------------------------------------------------
	   ,(  119,  'COMMON_NIGHTLY_DWH'              , 'JOB', 'load_dwh_DI_FACT_USER_LEDGER'                     			,  10, 1,1)

		
	   -- ---------------------------------------------------------------------
       -- MAINTENANCE jobs
       -- ---------------------------------------------------------------------
	   
       ,(  120,  'WEEKLY_MAINTENANCE', 'JOB', 'maintenance_QUIDCO_COM_RELOAD_INCORRECT_TRANSACTION_STATES'      		,  10, 1,1)
       ,(  121,  'WEEKLY_MAINTENANCE', 'JOB', 'load_dwh_QUIDCO_COM_DI_WRK_TRANSACTION'               					,  20, 1,1)
       ,(  122,  'WEEKLY_MAINTENANCE', 'JOB', 'maintenance_QUIDCO_COM_RELOAD_INCORRECT_TRANSACTION_AGGREGATES'  		,  30, 1,1)
       ,(  123,  'WEEKLY_MAINTENANCE', 'JOB', 'load_dwh_QUIDCO_COM_DI_WRK_TRANSACTION'               					,  40, 1,1)
       ,(  124,  'WEEKLY_MAINTENANCE', 'JOB', 'load_dwh_QUIDCO_COM_DI_FACT_PURCHASE'                					,  50, 1,1)	
		
		
       -- ---------------------------------------------------------------------
       -- USER GROUPS jobs
       -- ---------------------------------------------------------------------
       ,(  125,  'USER_GROUPS', 'JOB', 'load_stg_GROUP_QCO_USER_GROUP_CAMPAIGNS'            		,  10, 1,1)
       ,(  126,  'USER_GROUPS', 'JOB', 'load_stg_GROUP_QCO_USER_GROUP_PROGRAMMES'           		,  20, 1,1)
       ,(  127,  'USER_GROUPS', 'JOB', 'load_stg_GROUP_QCO_USER_GROUPS_USERS'               		,  30, 1,1)
       ,(  128,  'USER_GROUPS', 'JOB', 'load_stg_GROUP_QCO_USER_GROUPS'                     		,  40, 1,1)
       ,(  129,  'USER_GROUPS', 'JOB', 'load_dwh_GROUP_DI_DIM_USER_GROUP'                   		,  50, 1,1)
       ,(  130,  'USER_GROUPS', 'JOB', 'load_dwh_GROUP_DI_BRIDGE_USER_GROUP'                		,  60, 1,1)

       -- ---------------------------------------------------------------------
       -- Reports
       -- ---------------------------------------------------------------------
       ,(  131,  'REPORT_EARLY_CLAIM_PURCHASE', 'JOB', 'report_EARLY_CLAIM_PURCHASE'             			,  10, 1,1)
       ,(  132,  'REPORT_FASTER_PAYMENTS'     , 'JOB', 'report_FASTER_PAYMENTS'                  			,  10, 1,1)
       ,(  133,  'REPORT_REGS_MIRROR_PERFORMANCE', 'JOB', 'report_REGS_MIRROR_PERFORMANCE'       			,  10, 1,1)
       ,(  134,  'REPORT_MSM_HEALTHCHECK'     , 'JOB', 'report_MSM_HEALTHCHECK'                  			,  10, 1,1)
       ,(  135,  'REPORT_IPC_PRINT_EOM'       , 'JOB', 'report_IPC_PRINT_EOM'                    			,  10, 1,1)
       ,(  136,  'REPORT_RETENTION_FEE'       , 'JOB', 'report_RETENTION_FEE'                    			,  10, 1,1)
	   ,(  137,  'REPORT_DUPLICATED_INSTORE_TRANSACTIONS', 'JOB', 'report_DUPLICATED_INSTORE_TRANSACTIONS'  ,  10, 1,1)
	   ,(  138,  'REPORT_EXCLUDE_PENDING_COMMISSION', 'JOB', 'report_EXCLUDE_PENDING_COMMISSION'  			,  10, 1,1)
	   ,(  139,  'REPORT_COMMISSIONS_STATUS', 'JOB', 'report_COMMISSIONS_STATUS'  							,  10, 1,1)
	   ,(  140,  'REPORT_LIVE_MERCHANT_RATES', 'JOB', 'report_LIVE_MERCHANT_RATES'  						,  10, 1,1)
	   
	   

       -- ---------------------------------------------------------------------
       -- DMP feeds
       -- ---------------------------------------------------------------------
       ,(  141,  'DMP'        , 'JOB', 'feed_dmp_QUIDCO_COM_DI_DIM_MERCHANT'                     ,  10, 1,1)
       ,(  142,  'DMP'        , 'JOB', 'feed_dmp_QUIDCO_COM_DI_DIM_USER'                         ,  20, 1,1)
       ,(  143,  'DMP'        , 'JOB', 'feed_dmp_QIPU_DE_DI_DIM_MERCHANT'                        ,  30, 1,1)
       ,(  144,  'DMP'        , 'JOB', 'feed_dmp_QIPU_DE_DI_DIM_USER'                            ,  40, 1,1)
       ,(  145,  'DMP'        , 'JOB', 'feed_dmp_QUIDCO_COM_DI_FACT_TRANSACTION'                 ,  50, 1,1)
       ,(  146,  'DMP'        , 'JOB', 'feed_dmp_QIPU_DE_DI_FACT_TRANSACTION'                    ,  60, 1,1)

       -- ---------------------------------------------------------------------
       -- Commons
       -- ---------------------------------------------------------------------
       ,(  147,  'AGGR_USER_SEGMENTS', 'JOB', 'load_dwh_DI_DIM_USER_MONTHLY_SEGMENTS'                    ,  10, 1,1)
	   
;
