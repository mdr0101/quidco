TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_etl_job_params;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_etl_job_params (job_param_id
                                                      ,job_name
                                                      ,job_param_name
                                                      ,value_int
                                                      ,value_string
                                                      ,value_datetime
                                                      ,description)
 VALUES (   3,  'load_stg_QUIDCO_COM_TRANSACTIONS'  , 'last_source_modified',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')
       ,(   4,  'load_stg_QUIDCO_COM_TRANSACTIONS'  , 'threshold'    , 90, NULL, NULL        , 'Last load date staging threshold (minutes)')
       ,(   5,  'load_stg_QIPU_DE_CB_TRANSACTION_STATES'    , 'last_source_modified',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')
       
       ,(   6,  'load_stg_QUIDCO_COM_QCO_CLICKS'            , 'last_click_id',  0, NULL, NULL        , 'Last click id for incremental load to Staging')
       ,(   7,  'load_stg_QUIDCO_COM_QCO_LINK_CLICKS'       , 'last_click_id',  0, NULL, NULL        , 'Last click id for incremental load to Staging')
       ,(   8,  'load_stg_QIPU_DE_CB_CLICKS'                , 'last_click_id',  0, NULL, NULL        , 'Last click id for incremental load to Staging')
       
       ,(   9,  'load_dwh_QUIDCO_COM_DI_DIM_USER'          	, 'last_run_date',  0, NULL, '1900-01-01', 'Last load date for incremental load to Work table')
       ,(  10,  'load_dwh_QIPU_DE_DI_DIM_USER'              , 'last_run_date',  0, NULL, '1900-01-01', 'Last load date for incremental load to Work table')

       ,(  11,  'load_dwh_QUIDCO_COM_DI_FACT_PURCHASE'      , 'last_run_date',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')
       ,(  12,  'load_dwh_QIPU_DE_DI_FACT_PURCHASE'         , 'last_run_date',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')
       ,(  13,  'report_EARLY_CLAIM_PURCHASE'               , 'report_template',  0, 'early_claim_purchase_template.xlsx', NULL, 'Excel report template path')
       ,(  14,  'report_EARLY_CLAIM_PURCHASE'               , 'report_filename_prefix',  0, 'early_claim_purchase', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  15,  'report_EARLY_CLAIM_PURCHASE'               , 'email_recipients',  0, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')

       ,(  16,  'feed_dmp_QUIDCO_COM_DI_DIM_MERCHANT'       , 'last_run_date',  0, NULL, '1900-01-01 00:00:00', 'Last run date of QUIDCO.COM - DI_DIM_MERCHANT incremental DMP feed export.')
       ,(  17,  'feed_dmp_QUIDCO_COM_DI_DIM_MERCHANT'       , 'feed_filename_prefix',  0, 'di_dim_merchant', NULL, 'Last run date of QUIDCO.COM - DI_DIM_MERCHANT incremental DMP feed export.')
       ,(  18,  'feed_dmp_QUIDCO_COM_DI_DIM_MERCHANT'       , 'remote_dir',  0, '/ad-hoc-in/feeds/', NULL, 'Target remote directory in the SFTP server.')
       ,(  19,  'feed_dmp_QUIDCO_COM_DI_DIM_USER'           , 'last_run_date',  0, NULL, '1900-01-01 00:00:00', 'Last run date of QUIDCO.COM - DI_DIM_USER incremental DMP feed export.')
       ,(  20,  'feed_dmp_QUIDCO_COM_DI_DIM_USER'           , 'feed_filename_prefix',  0, 'di_dim_user', NULL, 'Last run date of QUIDCO.COM - DI_DIM_USER incremental DMP feed export.')
       ,(  21,  'feed_dmp_QUIDCO_COM_DI_DIM_USER'           , 'remote_dir',  0, '/ad-hoc-in/feeds/', NULL, 'Target remote directory in the SFTP server.')
       ,(  22,  'feed_dmp_QUIDCO_COM_DI_FACT_TRANSACTION'   , 'last_run_date'       ,  0, NULL, '1900-01-01 00:00:00', 'Last run date of QUIDCO.COM - DI_WRK_TRANSACTION incremental DMP feed export.')
       ,(  23,  'feed_dmp_QUIDCO_COM_DI_FACT_TRANSACTION'   , 'feed_filename_prefix',  0, 'di_fact_transaction', NULL, 'Output filename prefix of QUIDCO.COM - DI_WRK_TRANSACTION incremental DMP feed export.')
       ,(  24,  'feed_dmp_QUIDCO_COM_DI_FACT_TRANSACTION'   , 'remote_dir',  0, '/ad-hoc-in/feeds/invalid_transactions', NULL, 'Target remote directory in the SFTP server.')
       ,(  25,  'feed_dmp_QIPU_DE_DI_DIM_MERCHANT'          , 'last_run_date',  0, NULL, '1900-01-01 00:00:00', 'Last run date of QIPU.DE - DI_DIM_USER incremental DMP feed export.')
       ,(  26,  'feed_dmp_QIPU_DE_DI_DIM_MERCHANT'          , 'feed_filename_prefix',  0, 'di_dim_merchant_qipu_de', NULL, 'Last run date of QIPU.DE - DI_DIM_USER incremental DMP feed export.')
       ,(  27,  'feed_dmp_QIPU_DE_DI_DIM_MERCHANT'          , 'remote_dir',  0, '/maplesyrupmediaeu.ad-hoc-in/qipu/feeds', NULL, 'Target remote directory in the SFTP server.')
       ,(  28,  'feed_dmp_QIPU_DE_DI_DIM_USER'              , 'last_run_date',  0, NULL, '1900-01-01 00:00:00', 'Last run date of QIPU.DE - DI_DIM_USER incremental DMP feed export.')
       ,(  29,  'feed_dmp_QIPU_DE_DI_DIM_USER'              , 'feed_filename_prefix',  0, 'di_dim_user_qipu_de', NULL, 'Last run date of QIPU.DE - DI_DIM_USER incremental DMP feed export.')
       ,(  30,  'feed_dmp_QIPU_DE_DI_DIM_USER'              , 'remote_dir',  0, '/maplesyrupmediaeu.ad-hoc-in/qipu/feeds', NULL, 'Target remote directory in the SFTP server.')
       ,(  31,  'feed_dmp_QIPU_DE_DI_FACT_TRANSACTION'      , 'last_run_date'       ,  0, NULL, '1900-01-01 00:00:00', 'Last run date of QIPU.DE - DI_WRK_TRANSACTION incremental DMP feed export.')
       ,(  32,  'feed_dmp_QIPU_DE_DI_FACT_TRANSACTION'      , 'feed_filename_prefix',  0, 'di_fact_transaction_qipu_de', NULL, 'Output filename prefix of QIPU.DE - DI_WRK_TRANSACTION incremental DMP feed export.')
       ,(  33,  'feed_dmp_QIPU_DE_DI_FACT_TRANSACTION'      , 'remote_dir',  0, '/maplesyrupmediaeu.ad-hoc-in/qipu/feeds/invalid_transactions', NULL, 'Target remote directory in the SFTP server.')

       ,(  34,  'load_dwh_QUIDCO_COM_DI_WRK_USER_REG_SOURCE', 'single_reg_source_cutoff_date',  0, NULL, '2015-10-13 00:00:00', 'Start date of Single Registration Sources.')
       ,(  35,  'load_dwh_QUIDCO_COM_DI_WRK_USER_REG_SOURCE', 'single_reg_source_ref_camp_token',  0, 'referral campaign'      , NULL, 'Token in qco_user_domographics.reg_source to identify registration method as REFERRAL_CAMPAIGN')
       ,(  36,  'load_dwh_QUIDCO_COM_DI_WRK_USER_REG_SOURCE', 'single_reg_source_raf_token'     ,  0, 'RAF', NULL, 'Token in qco_user_domographics.reg_source to identify registration method as REFER_A_FRIEND')
       ,(  37,  'load_dwh_QUIDCO_COM_DI_WRK_USER_REG_SOURCE', 'single_reg_source_cobrand_token' ,  0, 'cobrand'       , NULL, 'Token in qco_user_domographics.reg_source to identify registration method as COBRAND.')
	   ,(  38,  'load_dwh_QUIDCO_COM_DI_FACT_PURCHASE'      , 'fast_payment_cutoff_date',20151111, NULL,NULL, 'Start date of Fast Merchant.')
	   ,(  39,  'load_dwh_QUIDCO_COM_DI_WRK_USER_COBRAND'   , 'cobrand_cutoff_date',  20151130, NULL, '2015-11-30 00:00:00', 'Start date of White Label.')
       ,(  40,  'load_stg_GROUP_QCO_USER_GROUPS_USERS'      , 'last_source_modified',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')

       ,(  41,  'report_FASTER_PAYMENTS'                    , 'report_template',  0, 'faster_payments_template.xlsx', NULL, 'Excel report template path')
       ,(  42,  'report_FASTER_PAYMENTS'                    , 'report_filename_prefix',  0, 'faster_payments', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  43,  'report_FASTER_PAYMENTS'                    , 'email_recipients',  0, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')
       ,(  44,  'report_FASTER_PAYMENTS'                    , 'report_start_date_id',  201008010, NULL, NULL, 'List of recipient email addresses')

       ,(  45,  'report_REGS_MIRROR_PERFORMANCE'            , 'report_template',  0, 'regs_mirror_performance.xlsx', NULL, 'Excel report template path')
       ,(  46,  'report_REGS_MIRROR_PERFORMANCE'            , 'report_filename_prefix',  0, 'regs_mirror_performance', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  47,  'report_REGS_MIRROR_PERFORMANCE'            , 'email_recipients',  0, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')
       ,(  48,  'report_REGS_MIRROR_PERFORMANCE'            , 'commission_per_actives', 15, NULL, NULL, 'Amount of commission per actives')
       
       ,(  49,  'report_MSM_HEALTHCHECK'                    , 'email_recipients', 15, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')

       ,(  50,  'report_IPC_PRINT_EOM'                      , 'report_template',  0, 'ipc_print_eom_template.xlsx', NULL, 'Excel report template path')
       ,(  51,  'report_IPC_PRINT_EOM'                      , 'report_filename_prefix',  0, 'ipc_print_eom', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  52,  'report_IPC_PRINT_EOM'                      , 'email_recipients',  0, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')
 
       ,(  53,  'report_RETENTION_FEE'                      , 'report_template',  0, 'retention_fee_template.xlsx', NULL, 'Excel report template path')
       ,(  54,  'report_RETENTION_FEE'                      , 'report_filename_prefix',  0, 'retention_fee', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  55,  'report_RETENTION_FEE'                      , 'email_recipients',  0, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')
	   ,(  56,  'load_dwh_SHOOP_FR_DI_DIM_USER'             , 'last_run_date',  0, NULL, '1971/01/01 00:00:00', 'Last load date for incremental load to Work table')
	   ,(  57,  'load_stg_SHOOP_FR_TRACKING_CLICKS'            , 'last_click_id',  0, NULL, NULL        , 'Last click id for incremental load to Staging')
	   ,(  58,  'load_stg_SHOOP_FR_TRANSACTION_TRANSACTIONS'  , 'last_source_modified',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')
       ,(  59,  'load_stg_SHOOP_FR_TRANSACTION_TRANSACTIONS'  , 'threshold'    , 30, NULL, NULL        , 'Last load date staging threshold (minutes)')
	   ,(  60,  'load_dwh_SHOOP_FR_DI_FACT_PURCHASE'      , 'last_run_date',  0, NULL, '1900-01-01', 'Last load date for incremental load to Staging')

	   ,(  61,  'load_dwh_QUIDCO_COM_DI_WRK_USER_MERCHANT_CLUSTER_FRAGMENTS', 'last_run_date',  0, NULL, '1900-01-01', 'Last run date for incremental scoring')
	   ,(  62,  'load_dwh_QUIDCO_COM_DI_WRK_USER_SEGMENT_BEHAVIOUR', 'last_run_date',  0, NULL, '1900-01-01', 'Last run date for incremental scoring')

	   ,(  63,  'load_dwh_DI_DIM_USER_MONTHLY_SEGMENTS', 'last_processed_month',  201606, NULL, NULL, 'Last processed month')
	   ,(  64,  'load_stg_WALLET_USER_LEDGERS','last_id',  0, NULL,0, 'Last ID for incremental load to Staging')
       ,(  65,  'report_DUPLICATED_INSTORE_TRANSACTIONS', 'report_template',  0, 'duplicated_rtv_transactions_template.xlsx', NULL, 'Excel report template path')
       ,(  66,  'report_DUPLICATED_INSTORE_TRANSACTIONS', 'report_filename_prefix',  0, 'duplicated_instore_transactions', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  67,  'report_DUPLICATED_INSTORE_TRANSACTIONS', 'email_recipients',  0, 'm.barnett@quidco.com; m.corbett@quidco.com; l.mcdermott@quidco.com; d.butterell@quidco.com; l.ghafur@quidco.com; j.neal@quidco.com; m.murali@quidco.com; d.malik@quidco.com; peter.kosztolanyi@quidco.com', NULL, 'List of recipient email addresses')
	   ,(  68,  'report_COMMISSIONS_STATUS', 'report_template',  0, 'commissions_status_template.xlsx', NULL, 'Excel report template path')
       ,(  69,  'report_COMMISSIONS_STATUS', 'report_filename_prefix',  0, 'commissions_status', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  70,  'report_COMMISSIONS_STATUS', 'email_recipients',  0, 'm.rubin@quidco.com; m.murali@quidco.com; d.malik@quidco.com; peter.kosztolanyi@quidco.com', NULL, 'List of recipient email addresses')
	   ,(  71,  'report_EXCLUDE_PENDING_COMMISSION','email_recipients',  0, 'm.murali@quidco.com;peter.kosztolanyi@quidco.com;d.malik@quidco.com',NULL, 'List of email recipients')
	   ,(  72,  'report_EXCLUDE_PENDING_COMMISSION','report_filename_prefix',  0, 'exclude_pending_commission',NULL, 'Timestamp and file extension will be appended automatically to the final filename')
	   ,(  73,  'report_EXCLUDE_PENDING_COMMISSION','report_template',  0, 'exclude_pending_commission_template.xlsx',NULL, 'Excel report template path')
	   ,(  74,  'report_EXCLUDE_PENDING_COMMISSION', 'start_date',  20160801, NULL, NULL, 'Start Date for the report to process')
       ,(  75,  'report_LIVE_MERCHANT_RATES'                , 'report_template',  0, 'live_merchant_rates_template.xlsx', NULL, 'Excel report template path')
       ,(  76,  'report_LIVE_MERCHANT_RATES'                , 'report_filename_prefix',  0, 'live_merchant_rates', NULL, 'Timestamp and file extension will be appended automatically to the final filename')
       ,(  77,  'report_LIVE_MERCHANT_RATES'                , 'email_recipients',  0, 'di_dev@quidco.com', NULL, 'List of recipient email addresses')
       ,(  78,  'report_LIVE_MERCHANT_RATES'                , 'report_sql',  0, 'SELECT DISTINCT a.commission_id
               ,a.merchant_id
               ,b.domain_merchant_name
               ,b.merchant_hierarchy_l2_name 
               ,b.merchant_hierarchy_l3_name
               ,b.merchant_hierarchy_l4_name
               ,a.start start_date
               ,a.end end_date
               ,a.text
               ,a.network_amount
               ,a.amount
FROM quidco_mambo.qco_commissions a 
     JOIN di_dim_merchant b 
            ON b.domain_merchant_id = a.merchant_id
           AND b.di_domain_id = 1 
           AND b.merchant_hierarchy_l2_id != -1
     JOIN quidco_mambo.qco_merchants m
            ON m.merchant_id = a.merchant_id
           AND m.live = ''yes''
     LEFT JOIN (SELECT DISTINCT domain_merchant_id
                  FROM di_dim_merchant
                 WHERE di_domain_id = 1
                   AND domain_merchant_name Like ''%.pl%'') cc 
            ON cc.domain_merchant_id = a.merchant_id
 WHERE (a.start IS NULL OR a.start < NOW())
   AND (a.end   IS NULL OR a.end > NOW())
   AND  a.amount > 0
   AND  a.network_amount > 0
   AND (a.`text` NOT LIKE ''%zakupy%'' AND a.`text` NOT LIKE ''%wszystkie%'')
   AND  cc.domain_merchant_id IS NULL
 ORDER BY domain_merchant_name', NULL, 'SQL query report')
 ,(  79,  'load_dwh_QUIDCO_COM_DI_WRK_TRANSACTION'      , 'fast_payment_cutoff_date',  20151111, NULL,NULL, 'Start date of Fast Merchant.')
;
