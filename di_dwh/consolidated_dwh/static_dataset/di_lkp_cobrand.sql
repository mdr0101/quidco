TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_cobrand;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_cobrand (	di_lkp_cobrand_id,
														di_domain_id,
														cobrand_name,
														is_included,
														is_included_comment,
														di_created_date,
														job_run_id)
 VALUES ('1','1','nus',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('2','1','candis',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('3','1','payday',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('4','1','avforums',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('5','1','cobrand',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('6','1','countdown',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('7','1','omnicom',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('8','1','chesterracecompany',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('9','1','s1',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('10','1','Quidco',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('11','1','GoodToKnow Voucher codes',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('12','1','magnetise_media',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('13','1','GoodToKnow Cashback',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
	,('14','1','GoodToKnow Printable vouchers',0,'bad data: flaged as 0','2015-11-10 17:06:30','-1')
;