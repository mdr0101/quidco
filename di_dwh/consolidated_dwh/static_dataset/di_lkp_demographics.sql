 TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_demographics;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_demographics (di_demographics_lkp_id
														,di_domain_id
														,lkp_item
														,category_id
														,category_code
														,category_name
														,gt
														,lt
														,di_created_date)
 VALUES  (1, 1, 'AGE_BAND', -1, NULL, 'Unknown Age', 0, 18, '1971-01-01')
		,(2, 1, 'AGE_BAND', 1, NULL, '18 to 24', 18, 25, '1971-01-01')	
		,(3, 1, 'AGE_BAND', 2, NULL, '25 to 34', 24, 35, '1971-01-01')	
		,(4, 1, 'AGE_BAND', 3, NULL, '35 to 44', 34, 45, '1971-01-01')	
		,(5, 1, 'AGE_BAND', 4, NULL, '45 to 54', 44, 55, '1971-01-01')	
		,(6, 1, 'AGE_BAND', 5, NULL, '55 to 64', 54, 65, '1971-01-01')	
		,(7, 1, 'AGE_BAND', 6, NULL, 'over 65', 64, 255, '1971-01-01')
		
		,(8, 1, 'INCOME_BAND', -1, NULL, 'Unknown', 0, 0, '1971-01-01')
		,(9, 1, 'INCOME_BAND', 1, NULL, '£0 - £9,999', 0, 10000, '1971-01-01')
		,(10, 1, 'INCOME_BAND', 2, NULL, '£10,000 - £16,999', 9999, 17000, '1971-01-01')
		,(11, 1, 'INCOME_BAND', 3, NULL, '£17,000 - £34,999', 16999, 35000, '1971-01-01')
		,(12, 1, 'INCOME_BAND', 4, NULL, '£35,000 - £54,999', 34999, 55000, '1971-01-01')
		,(13, 1, 'INCOME_BAND', 5, NULL, '£55,000 - £69,000', 54999, 70000, '1971-01-01')		
		,(14, 1, 'INCOME_BAND', 6, NULL, 'Over £70,000', 69, 1000000000, '1971-01-01')
		,(15, 1, 'INCOME_BAND', 7, NULL, 'Prefer not to say', 0, 0, '1971-01-01')
		,(16, 1, 'OCCUPATION', 1, 'A', 'Agriculture', NULL, NULL, '1971-01-01')
		,(17, 1, 'OCCUPATION', 2, 'B', 'Business Manager', NULL, NULL, '1971-01-01')
		,(18, 1, 'OCCUPATION', 3, 'C', 'Homemaker', NULL, NULL, '1971-01-01')
		,(19, 1, 'OCCUPATION', 4, 'D', 'Director', NULL, NULL, '1971-01-01')
		,(20, 1, 'OCCUPATION', 5, 'E', 'Manual/Factory Worker', NULL, NULL, '1971-01-01')
		,(21, 1, 'OCCUPATION', 6, 'F', 'Unemployed', NULL, NULL, '1971-01-01')
		,(22, 1, 'OCCUPATION', 7, 'G', 'Office/Clerical Worker', NULL, NULL, '1971-01-01')
		,(23, 1, 'OCCUPATION', 8, 'H', 'Professional Acnt/Lawyer/Snr Mgr', NULL, NULL, '1971-01-01')
		,(24, 1, 'OCCUPATION', 9, 'I', 'Professional Education/Medical', NULL, NULL, '1971-01-01')
		,(25, 1, 'OCCUPATION', 10, 'J', 'Public Sector', NULL, NULL, '1971-01-01')
		,(26, 1, 'OCCUPATION', 11, 'K', 'Retail/Shop Worker', NULL, NULL, '1971-01-01')
		,(27, 1, 'OCCUPATION', 12, 'L', 'Retired', NULL, NULL, '1971-01-01')
		,(28, 1, 'OCCUPATION', 13, 'M', 'Student', NULL, NULL, '1971-01-01')
		,(29, 1, 'OCCUPATION', 14, 'N', 'Trade/Craftsman', NULL, NULL, '1971-01-01')
		,(30, 1, 'OCCUPATION', 15, 'O', 'Other', NULL, NULL, '1971-01-01')
		
		,(31, 200, 'AGE_BAND', -1, NULL, 'Unknown Age', 0, 18, '1971-01-01')
		,(32, 200, 'AGE_BAND', 1, NULL, '18 to 24', 18, 25, '1971-01-01')	
		,(33, 200, 'AGE_BAND', 2, NULL, '25 to 34', 24, 35, '1971-01-01')	
		,(34, 200, 'AGE_BAND', 3, NULL, '35 to 44', 34, 45, '1971-01-01')	
		,(35, 200, 'AGE_BAND', 4, NULL, '45 to 54', 44, 55, '1971-01-01')	
		,(36, 200, 'AGE_BAND', 5, NULL, '55 to 64', 54, 65, '1971-01-01')	
		,(37, 200, 'AGE_BAND', 6, NULL, 'over 65', 64, 255, '1971-01-01')
;