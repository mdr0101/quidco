TRUNCATE TABLE di_lkp_msm_healthcheck;
INSERT INTO di_lkp_msm_healthcheck (msm_healthcheck_id
                                                           ,check_name
                                                           ,check_sql_output_type
                                                           ,check_sql
                                                           ,expected_number
                                                           ,percentage_threshold
                                                           ,expected_string
                                                           ,di_created_date)
 VALUES  ( 1, '(Quidco) - Raw transaction states'        ,'NUMBER', 'SELECT COUNT(*) FROM di_staging.stg_quidco_com_qco_transbeta_states', 700000, 50, NULL, '1971-01-01')
        ,( 2, '(Quidco) - Purchases'                     ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 1 AND purchase_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 20000, 30, NULL, '1971-01-01')
        ,( 3, '(Quidco) - Invalid Purchases'             ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 1 AND is_valid = 0 AND purchase_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 4000, 30, NULL, '1971-01-01')
        ,( 4, '(Quidco) - Outlier Purchases'             ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 1 AND is_outlier = 1 AND purchase_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 1, 500, NULL, '1971-01-01')
        ,( 5, '(Quidco) - Non-Cashback Transactions'     ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 1 AND is_noncashback = 1 AND purchase_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 100, 100, NULL, '1971-01-01')
        ,( 6, '(Quidco) - In-store Transactions'         ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase p, di_dim_merchant m WHERE m.di_merchant_id = p.di_merchant_id AND p.di_domain_id = 1 AND m.is_instore = 1 AND purchase_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 3000, 200, NULL, '1971-01-01')
        ,( 7, '(Quidco) - Clicks'                        ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_click WHERE di_domain_id = 1 AND click_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 80000, 30, NULL, '1971-01-01')
        ,( 8, '(Quidco) - Registrations'                 ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 1 AND registration_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 2000, 30, NULL, '1971-01-01')
        ,( 9, '(Quidco) - Authenticated Regs'            ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 1 AND is_authenticated = 1 AND registration_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 1500, 30, NULL, '1971-01-01')
        ,(10, '(Quidco) - Unmapped Regs to Reg Sources'  ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 1 AND registration_source_l1_id = -1 AND registration_date_id = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 0, 0, NULL, '1971-01-01')
        ,(11, '(Quidco) - Mailable Users'                ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 1 AND is_mailable = 1', 1500000, 20, NULL, '1971-01-01')
        ,(12, '(Quidco) - Cobrand Users'                 ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 1 AND is_cobrand = 1', 1800000, 20, NULL, '1971-01-01')
        ,(13, '(Quidco) - Networks'                      ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_network WHERE di_domain_id = 1 ', 100, 20, NULL, '1971-01-01')
        ,(14, '(Quidco) - Merchants'                     ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 1 ', 12000, 10, NULL, '1971-01-01')
        ,(15, '(Quidco) - Instore Merchants'             ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 1 AND is_instore = 1', 120, 10, NULL, '1971-01-01')
        ,(16, '(Quidco) - Unmapped Merchants to Hierarhy','NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 1 AND merchant_cluster_id = -1', 6000, 15, NULL, '1971-01-01')
        ,(17, '(Quidco) - Unmapped Merchants to Clusters','NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 1 AND merchant_hierarchy_l1_id = 0', 3500, 10, NULL, '1971-01-01')
        ,(18, '(Quidco) - Page Impressions'              ,'NUMBER', 'SELECT COUNT(*) FROM quidco_cdn_analytics_dw.di_page_impression WHERE page_impression_datetime > DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), "%Y%m%d")', 1000000, 30, NULL, '1971-01-01')
        
        ,(19, '(Qipu) - Raw transaction states'          ,'NUMBER', 'SELECT COUNT(*) FROM di_staging.stg_qipu_de_cb_transaction_states', 20000, 30, NULL, '1971-01-01')
        ,(20, '(Qipu) - Purchases'                       ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 2 AND purchase_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 4000, 30, NULL, '1971-01-01')
        ,(21, '(Qipu) - Invalid Purchases'               ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 2 AND is_valid = 0 AND purchase_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 100, 150, NULL, '1971-01-01')
        ,(22, '(Qipu) - Outlier Purchases'               ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_purchase WHERE di_domain_id = 1 AND is_outlier = 2 AND purchase_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 1, 500, NULL, '1971-01-01')
        ,(23, '(Qipu) - Clicks'                          ,'NUMBER', 'SELECT COUNT(*) FROM di_fact_click WHERE di_domain_id = 2 AND click_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 15000, 30, NULL, '1971-01-01')
        ,(24, '(Qipu) - Registrations'                   ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 2 AND registration_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 300, 30, NULL, '1971-01-01')
        ,(25, '(Qipu) - Authenticated Regs'              ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 2 AND is_authenticated = 1 AND registration_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 250, 30, NULL, '1971-01-01')
        ,(26, '(Qipu) - Unmapped Reg Sources'            ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_user WHERE di_domain_id = 2 AND registration_source_l1_id = -1 AND registration_date_id = DATE_FORMAT(NOW(), "%Y%m%d") - 1', 0, 0, NULL, '1971-01-01')
        ,(27, '(Qipu) - Total Networks'                  ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_network WHERE di_domain_id = 2', 70, 20, NULL, '1971-01-01')
        ,(28, '(Qipu) - Total Merchants'                 ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 2', 4000, 10, NULL, '1971-01-01')
        ,(29, '(Qipu) - Unmapped Merchant Hierarhy'      ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 2 AND merchant_cluster_id = -1', 0, 30, NULL, '1971-01-01')
        ,(30, '(Qipu) - Instore Merchants'               ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 2 AND is_instore = 1', 0, 0, NULL, '1971-01-01')
        ,(31, '(Qipu) - Unmapped Merchant Clusters'      ,'NUMBER', 'SELECT COUNT(*) FROM di_dim_merchant WHERE di_domain_id = 2 AND merchant_hierarchy_l1_id = 0', 3500, 10, NULL, '1971-01-01')
        
        ,(32, 'ETL - Quidco Nightly'                     ,'STRING', 'SELECT CASE COUNT(*) WHEN 0 THEN ''NOT FINISHED'' ELSE job_status END FROM di_etl_job_log WHERE start_datetime > DATE(NOW()) AND job_name = ''load_dwh_QUIDCO_COM_DI_FACT_CLICK'' ORDER BY 1 DESC LIMIT 1', NULL, NULL, 'SUCCESS', '1971-01-01')
        ,(33, 'ETL - Qipu Nightly'                       ,'STRING', 'SELECT CASE COUNT(*) WHEN 0 THEN ''NOT FINISHED'' ELSE job_status END FROM di_etl_job_log WHERE start_datetime > DATE(NOW()) AND job_name = ''load_dwh_QIPU_DE_DI_FACT_CLICK'' ORDER BY 1 DESC LIMIT 1', NULL, NULL, 'SUCCESS', '1971-01-01')
        ,(34, 'ETL - User groups daily'                  ,'STRING', 'SELECT CASE COUNT(*) WHEN 0 THEN ''NOT FINISHED'' ELSE job_status END FROM di_etl_job_log WHERE start_datetime > DATE(NOW() - INTERVAL 1 DAY) AND job_name = ''load_dwh_GROUP_DI_BRIDGE_USER_GROUP'' ORDER BY 1 DESC LIMIT 1', NULL, NULL, 'SUCCESS', '1971-01-01')
        ,(35, 'ETL - DMP daily export'                   ,'STRING', 'SELECT CASE COUNT(*) WHEN 0 THEN ''NOT FINISHED'' ELSE job_status END FROM di_etl_job_log WHERE start_datetime > DATE(NOW()) AND job_name = ''feed_dmp_QIPU_DE_DI_FACT_TRANSACTION'' ORDER BY 1 DESC LIMIT 1', NULL, NULL, 'SUCCESS', '1971-01-01')
        ,(36, 'ETL - Shoop FR Nightly'                   ,'STRING', 'SELECT CASE COUNT(*) WHEN 0 THEN ''NOT FINISHED'' ELSE job_status END FROM di_etl_job_log WHERE start_datetime > DATE(NOW()) AND job_name = ''load_dwh_SHOOP_FR_DI_DIM_USER'' ORDER BY 1 DESC LIMIT 1', NULL, NULL, 'SUCCESS', '1971-01-01')
;
