TRUNCATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_registration_source_hierarchy;
INSERT INTO ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_registration_source_hierarchy (di_lkp_registration_source_hierarchy_id
                                                                         ,di_domain_id
                                                                         ,registration_method_id
                                                                         ,registration_method_name
                                                                         ,registration_method_attr_1
                                                                         ,registration_method_attr_2
                                                                         ,registration_method_attr_3
                                                                         ,registration_source_l1_id
                                                                         ,registration_source_l1_name
                                                                         ,registration_source_l2_id
                                                                         ,registration_source_l2_name
                                                                         ,registration_source_l3_id
                                                                         ,registration_source_l3_name
                                                                         ,registration_source_l4_id
                                                                         ,registration_source_l4_name
                                                                         ,di_created_date
                                                                         ,job_run_id)
 VALUES ( 1, 1, 1, 'ORGANIC'          , '', '', '', -1, 'Unknown'        , -1, 'Unknown'      , -1, 'Unknown'                 ,  -1, 'Unknown'              , '1971-01-01', -1)
       ,( 2, 1, 2, 'REFERRAL_CAMPAIGN', '', '', '', -1, 'Unknown'        , -1, 'Unknown'      , -1, 'Unknown'                 ,  -1, 'Unknown'              , '1971-01-01', -1)
       ,( 3, 1, 3, 'REFER_A_FRIEND'   , '', '', '', -1, 'Unknown'        , -1, 'Unknown'      , -1, 'Unknown'                 ,  -1, 'Unknown'              , '1971-01-01', -1)
       ,( 4, 1, 4, 'COBRAND'          , '', '', '', -1, 'Unknown'        , -1, 'Unknown'      , -1, 'Unknown'                 ,  -1, 'Unknown'              , '1971-01-01', -1)
 ;
