DELIMITER //
DROP FUNCTION IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.fn_get_gaussian_kernal;
CREATE FUNCTION ${DB_DI_DWH_DWH_SCHEMA}.fn_get_gaussian_kernal(p_mean      DOUBLE
                                      ,p_sigma     DOUBLE
                                      ,p_datapoint DOUBLE) RETURNS double
DETERMINISTIC
BEGIN  
  RETURN    COALESCE(   EXP( - POWER(p_datapoint - p_mean, 2)
                          / (2 * POWER(p_sigma, 2)))
                     / (SQRT(2 * PI()) * p_sigma), 0);
END;
