DELIMITER //
DROP FUNCTION IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.get_merchant_hierarchy_mapping//
CREATE FUNCTION ${DB_DI_DWH_DWH_SCHEMA}.get_merchant_hierarchy_mapping(p_di_domain_id               INT
                                                               ,p_domain_merchant_id         INT
                                                               ,p_merchant_hierarchy_l4_id   INT
                                                               ) RETURNS INT
BEGIN
  -- ---------------------------------------------------------------------------
  -- This is a wrapper function to call stored procedure from a SELECT statement
  -- ---------------------------------------------------------------------------
  CALL set_merchant_hierarchy_mapping(p_di_domain_id
                                     ,p_domain_merchant_id
                                     ,p_merchant_hierarchy_l4_id);

  RETURN 0;
END;
//
DELIMITER ;