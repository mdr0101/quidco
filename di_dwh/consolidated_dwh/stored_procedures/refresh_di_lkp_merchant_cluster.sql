DELIMITER //
DROP PROCEDURE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.refresh_di_lkp_merchant_cluster;
CREATE PROCEDURE ${DB_DI_DWH_DWH_SCHEMA}.refresh_di_lkp_merchant_cluster(p_di_domain_id INT)
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE v_di_domain_id, v_domain_merchant_id, v_valid_purchases INT;
  DECLARE x_rfv_subgroup_id, x_sample_size INT;
  DECLARE v_sample_size_total
         ,v_sample_size_rfv_51
         ,v_sample_size_rfv_52
         ,v_sample_size_rfv_53
         ,v_sample_size_rfv_54
         ,v_sample_size_rfv_55
         ,v_sample_size_rfv_56
         ,v_sample_size_rfv_61
         ,v_sample_size_rfv_62
         ,v_sample_size_rfv_63
         ,v_sample_size_rfv_64
         ,v_sample_size_rfv_65
         ,v_sample_size_rfv_66
         ,v_sample_size_rfv_71
         ,v_sample_size_rfv_72
         ,v_sample_size_rfv_73
         ,v_sample_size_rfv_74
         ,v_sample_size_rfv_75
         ,v_sample_size_rfv_76 INT;
  
  -- Cursos to select every unclassified merchant with more than 100 purchases
  DECLARE cur1 CURSOR FOR SELECT m.di_domain_id
                                ,m.domain_merchant_id
                                ,COUNT(*) valid_purchases
                            FROM di_fact_purchase p
                                ,di_dim_merchant m
                                 LEFT JOIN di_lkp_merchant_cluster mc
                                        ON mc.di_domain_id = m.di_domain_id
                                       AND mc.domain_merchant_id = m.domain_merchant_id
                           WHERE m.di_merchant_id = p.di_merchant_id
                             AND m.di_domain_id != -1
                             AND m.merchant_cluster_id = -1
                             AND p.purchase_date_id > DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 YEAR), '%Y%m%d')
                             AND p.is_valid = 1
                             AND p.di_domain_id = p_di_domain_id
                             AND mc.di_lkp_merchant_cluster_id IS NULL
                           GROUP BY m.di_merchant_id   
                                   ,m.domain_merchant_id
                           HAVING COUNT(*) > 100;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  -- Truncate intermediate work tables
  TRUNCATE TABLE di_wrk_merchant_cluster_rfv_purchasers;
  TRUNCATE TABLE di_wrk_merchant_cluster_rfv_sample;
  
  -- Foreach the unclassified merchants and allocate to merchant cluster one by one
  OPEN cur1;
  read_loop: LOOP
    FETCH cur1 INTO v_di_domain_id, v_domain_merchant_id, v_valid_purchases;
    
      -- Finish if there is no more unclassified merchant
      IF done THEN
        LEAVE read_loop;
      END IF;

    -- Populate purchasers of the current merchant
    INSERT INTO di_wrk_merchant_cluster_rfv_purchasers(di_domain_id
                                                      ,domain_merchant_id
                                                      ,domain_user_id
                                                      ,rfv_subgroup_id
                                                      ,di_created_date)
    SELECT DISTINCT p.di_domain_id
          ,p.domain_merchant_id
          ,u.domain_user_id
          ,u.rfv_subgroup_id
          ,NOW()
      FROM di_fact_purchase p
          ,di_dim_user u
     WHERE u.di_user_id = p.di_user_id
       AND p.di_domain_id = v_di_domain_id
       AND p.is_valid = 1
       AND p.domain_merchant_id = v_domain_merchant_id;
       
    -- Get sample user sizes accross every RFV segment
    SET @sample_size_multiplier = 10;
    SELECT sample_size_total
          ,SUM(CASE WHEN p.rfv_subgroup_id = 51 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_51
          ,SUM(CASE WHEN p.rfv_subgroup_id = 52 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_52
          ,SUM(CASE WHEN p.rfv_subgroup_id = 53 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_53
          ,SUM(CASE WHEN p.rfv_subgroup_id = 54 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_54
          ,SUM(CASE WHEN p.rfv_subgroup_id = 55 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_55
          ,SUM(CASE WHEN p.rfv_subgroup_id = 56 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_56
          ,SUM(CASE WHEN p.rfv_subgroup_id = 61 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_61
          ,SUM(CASE WHEN p.rfv_subgroup_id = 62 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_62
          ,SUM(CASE WHEN p.rfv_subgroup_id = 63 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_63
          ,SUM(CASE WHEN p.rfv_subgroup_id = 64 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_64
          ,SUM(CASE WHEN p.rfv_subgroup_id = 65 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_65
          ,SUM(CASE WHEN p.rfv_subgroup_id = 66 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_66
          ,SUM(CASE WHEN p.rfv_subgroup_id = 71 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_71
          ,SUM(CASE WHEN p.rfv_subgroup_id = 72 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_72
          ,SUM(CASE WHEN p.rfv_subgroup_id = 73 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_73
          ,SUM(CASE WHEN p.rfv_subgroup_id = 74 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_74
          ,SUM(CASE WHEN p.rfv_subgroup_id = 75 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_75
          ,SUM(CASE WHEN p.rfv_subgroup_id = 76 THEN 1 ELSE 0 END) * @sample_size_multiplier sample_size_rfv_76
     INTO v_sample_size_total
         ,v_sample_size_rfv_51
         ,v_sample_size_rfv_52
         ,v_sample_size_rfv_53
         ,v_sample_size_rfv_54
         ,v_sample_size_rfv_55
         ,v_sample_size_rfv_56
         ,v_sample_size_rfv_61
         ,v_sample_size_rfv_62
         ,v_sample_size_rfv_63
         ,v_sample_size_rfv_64
         ,v_sample_size_rfv_65
         ,v_sample_size_rfv_66
         ,v_sample_size_rfv_71
         ,v_sample_size_rfv_72
         ,v_sample_size_rfv_73
         ,v_sample_size_rfv_74
         ,v_sample_size_rfv_75
         ,v_sample_size_rfv_76
      FROM di_wrk_merchant_cluster_rfv_purchasers p
          ,(SELECT COUNT(DISTINCT domain_user_id) * @sample_size_multiplier sample_size_total
              FROM di_wrk_merchant_cluster_rfv_purchasers
             WHERE di_domain_id = v_di_domain_id
               AND domain_merchant_id = v_domain_merchant_id) x
     WHERE p.di_domain_id = v_di_domain_id
       AND p.domain_merchant_id = v_domain_merchant_id;
     
      -- Take random sample of X number of users from each RFV group - Please make me better :((
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 51 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_51;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 52 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_52;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 53 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_53;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 54 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_54;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 55 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_55;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 56 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_56;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 61 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_61;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 62 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_62;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 63 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_63;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 64 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_64;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 65 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_65;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 66 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_66;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 71 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_71;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 72 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_72;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 73 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_73;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 74 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_74;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 75 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_75;
      INSERT INTO di_wrk_merchant_cluster_rfv_sample(di_domain_id, domain_merchant_id, domain_user_id, rfv_subgroup_id, di_created_date) SELECT di_domain_id, v_domain_merchant_id, domain_user_id, rfv_subgroup_id, NOW() FROM (SELECT di_domain_id, domain_user_id, rfv_subgroup_id, @rownum := @rownum + 1 rank FROM (SELECT u.di_domain_id, u.domain_user_id, u.rfv_subgroup_id FROM di_dim_user u LEFT JOIN di_wrk_merchant_cluster_rfv_purchasers p ON p.di_domain_id = u.di_domain_id AND p.domain_user_id = u.domain_user_id AND p.domain_merchant_id = v_domain_merchant_id, (SELECT @rownum := 0) init_vars WHERE u.di_domain_id = 1 AND u.is_cobrand = 0 AND u.rfv_subgroup_id = 76 AND p.domain_user_id IS NULL ORDER BY RAND()) c) d WHERE rank <= v_sample_size_rfv_76; 
      
      -- Allocate merchant to merchant cluster and insert into DI_LKP_MERCHANT_CLUSTER
      INSERT INTO di_lkp_merchant_cluster(di_domain_id
                                         ,domain_merchant_id
                                         ,merchant_cluster_id
                                         ,merchant_cluster_name
                                         ,penetration
                                         ,di_created_date)
      SELECT di_domain_id
            ,domain_merchant_id
            ,merchant_cluster_id
            ,merchant_cluster_name
            ,penetration
            ,NOW()
        FROM (SELECT p.di_domain_id
                    ,p.domain_merchant_id
                    ,p.merchant_cluster_id
                    ,p.merchant_cluster_name
                    ,CASE
                       WHEN p.users > (v_sample_size_total / @sample_size_multiplier) / 3 THEN (p.users / (v_sample_size_total / @sample_size_multiplier)) / (CASE WHEN sp.users IS NULL THEN 0.01 ELSE sp.users / v_sample_size_total END)
                       ELSE 0
                     END penetration
                FROM (SELECT p.di_domain_id
                            ,rfv_p.domain_merchant_id
                            ,m.merchant_cluster_id
                            ,m.merchant_cluster_name
                            ,COUNT(DISTINCT p.domain_user_id) users
                        FROM di_fact_purchase p
                            ,di_dim_merchant m
                            ,di_wrk_merchant_cluster_rfv_purchasers rfv_p
                       WHERE p.di_domain_id = p_di_domain_id
                         AND p.domain_user_id = rfv_p.domain_user_id
                         AND m.di_merchant_id = p.di_merchant_id
                         AND m.merchant_cluster_id != -1
                         AND p.is_valid = 1
                         AND rfv_p.domain_merchant_id = v_domain_merchant_id
                         AND p.purchase_date_id >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -2 YEAR), '%Y%m%d')
                       GROUP BY m.merchant_cluster_id) p
                     LEFT JOIN (SELECT m.merchant_cluster_id
                                      ,m.merchant_cluster_name
                                      ,COUNT(DISTINCT p.domain_user_id) users
                                  FROM di_fact_purchase p
                                      ,di_dim_merchant m
                                      ,di_wrk_merchant_cluster_rfv_sample rfv_s
                                 WHERE p.di_domain_id = p_di_domain_id
                                   AND p.domain_user_id = rfv_s.domain_user_id
                                   AND m.di_merchant_id = p.di_merchant_id
                                   AND m.merchant_cluster_id != -1
                                   AND p.is_valid = 1
                                   AND rfv_s.domain_merchant_id = v_domain_merchant_id
                                   AND p.purchase_date_id >= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -2 YEAR), '%Y%m%d')
                                 GROUP BY m.merchant_cluster_id) sp
                     ON sp.merchant_cluster_id = p.merchant_cluster_id
              ORDER BY penetration DESC
              LIMIT 1) x;
   END LOOP;
  
  CLOSE cur1;
END;
;