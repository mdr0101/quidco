DELIMITER //
DROP PROCEDURE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.set_merchant_hierarchy_mapping//
CREATE PROCEDURE ${DB_DI_DWH_DWH_SCHEMA}.set_merchant_hierarchy_mapping(p_di_domain_id               INT
                                                                ,p_domain_merchant_id         INT
                                                                ,p_merchant_hierarchy_l4_id   INT
                                                                )
BEGIN  
  DECLARE exc_not_implemented     CONDITION FOR SQLSTATE '45000';
  DECLARE exc_invalid_l4_id       CONDITION FOR SQLSTATE '45001';
  DECLARE exc_invalid_merchant_id CONDITION FOR SQLSTATE '45002';
  DECLARE exc_disabled            CONDITION FOR SQLSTATE '45003';
  DECLARE v_error_string          VARCHAR(500);
  DECLARE v_is_new_merchant       TINYINT       DEFAULT 0;
  DECLARE v_di_domain_name        VARCHAR(20)   DEFAULT di_unknown_str();


  -- ---------------------------------------------------------------------------
  -- Exit handler for invalid merchant id
  -- ---------------------------------------------------------------------------
	DECLARE EXIT HANDLER FOR SQLSTATE '45000'
	BEGIN
      SET v_error_string = CONCAT('Merchant hierarchy mapping is not implemented for this domain: [', p_di_domain_id ,'] - [', v_di_domain_name, ']');
                             
      SIGNAL exc_not_implemented
         SET MESSAGE_TEXT = v_error_string;
  END;

  -- ---------------------------------------------------------------------------
  -- Exit handler for invalid merchant hierarchy level 4
  -- User defined exception to throw it in case of negative hierarchy level 4 ID
  -- ---------------------------------------------------------------------------
	DECLARE EXIT HANDLER FOR SQLSTATE '45001'
	BEGIN
      SET v_error_string = CONCAT('Not existing merchant hierarchy Level 4 ID: [', p_merchant_hierarchy_l4_id ,'].');

      SIGNAL exc_invalid_l4_id
         SET MESSAGE_TEXT = v_error_string;
	END;
  
  -- ---------------------------------------------------------------------------
  -- Exit handler for invalid merchant id
  -- ---------------------------------------------------------------------------
	DECLARE EXIT HANDLER FOR SQLSTATE '45002'
	BEGIN
      SET v_error_string = CONCAT('Not existing merchant ID: [', p_di_domain_name, '] - [', p_domain_merchant_id ,']');

      SIGNAL exc_invalid_merchant_id
         SET MESSAGE_TEXT = v_error_string;
	END;
  
  -- ---------------------------------------------------------------------------
  -- Exit handler for invalid merchant hierarchy level 4
  -- Referential integrity exception - Merchant ID is not existing the op db
  -- ---------------------------------------------------------------------------
	DECLARE EXIT HANDLER FOR SQLSTATE '23000'
	BEGIN
      SET v_error_string = CONCAT('Not existing merchant hierarchy Level 4 ID: [', p_merchant_hierarchy_l4_id ,'].');

      SIGNAL exc_invalid_l4_id
         SET MESSAGE_TEXT = v_error_string;
	END;
  
  -- ---------------------------------------------------------------------------
  -- Exit handler for disabled merchant id
  -- ---------------------------------------------------------------------------
	DECLARE EXIT HANDLER FOR SQLSTATE '45003'
	BEGIN
      SET v_error_string = CONCAT('Merchant hierarchy mapping is currently disabled for this domain: [', p_di_domain_id ,'] - [', v_di_domain_name, ']. Email to di@quidco.com for more details.');
                             
      SIGNAL exc_disabled
         SET MESSAGE_TEXT = v_error_string;
  END;
  
  
  
  -- ---------------------------------------------------------------------------
  -- Raise an error if merchant ID or hierarchy level 4 is lower than zero
  --
  -- This is not handled by referential integrity check as every minus value is
  -- converted to zero
  -- ---------------------------------------------------------------------------
  IF p_domain_merchant_id < 0 THEN
    SIGNAL exc_invalid_merchant_id;
  END IF;
  
  IF p_merchant_hierarchy_l4_id < 0 THEN
    SIGNAL exc_invalid_l4_id;
  END IF;
  
  -- ---------------------------------------------------------------------------
  -- Get domain name
  -- ---------------------------------------------------------------------------
  SELECT COALESCE(di_domain_name, di_unknown_str())
    INTO v_di_domain_name
    FROM di_dim_domain
   WHERE di_domain_id = p_di_domain_id;
      

  -- ---------------------------------------------------------------------------
  -- Update the hierarchy mapping
  -- ---------------------------------------------------------------------------
  CASE v_di_domain_name

    -- -------------------------------------------------------------------------
    -- QUIDCO.COM
    -- -------------------------------------------------------------------------
    WHEN 'quidco.com' THEN

      -- Check Merchant Id validity
      SELECT CASE
               WHEN COUNT(*) > 0 THEN 0
               ELSE 1
             END
        INTO v_is_new_merchant
        FROM di_dim_merchant
       WHERE di_domain_id = p_di_domain_id
         AND domain_merchant_id = p_domain_merchant_id;
       
      /*
      IF v_is_valid_merchant_id = 0 THEN                            
        SIGNAL exc_invalid_merchant_id;
      END IF;
      */
       
      -- Update the legacy remote tables in CDN Scoring for backward compatibility
      /*
      UPDATE quidco_di_extract.lnk_cdn_scoring_di_merchant_category
         SET di_merchant_category_l4_id = p_merchant_hierarchy_l4_id
       WHERE merchant_id = p_domain_merchant_id;
      */
      
      -- SIGNAL exc_disabled;
      
      -- Insert new mapping
      INSERT INTO di_cfg_merchant_hierarchy_mapping (di_domain_id
                                                    ,domain_merchant_id
                                                    ,merchant_hierarchy_l4_id
                                                    ,di_created_date
                                                    ,di_last_updated_date)
      SELECT d.di_domain_id
            ,p_domain_merchant_id
            ,p_merchant_hierarchy_l4_id
            ,NOW()
            ,NOW()
        FROM di_dim_domain d
       WHERE d.di_domain_id = p_di_domain_id
       
      -- Update the existing mapping if required
      ON DUPLICATE KEY UPDATE
        merchant_hierarchy_l4_id  = VALUES(merchant_hierarchy_l4_id)
       ,di_last_updated_date      = NOW();
      
    -- -------------------------------------------------------------------------
    -- QIPU.DE
    -- -------------------------------------------------------------------------
    WHEN 'qipu.de' THEN
    
      -- Check Merchant Id validity
      SELECT CASE
               WHEN COUNT(*) > 0 THEN 0
               ELSE 1
             END
        INTO v_is_new_merchant
        FROM di_dim_merchant
       WHERE di_domain_id = p_di_domain_id
         AND domain_merchant_id = p_domain_merchant_id;
    
      -- Insert new mapping
      INSERT INTO di_cfg_merchant_hierarchy_mapping (di_domain_id
                                                    ,domain_merchant_id
                                                    ,merchant_hierarchy_l4_id
                                                    ,is_new_merchant
                                                    ,di_created_date
                                                    ,di_last_updated_date)
      SELECT d.di_domain_id
            ,p_domain_merchant_id
            ,p_merchant_hierarchy_l4_id
            ,v_is_new_merchant
            ,NOW()
            ,NOW()
        FROM di_dim_domain d
       WHERE d.di_domain_id = p_di_domain_id
       
      -- Update the existing mapping if required
      ON DUPLICATE KEY UPDATE
        merchant_hierarchy_l4_id  = VALUES(merchant_hierarchy_l4_id)
       ,is_new_merchant           = v_is_new_merchant
       ,di_last_updated_date      = NOW();
                  
    -- -------------------------------------------------------------------------
    -- The domain parameter is referring to an invalid domain or the
    -- registration source rules are not implemented for that domain.
    -- -------------------------------------------------------------------------
    ELSE
      SIGNAL exc_not_implemented;
    
  END CASE;

END;
//
DELIMITER ;
