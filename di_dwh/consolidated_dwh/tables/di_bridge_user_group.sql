DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_bridge_user_group;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_bridge_user_group (
  di_domain_id                              INT              NOT NULL DEFAULT -1              COMMENT 'Reference to DI_DIM_DOMAIN tabel (FK)'
 ,di_user_id                                INT              NOT NULL                         COMMENT 'Reference to DI_DIM_USER table (FK)'
 ,domain_user_id                            INT     UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,group_id                                  INT              NOT NULL                         COMMENT 'Reference to DI_DIM_USER_GROUP table (FK)'
 ,start_date                                DATETIME                                          COMMENT 'Start date of the user is in the group'
 ,end_date                                  DATETIME                                          COMMENT 'End date of the user is in the group'
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,UNIQUE KEY nk_di_bridge_user_group (di_domain_id,domain_user_id,group_id,start_date,end_date)
 ,KEY idx_domain_user_id (domain_user_id)
 ,KEY idx_group_id (group_id)
 ,KEY idx_end_date (end_date)
) COMMENT = 'DI Consolidated User Groups Bridge Table'
 ENGINE=InnoDB
 CHARACTER SET utf8 COLLATE utf8_general_ci
 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8
;
