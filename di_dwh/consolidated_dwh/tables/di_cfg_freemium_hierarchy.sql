DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_freemium_hierarchy;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_freemium_hierarchy (
  freemium_hierarchy_id       INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,freemium_type_l1_id         INT           NOT NULL                         COMMENT 'Freemium Type Level 1 ID'
 ,freemium_type_l1_name       VARCHAR(64)   NOT NULL                         COMMENT 'Freemium Type Level 1 Name'
 ,freemium_type_l2_id         INT           NOT NULL                         COMMENT 'Freemium Type Level 2 ID'
 ,freemium_type_l2_name       VARCHAR(64)   NOT NULL                         COMMENT 'Freemium Type Level 2 Name'
 ,freemium_type_l3_id         INT           NOT NULL                         COMMENT 'Freemium Type Level 3 ID'
 ,freemium_type_l3_name       VARCHAR(64)   NOT NULL                         COMMENT 'Freemium Type Level 3 Name'
 ,user_type                   INT           NOT NULL                         COMMENT 'User type mapping'
 ,PRIMARY KEY (`freemium_hierarchy_id`)
) COMMENT =  'ETL mapping rules for the Freemium type hierarhcy'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
