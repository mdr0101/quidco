DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_mapping_rules;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_mapping_rules (
  rule_id                     INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,table_name                  VARCHAR(64)   NOT NULL                         COMMENT 'Target table name of mapping/transformation'
 ,column_name                 VARCHAR(64)   NOT NULL                         COMMENT 'Target column name of mapping/transformation'
 ,mapping_rule                VARCHAR(50)   NOT NULL                         COMMENT 'Mapping rule name'
 ,value_int                   INT                                            COMMENT 'Numeric value'
 ,value_string                TEXT                                           COMMENT 'Character value'
 ,value_datetime              DATETIME                                       COMMENT 'Datetime value'
 ,description                 VARCHAR(2000) NOT NULL                         COMMENT 'Mapping description'
 ,created_at                  TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (`rule_id`)
 ,UNIQUE INDEX nk_di_cfg_mapping_rules (di_domain_id, table_name, column_name, mapping_rule)
) COMMENT =  'Cross-domain configuration table for every ETL mapping and transformation. (quidco.com, qipu.de, etc.)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;
