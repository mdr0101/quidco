DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_mapping_rules_spend_imputed;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_mapping_rules_spend_imputed (
  di_cfg_spend_imputed_rule_id        INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                        INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,rule_category                       VARCHAR(100)  NOT NULL                         COMMENT 'Spend imputed category name'
 ,lookup_id                           INT           NOT NULL                         COMMENT 'Category lookup key (merchant_id, merchant_hierarchy_l4_id, etc.)'
 ,month                               INT                                            COMMENT 'Month specific rules'
 ,spend_imputed                       DECIMAL(10,2)                                  COMMENT 'Spend imputed value to apply'
 ,spend_imputed_correction            DECIMAL(10,2)                                  COMMENT 'Spend imputed correction to apply (eg. VAT percentage)'
 ,di_created_date                     TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (di_cfg_spend_imputed_rule_id)
 ,UNIQUE KEY (di_domain_id, rule_category, lookup_id, month, spend_imputed, spend_imputed_correction)
) COMMENT =  'Cross-domain spend imputed business rules'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;