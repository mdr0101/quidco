DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_merchant_hierarchy;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_merchant_hierarchy (
  di_cfg_merchant_hierarchy_id       INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,merchant_hierarchy_l1_id           INT           NOT NULL                         COMMENT 'Merchant Hierarchy Level 1 ID'
 ,merchant_hierarchy_l1_name         VARCHAR(100)  NOT NULL                         COMMENT 'Merchant Hierarchy Level 1 Name'
 ,merchant_hierarchy_l2_id           INT           NOT NULL                         COMMENT 'Merchant Hierarchy Level 2 ID'
 ,merchant_hierarchy_l2_name         VARCHAR(100)  NOT NULL                         COMMENT 'Merchant Hierarchy Level 2 Name'
 ,merchant_hierarchy_l3_id           INT           NOT NULL                         COMMENT 'Merchant Hierarchy Level 3 ID'
 ,merchant_hierarchy_l3_name         VARCHAR(100)  NOT NULL                         COMMENT 'Merchant Hierarchy Level 3 Name'
 ,merchant_hierarchy_l4_id           INT           NOT NULL                         COMMENT 'Merchant Hierarchy Level 4 ID'
 ,merchant_hierarchy_l4_name         VARCHAR(100)  NOT NULL                         COMMENT 'Merchant Hierarchy Level 4 Name'
 ,di_created_date                    TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (di_cfg_merchant_hierarchy_id)
 ,KEY (merchant_hierarchy_l4_id)
) COMMENT =  'Cross-domain merchant hierarchy'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;