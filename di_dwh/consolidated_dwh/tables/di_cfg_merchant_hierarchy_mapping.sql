DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_merchant_hierarchy_mapping;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_merchant_hierarchy_mapping (
  di_cfg_merchant_hierarchy_mapping_id  INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                          INT               NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_merchant_id                    INT(10) UNSIGNED  NOT NULL                         COMMENT 'Merchant ID in the source system'
 ,merchant_hierarchy_l4_id              INT               NOT NULL                         COMMENT 'Link to Merhant Hierarchy Level 4 Id'
 ,is_new_merchant                       TINYINT           NOT NULL                         COMMENT 'Merchant is loaded into DI_DIM_MERCHANT table flag'
 ,di_created_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                  TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,PRIMARY KEY (di_cfg_merchant_hierarchy_mapping_id)
 ,UNIQUE KEY (di_domain_id, domain_merchant_id)
-- ,CONSTRAINT fk_merchant_hierarchy_l4_id FOREIGN KEY (merchant_hierarchy_l4_id) REFERENCES di_cfg_merchant_hierarchy (merchant_hierarchy_l4_id) ON DELETE NO ACTION ON UPDATE NO ACTION
) COMMENT =  'Cross-domain merchant hierarchy mappings'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
