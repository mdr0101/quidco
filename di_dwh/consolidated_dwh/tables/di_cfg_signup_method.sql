DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_signup_method;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_signup_method (
  di_cfg_signup_method_id        INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,signup_method_id               INT           NOT NULL                         COMMENT 'Signup method Id (-1: Unknown, 1: Quidco, 2: Facebook, 3: PayPal)'
 ,signup_method_name             VARCHAR(100)  NOT NULL                         COMMENT 'Signup method name (Unknown, Quidco, Facebook, PayPal)'
 ,di_created_date                TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (di_cfg_signup_method_id)
) COMMENT =  'Cross-domain signup methods'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;