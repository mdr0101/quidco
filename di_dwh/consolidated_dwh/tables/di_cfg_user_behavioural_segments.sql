DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_user_behavioural_segments;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_cfg_user_behavioural_segments (
  di_cfg_user_behaviour_segment_id    INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                        INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_user_id                      INT  UNSIGNED NOT NULL                         COMMENT 'Domain specific user ID'
 ,segment_id                          INT           NOT NULL                         COMMENT 'Behavioural Segment ID'
 ,segment_name                        VARCHAR(100)  NOT NULL                         COMMENT 'Behavioural Segment Name'
 ,probability                         DOUBLE        NOT NULL                         COMMENT 'Probability'
 ,di_created_date                     TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (di_cfg_user_behaviour_segment_id)
 ,UNIQUE KEY nk_di_cfg_merchant_cluster (di_domain_id, domain_user_id)
) COMMENT =  'Cross-domain static user behavioural segments'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;