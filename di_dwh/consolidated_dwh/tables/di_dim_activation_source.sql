/* New Dimension for Activation Source: di_dim_activation_source */
DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_activation_source;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_activation_source (
  di_activation_source_id   		INT               	NOT NULL AUTO_INCREMENT          	COMMENT 'Primary Key/Surrogate Key'
 ,activation_source_id 				INT(11) 			NOT NULL 							COMMENT 'Activation Source ID: 1, 0'
 ,activation_source_name 			VARCHAR(20) 		NOT NULL 							COMMENT 'Activation Source Name: mobile, online'
 ,PRIMARY KEY (di_activation_source_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='DI Consolidated Domain Dimension Table (SCD Type 1)';
;
