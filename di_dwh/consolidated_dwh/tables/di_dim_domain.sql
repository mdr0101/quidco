DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_domain;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_domain (
  di_domain_id                INT          NOT NULL          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_name              VARCHAR(20)  NOT NULL          COMMENT 'Name of Domain (1: quidco.com, 2: qipu.de, etc.)'
 ,domain_go_live_date_id      INT          NOT NULL          COMMENT 'Date when the domain initialised'
 ,domain_description          VARCHAR(100) NOT NULL          COMMENT 'Domain description '
 ,currency_code               CHAR(3)      NOT NULL          COMMENT 'ISO-4217 Currency Codes'
 ,currency_name               VARCHAR(30)  NOT NULL          COMMENT 'Country Currency Name'
 ,PRIMARY KEY (di_domain_id)
) COMMENT = 'DI Consolidated Domain Dimension Table (SCD Type 1)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
