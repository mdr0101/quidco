DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_duration;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_duration (
  duration_id                     		INT             NOT NULL          COMMENT 'Primary Key - Day in integer'
 ,days_duration_id                     	INT             NOT NULL          COMMENT 'Primary Key - Day in integer'
 ,days_duration_name                  	VARCHAR(25)     NOT NULL          COMMENT 'Day Name'
 ,week_duration_id              		INT             NOT NULL          COMMENT 'Week ID'
 ,week_duration_name            		VARCHAR(25)     NOT NULL          COMMENT 'Week name'
 ,month_duration_id						INT             NOT NULL          COMMENT 'Four week period ID'
 ,month_duration_name					VARCHAR(25)     NOT NULL          COMMENT 'Four week period name'
 ,year_duration_id						INT             NOT NULL          COMMENT 'Year ID'
 ,year_duration_name					VARCHAR(25)     NOT NULL          COMMENT 'Year name'
 ,PRIMARY KEY (duration_id)
) COMMENT = 'DI Consolidated Days Duration Dimension'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
;