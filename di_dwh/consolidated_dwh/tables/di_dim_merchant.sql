DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_merchant;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_merchant (
  di_merchant_id                            INT              NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_merchant_name                          VARCHAR(200)     NOT NULL                         COMMENT 'DI Specific merchant full name (Concatenated with original merchant ID and domain name)'
 ,di_domain_id                              INT              NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_merchant_id                        INT(10) UNSIGNED NOT NULL                         COMMENT 'Merchant ID in the source system'
 ,domain_merchant_name                      VARCHAR(100)     NOT NULL                         COMMENT 'Merchant Name in the source system'
 ,merchant_cluster_id                       INT              NOT NULL                         COMMENT 'Merchant cluster id'
 ,merchant_cluster_name                     VARCHAR(100)     NOT NULL                         COMMENT 'Merchant cluster name'
 ,merchant_hierarchy_l1_id                  INT              NOT NULL                         COMMENT 'Merchant Hierarchy level 1 ID'
 ,merchant_hierarchy_l1_name                VARCHAR(100)     NOT NULL                         COMMENT 'Name of Merchant Hierarchy level 1'
 ,merchant_hierarchy_l2_id                  INT              NOT NULL                         COMMENT 'Merchant Hierarchy level 2 ID'
 ,merchant_hierarchy_l2_name                VARCHAR(100)     NOT NULL                         COMMENT 'Name of Merchant Hierarchy level 2'
 ,merchant_hierarchy_l3_id                  INT              NOT NULL                         COMMENT 'Merchant Hierarchy level 3 ID'
 ,merchant_hierarchy_l3_name                VARCHAR(100)     NOT NULL                         COMMENT 'Name of Merchant Hierarchy level 3'
 ,merchant_hierarchy_l4_id                  INT              NOT NULL                         COMMENT 'Merchant Hierarchy level 4 ID'
 ,merchant_hierarchy_l4_name                VARCHAR(100)     NOT NULL                         COMMENT 'Name of Merchant Hierarchy level 4'
 ,is_instore                                TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Is In-Store merchant flag (0: no, 1: yes)'
 ,instore_type_id                           TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'In-Store Merhant type (0: Not In-Store, 1: In-Store, 2: Receipt Upload)'
 ,instore_type_name                         VARCHAR(100)     NOT NULL                         COMMENT 'Name of In-Store Merchant type'
 ,has_instore_control_group                 TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Has instore control group flag (0: no, 1: yes'
 ,is_fast_payment                           TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Fast Payment Merchant Flag (0/1)'
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_merchant_id)
 ,UNIQUE INDEX nk_di_dim_merchant (di_domain_id, domain_merchant_id)
 ,INDEX idx_domain_merchant_id (domain_merchant_id)
 ,INDEX idx_merchant_hierarchy_l1_id (merchant_hierarchy_l1_id)
 ,INDEX idx_is_instore (is_instore)
 ,INDEX idx_job_run_id_di_dim_merchant(job_run_id)
) COMMENT = 'DI Consolidated Merchant Dimension Table (SCD Type1)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
