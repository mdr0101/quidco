DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_merchant_hist;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_merchant_hist (
   di_merchant_hist_id                      INT(11) 			NOT NULL 					AUTO_INCREMENT 						COMMENT 'Primary Key/Surrogate Key'
  ,di_merchant_name 						VARCHAR(200) 		NOT NULL 														COMMENT 'DI Specific merchant full name (Concatenated with original merchant ID and domain name)'
  ,di_domain_id 							INT(11) 			NOT NULL 														COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
  ,domain_merchant_id 						INT(10) unsigned 	NOT NULL 														COMMENT 'Merchant ID in the source system'
  ,domain_merchant_name 					VARCHAR(100) 		NOT NULL 														COMMENT 'Merchant Name in the source system'
  ,merchant_cluster_id 						INT(11) 			NOT NULL 														COMMENT 'Merchant cluster id'
  ,merchant_cluster_name 					VARCHAR(100) 		NOT NULL 														COMMENT 'Merchant cluster name'
  ,merchant_hierarchy_l1_id 				INT(11) 			NOT NULL 														COMMENT 'Merchant Hierarchy level 1 ID'
  ,merchant_hierarchy_l1_name 				VARCHAR(100) 		NOT NULL 														COMMENT 'Name of Merchant Hierarchy level 1'
  ,merchant_hierarchy_l2_id 				INT(11) 			NOT NULL 														COMMENT 'Merchant Hierarchy level 2 ID'
  ,merchant_hierarchy_l2_name 				VARCHAR(100) 		NOT NULL 														COMMENT 'Name of Merchant Hierarchy level 2'
  ,merchant_hierarchy_l3_id 				INT(11) 			NOT NULL 														COMMENT 'Merchant Hierarchy level 3 ID'
  ,merchant_hierarchy_l3_name 				VARCHAR(100) 		NOT NULL 														COMMENT 'Name of Merchant Hierarchy level 3'
  ,merchant_hierarchy_l4_id 				INT(11) 			NOT NULL 														COMMENT 'Merchant Hierarchy level 4 ID'
  ,merchant_hierarchy_l4_name 				VARCHAR(100) 		NOT NULL 														COMMENT 'Name of Merchant Hierarchy level 4'
  ,is_instore 								TINYINT(4) 			NOT NULL 		DEFAULT '0' 									COMMENT 'Is In-Store merchant flag (0: no, 1: yes)'
  ,instore_type_id 							TINYINT(4) 			NOT NULL 		DEFAULT '0' 									COMMENT 'In-Store Merhant type (0: Not In-Store, 1: In-Store, 2: Receipt Upload)'
  ,instore_type_name 						VARCHAR(100) 		NOT NULL 														COMMENT 'Name of In-Store Merchant type'
  ,has_instore_control_group 				TINYINT(4) 			NOT NULL 		DEFAULT '0' 									COMMENT 'Has instore control group flag (0: no, 1: yes)'
  ,is_fast_payment 							TINYINT(4) 			NOT NULL 		DEFAULT '0' 									COMMENT 'Fast Payment Merchant Flag (0/1)'
  ,valid_from								DATETIME			NOT NULL														COMMENT 'SCD Type 2: Valid from DATETIME'
  ,valid_to									DATETIME							DEFAULT NULL									COMMENT 'SCD Type2: Valid to DATETIME'
  ,is_latest								TINYINT(4)			NOT NULL														COMMENT 'SCD Type2: Is Latest Flag'		
  ,di_created_date 							TIMESTAMP 			NOT NULL 		DEFAULT 	CURRENT_TIMESTAMP 					COMMENT 'Row creation date'
  ,di_last_updated_date 					TIMESTAMP 			NOT NULL 		DEFAULT 	CURRENT_TIMESTAMP 					COMMENT 'Row last updated date'
  ,job_run_id 								INT(11) 			NOT NULL 														COMMENT 'Modified to reference di_etl_job_log'
  ,PRIMARY KEY (di_merchant_hist_id)
  ,UNIQUE KEY nk_di_dim_merchant_hist (di_domain_id, domain_merchant_id, valid_to)
  ,KEY idx_domain_merchant_id (domain_merchant_id)
  ,KEY idx_merchant_hierarchy_l1_id (merchant_hierarchy_l1_id)
  ,KEY idx_is_instore (is_instore)
  ,KEY idx_valid_from (valid_from)
  ,KEY idx_valid_to (valid_to)
  ,KEY idx_job_run_id_di_dim_merchant_hist (job_run_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='DI Consolidated Merchant History Dimension Table (SCD Type 2)'
;
