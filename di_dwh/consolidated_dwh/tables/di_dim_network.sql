DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_network;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_network (
  di_network_id                             INT              NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_network_name                           VARCHAR(200)     NOT NULL                         COMMENT 'DI Specific network full name (Concatenated with original domain name)'
 ,di_domain_id                              INT              NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_network                            VARCHAR(10)      NOT NULL                         COMMENT 'Domain specific Network ID in the source transactional database'
 ,network_group_id                          INT              NOT NULL DEFAULT -1              COMMENT 'Network Group Id (1: Online, 2: HighStreet, 3: Opinions, 4: Compare, 5: Invalid)'
 ,network_group_name                        VARCHAR(100)     NOT NULL DEFAULT 'Unknown'       COMMENT 'Network Group Name (1: Online, 2: HighStreet, 3: Opinions, 4: Compare, 5: Invalid)'
 ,contact                                   VARCHAR(100)     NOT NULL                         COMMENT 'Network contact person'
 ,email                                     VARCHAR(100)     NOT NULL                         COMMENT 'Network contact email'
 ,currency                                  VARCHAR(10)      NOT NULL                         COMMENT 'Network currency'
 ,is_card_linked_network                    TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Is card linked network flag (0: no, 1: yes)'
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_network_id)
 ,UNIQUE INDEX nk_di_dim_network (di_domain_id, domain_network)
 ,INDEX idx_domain_network (domain_network)
 ,INDEX idx_is_instore (is_card_linked_network)
 ,INDEX idx_job_run_id_di_dim_network(job_run_id)
) COMMENT = 'DI Consolidated Network Dimension Table (SCD Type1)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
