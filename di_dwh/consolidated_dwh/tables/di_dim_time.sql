DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_time;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_time (
  time_id                     INT             NOT NULL          COMMENT 'Primary Key - Hour in integer'
 ,start_time                  TIME            NOT NULL          COMMENT 'Start time of period'
 ,end_time                    TIME            NOT NULL          COMMENT 'End time of period'
 ,part_of_day_id              INT             NOT NULL          COMMENT 'Part of the day ID'
 ,part_of_day_name            VARCHAR(40)     NOT NULL          COMMENT 'Part of the day name'
 ,PRIMARY KEY (`time_id`)
) COMMENT = 'DI Consolidated Time Dimension Table (Role playing dimension)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
