DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_transaction_status;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_transaction_status (
   di_transaction_status_id      	INT           NOT NULL AUTO_INCREMENT     	COMMENT 'Primary Key/Surrogate Key'
  ,di_domain_id               		INT           NOT NULL                    	COMMENT 'Domain ID (1: quidco.com, 2: qipu.de, etc.)'
  ,transaction_status_id 	      	INT           NOT NULL 	                	COMMENT 'Primary Key/Surrogate Key'
  ,transaction_status_name       	VARCHAR(30)   NOT NULL                    	COMMENT 'Purchase Status (1: Pending, 2: Added, etc.)'
  ,transaction_status_group_id   	INT           NOT NULL                    	COMMENT 'Primary Key/Surrogate Key'
  ,transaction_status_group_name 	VARCHAR(30)   NOT NULL                    	COMMENT 'Purchase Status (1: Pending, 2: Added, etc.)'
  ,PRIMARY KEY (di_transaction_status_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='DI Transaction Status'
;