DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_dim_user_group;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_dim_user_group (
  group_id                                  INT              NOT NULL                         COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                              INT              NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,group_name                                VARCHAR(250)     NOT NULL                         COMMENT 'Group name'
 ,description                               VARCHAR(500)     NOT NULL                         COMMENT 'Group description'
 ,is_active                                 TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Active flag (0:no, 1: yes)'
 ,end_date                                  DATETIME                                          COMMENT 'End date of the group'
 ,current_users                             INT              NOT NULL                         COMMENT 'Number of current users in the group'
 ,is_adhoc_run                              TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Adhoc run flag (0: no, 1: yes)'
 ,is_control_group                          TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT 'Control group flag (0: no, 1: yes)' 
 ,parent_id                                 INT              NOT NULL                         COMMENT 'Set if this is a control group and will be the related group'
 ,type                                      VARCHAR(100)                                      COMMENT '(new-users or targetted-marketing) New-users is set automatically if there is a new_user_actio.'
 ,group_campaign_id                         INT              NOT NULL                         COMMENT 'Group Campaign Id'
 ,group_campaign_name                       VARCHAR(250)     NOT NULL                         COMMENT 'Group Campaign Name'
 ,group_campaign_description                VARCHAR(500)     NOT NULL                         COMMENT 'Group Campaign Description'
 ,group_campaign_last_modified_by           VARCHAR(100)     NOT NULL                         COMMENT 'Group Campaign Last modified name'
 ,group_programme_id                        INT              NOT NULL                         COMMENT 'Group Programme Id'
 ,group_programme_name                      VARCHAR(250)     NOT NULL                         COMMENT 'Group Programme Name'
 ,group_programme_description               VARCHAR(500)     NOT NULL                         COMMENT 'Group Programme Description'
 ,group_programme_last_modified_by          VARCHAR(100)     NOT NULL                         COMMENT 'Group Programme Last modified name'
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (group_id, di_domain_id)
 ,UNIQUE INDEX nk_di_dim_user (di_domain_id, group_id)
 ,INDEX idx_group_id (group_id)
 ,INDEX idx_group_campaign_id (group_campaign_id)
 ,INDEX idx_group_programme_id (group_programme_id)
 ,INDEX idx_is_adhoc_run(is_adhoc_run)
 ,INDEX idx_is_control_group(is_control_group)
 ,INDEX idx_type(type)
 ,INDEX idx_job_run_id_di_dim_user_group(job_run_id)
) COMMENT = 'DI Consolidated User Groups Dimension Table (SCD Type1)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
  PARTITION BY LIST (di_domain_id) (
    PARTITION p_unknown VALUES IN (-1),
    PARTITION p_quidco_com VALUES IN (1),
    PARTITION p_qipu_de VALUES IN (2)
);
