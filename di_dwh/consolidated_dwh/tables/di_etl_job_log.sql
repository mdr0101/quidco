DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_etl_job_log;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_etl_job_log (
  job_run_id                  INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,job_id                      INT           NOT NULL                         COMMENT 'Job ID Reference to di_etl_jobs.job_id'
 ,job_name                    VARCHAR(100)  NOT NULL                         COMMENT 'ETL job name'
 ,start_datetime              DATETIME      NOT NULL                         COMMENT 'Start datetime'
 ,end_datetime                DATETIME                                       COMMENT 'End datetime'
 ,job_status                  VARCHAR(20)   NOT NULL                         COMMENT 'ETL job status'
 ,error_message               TEXT                                           COMMENT 'ETL error message'
 ,PRIMARY KEY (`job_run_id`)
) COMMENT =  'ETL job log table'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
 ;