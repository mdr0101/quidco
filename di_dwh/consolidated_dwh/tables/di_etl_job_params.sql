DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_etl_job_params;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_etl_job_params (
  job_param_id                INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,job_name                    VARCHAR(100)  NOT NULL                         COMMENT 'ETL job name'
 ,job_param_name              VARCHAR(100)  NOT NULL                         COMMENT 'ETL job parameter name'
 ,value_int                   INT                                            COMMENT 'Numeric value'
 ,value_string                TEXT                                           COMMENT 'Character value'
 ,value_datetime              DATETIME                                       COMMENT 'Datetime value'
 ,description                 VARCHAR(2000) NOT NULL                         COMMENT 'Mapping description'
 ,di_created_date             TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date        TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,PRIMARY KEY (`job_param_id`)
 ,UNIQUE INDEX nk_di_etl_job_parms (job_name, job_param_name)
) COMMENT =  'ETL job parameters table'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;
