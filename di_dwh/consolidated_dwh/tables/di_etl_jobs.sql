DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_etl_jobs;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_etl_jobs (
  job_id                      INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,job_group                   VARCHAR(100)  NOT NULL                         COMMENT 'ETL job group'
 ,job_type                    VARCHAR(100)  NOT NULL                         COMMENT 'ETL job type (JOB or JOB_GROUP)'
 ,job_name                    VARCHAR(100)  NOT NULL                         COMMENT 'ETL job name'
 ,run_priority                INT           NOT NULL                         COMMENT 'Run priority of the job'
 ,is_active                   TINYINT       NOT NULL DEFAULT '1'             COMMENT 'Is active flag. (0: no, 1: yes)'
 ,is_next_run_active 		  TINYINT(4) 	NOT NULL DEFAULT '1' 			 COMMENT 'Is run active flag. (0: no, 1: yes)'
 ,PRIMARY KEY (`job_id`)
) COMMENT =  'ETL jobs table'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
 ;
