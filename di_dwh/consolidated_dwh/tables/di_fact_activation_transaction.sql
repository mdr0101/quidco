 /* New Fact Table for activation transactions: di_fact_activation_transaction */ 
DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_fact_activation_transaction;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_fact_activation_transaction (
  di_activation_transaction_id              INT            NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                              INT            NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_activation_id                      INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific Activation ID in the source transactional database'
 ,di_transaction_id                         INT   UNSIGNED                                  COMMENT 'Reference to DI_WRK_TRANSACTION'
 ,di_purchase_id                            VARCHAR(100)                                    COMMENT 'Reference to DI_FACT_PURCHASE'
 ,di_user_id                                INT            NOT NULL                         COMMENT 'Referecen to DI_DIM_USER'
 ,di_merchant_id                            INT            NOT NULL                         COMMENT 'Reference to DI_DIM_MERCHANT'
 ,domain_transaction_id                     INT   UNSIGNED                                  COMMENT 'Domain specific Transaction ID in the source transactional database'
 ,domain_user_id                            INT   UNSIGNED 			                        COMMENT 'Domain specific User ID in the source transactional database'
 ,domain_merchant_id                        INT   UNSIGNED 			                        COMMENT 'Merchant ID in the source system'
 ,activation_date_id                        INT            NOT NULL                         COMMENT 'Reference to DI_DIM_DATE - Activation Date'
 ,purchase_date_id                          INT            NOT NULL                         COMMENT 'Reference to DI_FACT_PURCHASE - Purchase Date Id'
 ,is_mobile_activation                      TINYINT(4)                                      COMMENT 'Activation source (0: Online, 1: mobile)'
 ,latitude                                  DECIMAL(10, 6)                                  COMMENT 'Geographical Latitude Coordinate of Activation'
 ,longitude                                 DECIMAL(10, 6)                                  COMMENT 'Geographical Longitude Coordinate of Activation'
 ,di_created_date                           TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT            NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_activation_transaction_id)
 ,UNIQUE idx_activation_transaction (di_domain_id, domain_activation_id, domain_transaction_id)
 ,INDEX idx_domain (di_domain_id)
 ,INDEX idx_job_run_id_di_fact_click(job_run_id)
) COMMENT = 'DI Consolidated Instore Activation Transaction Table.'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED
;