DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_fact_click;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_fact_click (
  di_click_id                               INT            NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                              INT            NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_click_id                           INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific Click ID in the source transactional database'
 ,di_user_id                                INT            NOT NULL                         COMMENT 'Reference to DI_DIM_USER table (FK)'
 ,di_user_hist_id                           INT            NOT NULL                         COMMENT 'Reference to DI_DIM_USER_HIST table (FK)'
 ,di_merchant_id                            INT            NOT NULL                         COMMENT 'Reference to DI_DIM_MERCHANT table (FK)'
 ,di_merchant_hist_id                       INT            NOT NULL                         COMMENT 'Reference to DI_DIM_MERCHANT_HIST table (FK)'
 ,di_network_id                             INT            NOT NULL                         COMMENT 'Reference to DI_DIM_NETWORK table (FK)'
 ,domain_user_id                            INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,domain_merchant_id                        INT   UNSIGNED NOT NULL                         COMMENT 'Merchant ID in the source system'
 ,domain_network                            VARCHAR(10)    NOT NULL                         COMMENT 'Domain specific Network ID in the source transactional database'
 ,ip                                        VARCHAR(32)                                     COMMENT 'Click IP address'
 ,click_date_id                             INT            NOT NULL                         COMMENT 'Click Date. Reference to DI_DIM_DATE table (FK)'
 ,click_time_id                             INT            NOT NULL DEFAULT '-1'            COMMENT '(Reg/Auth) - Time of click. Reference to DI_DIM_DATE table (FK)'
 ,click_datetime                            DATETIME       NOT NULL                         COMMENT 'Click DATETIME'
 ,is_valid                                  TINYINT(4)     NOT NULL DEFAULT '1'             COMMENT 'Is valid flag. (0: no, 1: yes) - Rules are defined in DI_CFG_MAPPING_RULES'
 ,di_created_date                           TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT            NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_click_id, di_domain_id, click_date_id)
 ,INDEX idx_domain (di_domain_id)
 ,INDEX idx_user_id (di_user_id)
 ,INDEX idx_user_hist_id (di_user_hist_id)
 ,INDEX idx_merchant_id (di_merchant_id)
 ,INDEX idx_merchant_hist_id (di_merchant_hist_id)
 ,INDEX idx_network_id (di_network_id)
 ,INDEX idx_click_date_id (click_date_id)
 ,INDEX idx_click_datatime (click_datetime)
 ,INDEX idx_job_run_id_di_fact_click(job_run_id)
) COMMENT = 'DI Consolidated Click Fact Table. (Partitioned by domains - quidco.com, qipu.de, etc.)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED
 PARTITION BY RANGE (click_date_id)
 SUBPARTITION BY HASH (di_domain_id)
 SUBPARTITIONS 2 (
    PARTITION p_2006   VALUES LESS THAN (20070101),
    
    PARTITION p_2007_1 VALUES LESS THAN (20070701),
    PARTITION p_2007_2 VALUES LESS THAN (20080101),
    
    PARTITION p_2008_1 VALUES LESS THAN (20080701),
    PARTITION p_2008_2 VALUES LESS THAN (20090101),
    
    PARTITION p_2009_1 VALUES LESS THAN (20090701),
    PARTITION p_2009_2 VALUES LESS THAN (20100101),
    
    PARTITION p_2010_1 VALUES LESS THAN (20100701),
    PARTITION p_2010_2 VALUES LESS THAN (20110101),
    
    PARTITION p_2011_1 VALUES LESS THAN (20110701),
    PARTITION p_2011_2 VALUES LESS THAN (20120101),
    
    PARTITION p_2012_1 VALUES LESS THAN (20120701),
    PARTITION p_2012_2 VALUES LESS THAN (20130101),
    
    PARTITION p_2013_3 VALUES LESS THAN (20130701),
    PARTITION p_2013_4 VALUES LESS THAN (20140101),

    PARTITION p_2014_1 VALUES LESS THAN (20140401),
    PARTITION p_2014_2 VALUES LESS THAN (20140701),
    PARTITION p_2014_3 VALUES LESS THAN (20141001),
    PARTITION p_2014_4 VALUES LESS THAN (20150101),

    PARTITION p_2015_1 VALUES LESS THAN (20150401),
    PARTITION p_2015_2 VALUES LESS THAN (20150701),
    PARTITION p_2015_3 VALUES LESS THAN (20151001),
    PARTITION p_2015_4 VALUES LESS THAN (20160101),

    PARTITION p_2016_1 VALUES LESS THAN (20160401),
    PARTITION p_2016_2 VALUES LESS THAN (20160701),
    PARTITION p_2016_3 VALUES LESS THAN (20161001),
    PARTITION p_2016_4 VALUES LESS THAN (20170101),

    PARTITION p_2017_1 VALUES LESS THAN (20170401),
    PARTITION p_2017_2 VALUES LESS THAN (20170701),
    PARTITION p_2017_3 VALUES LESS THAN (20171001),
    PARTITION p_2017_4 VALUES LESS THAN (20180101),
       
    PARTITION p_2018_1 VALUES LESS THAN (20180401),
    PARTITION p_2018_2 VALUES LESS THAN (20180701),
    PARTITION p_2018_3 VALUES LESS THAN (20181001),
    PARTITION p_2018_4 VALUES LESS THAN (20190101),
    PARTITION p_2019_1 VALUES LESS THAN (MAXVALUE)
);
