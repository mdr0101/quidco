DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_fact_transaction;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_fact_transaction (
  di_transaction_id					        VARCHAR(100)   					NOT NULL DEFAULT -1			     COMMENT 'Reference to DI_WRK_TRANSACTION table' 
 ,di_purchase_id					        VARCHAR(100)   					NOT NULL DEFAULT 'NA'            COMMENT 'Reference to DI_WRK_TRANSACTION table'
 ,di_domain_id                              INT            					NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,di_user_id                                INT            					NOT NULL                         COMMENT 'Reference to DI_DIM_USER table (FK)'
 ,di_user_hist_id                           INT            					NOT NULL                         COMMENT 'Reference to DI_DIM_USER_HIST table (FK)'
 ,di_merchant_id                            INT            					NOT NULL                         COMMENT 'Reference to DI_DIM_MERCHANT table (FK)'
 ,di_merchant_hist_id                       INT            					NOT NULL                         COMMENT 'Reference to DI_DIM_MERCHANT_HIST table (FK)'
 ,di_network_id                             INT            					NOT NULL                         COMMENT 'Reference to DI_DIM_NETWORK table (FK)'
 ,domain_user_id                            INT     		UNSIGNED 		NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,domain_merchant_id                        INT     		UNSIGNED 		NOT NULL                         COMMENT 'Merchant ID in the source system'
 ,domain_network                            VARCHAR(10)    					NOT NULL                         COMMENT 'Domain specific Network ID in the source transactional database'
 ,domain_click_id                           INT     		UNSIGNED 		NOT NULL                         COMMENT 'Reference to DI_FACT_CLICK table (FK)'
 ,domain_transaction_id						INT(10) 		UNSIGNED 		NOT NULL 						 COMMENT 'Domain specific Transaction ID in the source transactional database'
 ,transaction_datetime                      DATETIME       					NOT NULL                         COMMENT 'Transaction DATETIME'
 ,transaction_date_id                       INT            					NOT NULL                         COMMENT 'Transaction Date. Reference to DI_DIM_DATE table (FK)'
 ,transaction_time_id                       INT            					NOT NULL DEFAULT '-1'            COMMENT 'Transaction Time bucket. Reference to DI_DIM_TIME table (FK)'
 ,first_tracked_date_id                     INT            					NOT NULL                         COMMENT 'Date when the purchase has been tracked'
 ,first_confirmed_date_id                   INT            					NOT NULL                         COMMENT 'Date when the purchase has been confirmed'
 ,first_imported_date_id                    INT            					NOT NULL                         COMMENT 'Purchase First imported Date.'
 ,first_validated_date_id                   INT            					NOT NULL                         COMMENT 'Date when the purchase has been validated'
 ,first_declined_date_id                    INT            					NOT NULL                         COMMENT 'Date when the purchase has been declined'
 ,first_not_pending_or_denied_date_id       INT            					NOT NULL                         COMMENT 'Purchase First not pending or denied Date.'
 ,spend                                     DECIMAL(11, 2) 					NOT NULL                         COMMENT 'Spend value'
 ,spend_imputed                             DECIMAL(11, 2) 					NOT NULL                         COMMENT 'Spend imputed value'
 ,commission                                DECIMAL(11, 2) 					NOT NULL                         COMMENT 'Commission value'
 ,user_commission                           DECIMAL(11, 2) 					NOT NULL                         COMMENT 'User Commission value'
 ,di_transaction_status_id                  INT		       					NOT NULL DEFAULT -1              COMMENT 'Transaction status'	-- create new table
 ,di_payments_out_status_id                 INT		       					NOT NULL DEFAULT -1              COMMENT 'Transaction payments out status'
 ,is_paypal_paid                            TINYINT(4)     					NOT NULL DEFAULT '0'             COMMENT 'Is Paid by PayPal flag (0: no, 1: yes)'
 ,is_valid                                  TINYINT(4)     					NOT NULL DEFAULT '1'             COMMENT 'Is valid flag. (0: no, 1: yes) - Rules are defined in DI_CFG_MAPPING_RULES'
 ,is_gross 									TINYINT(4)     					NOT NULL DEFAULT '1'			 COMMENT 'Gross Flag. (0:no, 1:yes)' -- new column
 ,is_outlier								TINYINT(4)     					NOT NULL DEFAULT '1'             COMMENT 'Is outlier flag. (0: no, 1: yes) - Rules are defined in DI_CFG_MAPPING_RULES'
 ,is_enquiry                                TINYINT(4)     					NOT NULL DEFAULT '0'             COMMENT 'Enquiry flag'
 ,is_noncashback                            TINYINT(4)     					NOT NULL DEFAULT '0'             COMMENT 'Non-cashback transactions flag'
 ,is_fast_payment                           TINYINT(4)     					NOT NULL DEFAULT '0'             COMMENT 'Fast payment purchase flag'
 ,is_source_deleted                         TINYINT(4)     					NOT NULL DEFAULT '0'             COMMENT 'Is underlying transaction(s) is deleted (0: no, 1: yes)'
 ,di_created_date                           TIMESTAMP      					DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP      					DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT            					NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_transaction_id, di_domain_id, transaction_date_id)
 ,INDEX idx_domain_id (di_domain_id) 
 ,INDEX idx_user_id (di_user_id)
 ,INDEX idx_user_hist_id (di_user_hist_id)
 ,INDEX idx_merchant_id (di_merchant_id)
 ,INDEX idx_merchant_hist_id (di_merchant_hist_id)
 ,INDEX idx_network_id (di_network_id)
 ,INDEX idx_click_id (domain_click_id)
 ,INDEX idx_domain_user_id (domain_user_id)
 ,INDEX idx_domain_merchant_id (domain_merchant_id)
 ,INDEX idx_transaction_date_id (transaction_date_id)
 ,INDEX idx_first_not_pending_or_denied_date_id (first_not_pending_or_denied_date_id)
 ,INDEX idx_transaction_datatime (transaction_datetime)
 ,INDEX idx_job_run_id_di_fact_transaction (job_run_id)
) COMMENT = 'DI Consolidated Transaction Fact Table. (Partitioned by domains - quidco.com, qipu.de, etc.)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED
 PARTITION BY RANGE (transaction_date_id)
 SUBPARTITION BY HASH (di_domain_id)
 SUBPARTITIONS 2 (
    PARTITION p_2006   VALUES LESS THAN (20070101),
    PARTITION p_2007_1 VALUES LESS THAN (20070701),
    PARTITION p_2007_2 VALUES LESS THAN (20080101),
    PARTITION p_2008_1 VALUES LESS THAN (20080701),
    PARTITION p_2008_2 VALUES LESS THAN (20090101),
    PARTITION p_2009_1 VALUES LESS THAN (20090701),
    PARTITION p_2009_2 VALUES LESS THAN (20100101),
    PARTITION p_2010_1 VALUES LESS THAN (20100701),
    PARTITION p_2010_2 VALUES LESS THAN (20110101),
    PARTITION p_2011_1 VALUES LESS THAN (20110701),
    PARTITION p_2011_2 VALUES LESS THAN (20120101),
    PARTITION p_2012_1 VALUES LESS THAN (20120701),
    PARTITION p_2012_2 VALUES LESS THAN (20130101),
    PARTITION p_2013_3 VALUES LESS THAN (20130701),
    PARTITION p_2013_4 VALUES LESS THAN (20140101),
    PARTITION p_2014_1 VALUES LESS THAN (20140401),
    PARTITION p_2014_2 VALUES LESS THAN (20140701),
    PARTITION p_2014_3 VALUES LESS THAN (20141001),
    PARTITION p_2014_4 VALUES LESS THAN (20150101),
    PARTITION p_2015_1 VALUES LESS THAN (20150401),
    PARTITION p_2015_2 VALUES LESS THAN (20150701),
    PARTITION p_2015_3 VALUES LESS THAN (20151001),
    PARTITION p_2015_4 VALUES LESS THAN (20160101),
    PARTITION p_2016_1 VALUES LESS THAN (20160401),
    PARTITION p_2016_2 VALUES LESS THAN (20160701),
    PARTITION p_2016_3 VALUES LESS THAN (20161001),
    PARTITION p_2016_4 VALUES LESS THAN (20170101),
    PARTITION p_2017_1 VALUES LESS THAN (20170401),
    PARTITION p_2017_2 VALUES LESS THAN (20170701),
    PARTITION p_2017_3 VALUES LESS THAN (20171001),
    PARTITION p_2017_4 VALUES LESS THAN (20180101),
    PARTITION p_2018_1 VALUES LESS THAN (20180401),
    PARTITION p_2018_2 VALUES LESS THAN (20180701),
    PARTITION p_2018_3 VALUES LESS THAN (20181001),
    PARTITION p_2018_4 VALUES LESS THAN (20190101),
    PARTITION p_2019_1 VALUES LESS THAN (MAXVALUE)
)
;