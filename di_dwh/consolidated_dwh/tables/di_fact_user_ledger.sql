DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_fact_user_ledger;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_fact_user_ledger(
  di_user_ledger_id                         INT            NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                              INT            NOT NULL                         COMMENT 'Domain_ID in Source'
 ,domain_user_ledger_id                     INT   UNSIGNED NOT NULL                         COMMENT 'ID in the source transactional database'
 ,di_user_id                                INT            NOT NULL                         COMMENT 'Reference to DI_DIM_USER table (FK)'
 ,di_user_hist_id                           INT            NOT NULL                         COMMENT 'Reference to DI_DIM_USER_HIST table (FK)'
 ,domain_user_id                            INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,created_date_id                           INT            NOT NULL DEFAULT '-1'            COMMENT 'Created Date ID'
 ,created_datetime                          DATETIME       NOT NULL                         COMMENT 'Created DATETIME'
 ,reference_code                            VARCHAR(10)    NOT NULL                         
 ,reference_identifier                      VARCHAR(50)    NOT NULL 
 ,debit                                     DECIMAL(10,2)  NULL
 ,credit                                    DECIMAL(10,2)  NULL
 ,balance                                   DECIMAL(10,2)  NOT NULL
 ,description                               VARCHAR(200)   NULL
 ,is_proprietary                            TINYINT        NOT NULL DEFAULT '0'
 ,di_created_date                           TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT            NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_user_ledger_id, di_domain_id, created_date_id, domain_user_ledger_id)
 ,INDEX idx_domain (di_domain_id)
 ,INDEX idx_user_id (di_user_id)
 ,INDEX idx_user_hist_id (di_user_hist_id)
 ,INDEX idx_click_date_id (created_date_id)
 ,INDEX idx_click_datatime (created_datetime)
 ,INDEX idx_job_run_id_di_fact_click(job_run_id)
) COMMENT = 'DI Consolidated Wallet User Ledger Fact Table)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED
 PARTITION BY RANGE (created_date_id)
SUBPARTITION BY HASH (di_domain_id)
SUBPARTITIONS 2
(PARTITION p_2016_1 VALUES LESS THAN (20160101) ENGINE = InnoDB,
 PARTITION p_2016_2 VALUES LESS THAN (20160401) ENGINE = InnoDB,
 PARTITION p_2016_3 VALUES LESS THAN (20160701) ENGINE = InnoDB,
 PARTITION p_2016_4 VALUES LESS THAN (20161001) ENGINE = InnoDB,
 PARTITION p_2017_1 VALUES LESS THAN (20170101) ENGINE = InnoDB,
 PARTITION p_2017_2 VALUES LESS THAN (20170401) ENGINE = InnoDB,
 PARTITION p_2017_3 VALUES LESS THAN (20170701) ENGINE = InnoDB,
 PARTITION p_2017_4 VALUES LESS THAN (20171001) ENGINE = InnoDB,
 PARTITION p_2018_1 VALUES LESS THAN (20180101) ENGINE = InnoDB,
 PARTITION p_2018_2 VALUES LESS THAN (20180401) ENGINE = InnoDB,
 PARTITION p_2018_3 VALUES LESS THAN (20180701) ENGINE = InnoDB,
 PARTITION p_2018_4 VALUES LESS THAN (20181001) ENGINE = InnoDB,
 PARTITION p_2019_1 VALUES LESS THAN (20190101) ENGINE = InnoDB,
 PARTITION p_2019_2 VALUES LESS THAN (20190401) ENGINE = InnoDB,
 PARTITION p_2019_3 VALUES LESS THAN (20190701) ENGINE = InnoDB,
 PARTITION p_2019_4 VALUES LESS THAN (20191001) ENGINE = InnoDB)
 ;