DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_cobrand;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_cobrand (
  di_lkp_cobrand_id       					INT           	  NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                                  INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,cobrand_name                      			VARCHAR(100)  NOT NULL                         COMMENT 'Cobrand names: rewards4cricket, rewards4fishing'
 ,is_included				                    TINYINT(4)	  NOT NULL DEFAULT 1               COMMENT 'Cobrand inclusion flag (0/1)'
 ,is_included_comment							VARCHAR(100)  DEFAULT NULL			   		   COMMENT 'Brief description about the reason of is_included flag'
 ,di_created_date                               TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                    INT SIGNED    NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_lkp_cobrand_id)
 ,INDEX idx_job_run_id_di_lkp_cobrand(job_run_id)
) COMMENT =  'Cobrand lookup table'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;