DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_demographics;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_demographics (
  di_demographics_lkp_id           		  INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id							  INT(11)			NOT NULL						 COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'	
 ,lkp_item								  VARCHAR(15)		NOT NULL						 COMMENT 'Lookup item for age band, income band and for occupation'
 ,category_id							  INT(1)			NOT NULL						 COMMENT 'IDs for age band, income band and for occupation'
 ,category_code							  VARCHAR(15)										 COMMENT 'Code for the category - occupation'
 ,category_name							  VARCHAR(100)										 COMMENT 'Category Name for the lookup'
 ,gt									  INT(11)											 COMMENT 'Min range for the band'
 ,lt									  INT(11)											 COMMENT 'Max range for the band'
 ,di_created_date                         TIMESTAMP         DEFAULT CURRENT_TIMESTAMP      	 COMMENT 'Row creation date'
 ,PRIMARY KEY (di_demographics_lkp_id)
 ,INDEX idx_stg_load_date (di_created_date)
) COMMENT = 'Lookup table for the age_band, income_bane and for occupation for quidco.com domain, Scientia, Seopa, Cint'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;
