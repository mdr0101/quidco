DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_merchant_cluster;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_merchant_cluster (
  di_lkp_merchant_cluster_id          INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                        INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_merchant_id                  INT           NOT NULL                         COMMENT 'Domain specific merchant id'
 ,merchant_cluster_id                 INT           NOT NULL                         COMMENT 'Merchant cluster id'
 ,merchant_cluster_name               VARCHAR(100)  NOT NULL                         COMMENT 'Merchant cluster name'
 ,penetration                         DECIMAL(9,6)  NOT NULL                         COMMENT 'Merchant cluster penetration'
 ,di_created_date                     TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (di_lkp_merchant_cluster_id)
 ,UNIQUE KEY nk_di_lkp_merchant_cluster (di_domain_id, domain_merchant_id)
) COMMENT =  'Cross-domain static merchant clusters'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
