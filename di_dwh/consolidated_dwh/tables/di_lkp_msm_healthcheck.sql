DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_msm_healthcheck;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_msm_healthcheck (
  msm_healthcheck_id                      INT                      NOT NULL AUTO_INCREMENT    COMMENT 'Primary Key/Surrogate Key'
 ,check_name                              VARCHAR(250)             NOT NULL						        COMMENT 'Name of the check'
 ,check_sql_output_type                   ENUM('NUMBER', 'STRING') NOT NULL                   COMMENT 'Supported outputs: NUMERIC, STRING'
 ,check_sql                               TEXT                     NOT NULL						        COMMENT 'SQL query - this must return a number in one column and in one row'
 ,expected_number                         INT                                                 COMMENT 'Expected numeric result of the check SQL query'
 ,percentage_threshold                    DECIMAL(5,2)                                        COMMENT 'Threshold in Percentage if check type is NUMBER'
 ,expected_string                         VARCHAR(250)                                        COMMENT 'Expected string result of the check SQL query'
 ,di_created_date                         TIMESTAMP                DEFAULT CURRENT_TIMESTAMP  COMMENT 'Row creation date'
 ,PRIMARY KEY (msm_healthcheck_id)
 ,INDEX idx_check_name (check_name)
) COMMENT = 'Lookup table for DI DWH Healtcheck - SQLs, expected results, thresholds'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;
