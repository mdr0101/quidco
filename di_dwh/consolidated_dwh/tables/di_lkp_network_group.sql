DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_network_group;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_network_group (
  di_network_lkp_id           INT             NOT NULL AUTO_INCREMENT    COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                INT             NOT NULL                   COMMENT ''
 ,domain_network              VARCHAR(10)     NOT NULL                   COMMENT ''
 ,network_group_id            INT             NOT NULL                   COMMENT ''
 ,network_group_name          VARCHAR(100)    NOT NULL                   COMMENT ''
 ,di_created_date             TIMESTAMP       DEFAULT CURRENT_TIMESTAMP  COMMENT 'Row creation date'
 ,PRIMARY KEY (di_network_lkp_id)
 ,UNIQUE KEY nk_lkp_network_group (di_domain_id, domain_network)
) COMMENT = 'Lookup table for network groups'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;
