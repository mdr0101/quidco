  DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_postcode;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_postcode (
  di_postcode_lkp_id           		  		INT               	NOT NULL AUTO_INCREMENT          	COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id							  	INT(11)				NOT NULL						 	COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'	
 ,postcode						 			VARCHAR(15)			NOT NULL							COMMENT ''
 ,area										VARCHAR(10)			NOT NULL							COMMENT ''
 ,area_name									VARCHAR(100)		NOT NULL							COMMENT ''
 ,region									VARCHAR(10)			NOT NULL							COMMENT ''
 ,region_name								VARCHAR(100)		NOT NULL							COMMENT ''
 ,latitude 									DECIMAL(10,6) 		NOT NULL							COMMENT ''
 ,longitude 								DECIMAL(10,6) 		NOT NULL							COMMENT ''
 ,easting 									INT(11) 			NOT NULL							COMMENT ''
 ,northing 									INT(11) 			NOT NULL							COMMENT ''
 ,grid_ref 									VARCHAR(100) 		NOT NULL							COMMENT ''
 ,county 									VARCHAR(100) 		NOT NULL							COMMENT ''
 ,district 									VARCHAR(100) 		NOT NULL							COMMENT ''
 ,ward 										VARCHAR(100) 		NOT NULL							COMMENT ''
 ,district_code								VARCHAR(100) 		NOT NULL							COMMENT ''
 ,ward_code 								VARCHAR(100) 		NOT NULL							COMMENT ''
 ,country 									VARCHAR(100) 		NOT NULL							COMMENT ''
 ,county_code 								VARCHAR(100) 		NOT NULL							COMMENT ''
 ,introduced 								DATETIME			NOT NULL							COMMENT ''
 ,terminated_dt								DATETIME			NOT NULL							COMMENT ''
 ,di_created_date                         	TIMESTAMP         DEFAULT CURRENT_TIMESTAMP      	 		COMMENT 'Row creation date'
 ,PRIMARY KEY (di_postcode_lkp_id)
 ,INDEX idx_stg_load_date (di_created_date)
) COMMENT = 'Lookup table for postcode'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;