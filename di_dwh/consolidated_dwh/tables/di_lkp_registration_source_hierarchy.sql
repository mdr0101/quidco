DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_registration_source_hierarchy;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_lkp_registration_source_hierarchy (
  di_lkp_registration_source_hierarchy_id       INT           NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                                  INT           NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,registration_method_id                        INT           NOT NULL                         COMMENT '1: ORGANIC, 2: REFERRAL_CAMPAIGN, 3: REFER_A_FRIEND, 4: COBRAND'
 ,registration_method_name                      VARCHAR(100)  NOT NULL                         COMMENT '1: ORGANIC, 2: REFERRAL_CAMPAIGN, 3: REFER_A_FRIEND, 4: COBRAND'
 ,registration_method_attr_1                    VARCHAR(250)  NOT NULL                         COMMENT 'Registration method specific attribute nr 1'
 ,registration_method_attr_2                    VARCHAR(250)  NOT NULL                         COMMENT 'Registration method specific attribute nr 2'
 ,registration_method_attr_3                    VARCHAR(250)  NOT NULL                         COMMENT 'Registration method specific attribute nr 3'
 ,registration_source_l1_id                     INT           NOT NULL                         COMMENT 'Registration Source Level 1 ID'
 ,registration_source_l1_name                   VARCHAR(100)  NOT NULL                         COMMENT 'Registration Source Level 1 Name'
 ,registration_source_l2_id                     INT           NOT NULL                         COMMENT 'Registration Source Level 2 ID'
 ,registration_source_l2_name                   VARCHAR(100)  NOT NULL                         COMMENT 'Registration Source Level 2 Name'
 ,registration_source_l3_id                     INT           NOT NULL                         COMMENT 'Registration Source Level 3 ID'
 ,registration_source_l3_name                   VARCHAR(100)  NOT NULL                         COMMENT 'Registration Source Level 3 Name'
 ,registration_source_l4_id                     INT           NOT NULL                         COMMENT 'Registration Source Level 4 ID'
 ,registration_source_l4_name                   VARCHAR(100)  NOT NULL                         COMMENT 'Registration Source Level 4 Name'
 ,di_created_date                               TIMESTAMP     DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                    INT SIGNED    NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (`di_lkp_registration_source_hierarchy_id`)
 ,INDEX idx_job_run_id_di_lkp_registration_source_hierarchy(job_run_id)
) COMMENT =  'Registration source hierarchy lookup table'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;

