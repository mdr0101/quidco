DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_sum_user_rfv_hist;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_sum_user_rfv_hist (
  di_sum_user_rfv_hist_id       INT           NOT NULL  AUTO_INCREMENT            COMMENT 'Primary Key/Surrogate Key'
 ,effective_date_id             INT           NOT NULL  DEFAULT '0'
 ,di_domain_id                  INT           NOT NULL                            COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,is_cobrand                    TINYINT(4)    NOT NULL  DEFAULT '0'               COMMENT '(Cobrand) - Is Cobrand flag (0: no, 1: yes)'
 ,freemium_type_l2_id           INT           NOT NULL                            COMMENT '(Freemium) - Freemium Type level 2 ID'
 ,freemium_type_l2_name         VARCHAR(100)  NOT NULL                            COMMENT '(Freemium) - Freemium Type level 2 name'
 ,registration_date_id          INT           NOT NULL                            COMMENT '(Reg/Auth) - Date of registration. Reference to DI_DIM_DATE table (FK)'
 ,registration_source_l1_id     INT           NOT NULL                            COMMENT '(Reg Source) - Registration Source level 1 ID'
 ,registration_source_l1_name   VARCHAR(100)  NOT NULL                            COMMENT '(Reg Source) - Registration Source level 1 name'
 ,registration_source_l2_id     INT           NOT NULL                            COMMENT '(Reg Source) - Registration Source level 2 ID'
 ,registration_source_l2_name   VARCHAR(100)  NOT NULL                            COMMENT '(Reg Source) - Registration Source level 2 name'
 ,registration_source_l3_id     INT           NOT NULL                            COMMENT '(Reg Source) - Registration Source level 3 ID'
 ,registration_source_l3_name   VARCHAR(100)  NOT NULL                            COMMENT '(Reg Source) - Registration Source level 3 name'
 ,registration_source_l4_id     INT           NOT NULL                            COMMENT '(Reg Source) - Registration Source level 4 ID'
 ,registration_source_l4_name   VARCHAR(100)  NOT NULL                            COMMENT '(Reg Source) - Registration Source level 4 name'
 ,rfv_we                        INT           NOT NULL                            COMMENT 'Number of uses in Welcome and Engage RFV group'
 ,rfv_we_acquired               INT           NOT NULL                            COMMENT 'Number of new users in Welcome and Engage'
 ,rfv_we_same                   INT           NOT NULL                            COMMENT 'Number of users was W&E in the previous period in the same RFV subgroup'
 ,rfv_we_up                     INT           NOT NULL                            COMMENT 'Number of users was W&E in the previous period but in lower RFV subgroup'
 ,rfv_we_down                   INT           NOT NULL                            COMMENT 'Number of users was W&E in the previous period but in higher subgroup'
 ,rfv_rd                        INT           NOT NULL                            COMMENT 'Number of users in Retain and Develop RFV group'
 ,rfv_rd_from_we                INT           NOT NULL                            COMMENT 'Number of users transferred from W&E to R&D'
 ,rfv_rd_from_rc                INT           NOT NULL                            COMMENT 'Number of users transferred from R&C to R&D'
 ,rfv_rd_up                     INT           NOT NULL                            COMMENT 'Number of users was R&D in the previous period but in lower RFV subgroup'
 ,rfv_rd_down                   INT           NOT NULL                            COMMENT 'Number of users was R&D in the previous period but in higher RFV subgroup'
 ,rfv_rc                        INT           NOT NULL                            COMMENT 'Number of users in Recovery and Churn RFV group'
 ,rfv_rc_from_we                INT           NOT NULL                            COMMENT 'Number of users transferred from W&E to R&C'
 ,rfv_rc_from_rd                INT           NOT NULL                            COMMENT 'Number of users transferred from R&D to R&C'
 ,rfv_rc_up                     INT           NOT NULL                            COMMENT 'Number of users was R&C in the previous period but in lower RFV subgroup'
 ,rfv_rc_down                   INT           NOT NULL                            COMMENT 'Number of users was R&C in the previous period but in higher RFV subgroup'
 ,in_month_actives              INT           NOT NULL                            COMMENT 'Number of users who made the first valid purchase in the month of registration'
 ,di_created_date               TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT 'Row creation date'
 ,di_last_updated_date          TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP  COMMENT 'Last updated date'
 ,job_run_id                    INT           NOT NULL                            COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY pk_stat_segments (di_sum_user_rfv_hist_id)
 ,UNIQUE KEY (effective_date_id
             ,di_domain_id
             ,is_cobrand
             ,freemium_type_l2_id
             ,registration_date_id
             ,registration_source_l1_id
             ,registration_source_l2_id
             ,registration_source_l3_id
             ,registration_source_l4_id)
) ENGINE=InnoDB
;

