DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_merchant_cluster_rfv_sample;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_merchant_cluster_rfv_sample (
  di_wrk_merchant_cluster_rfv_sample_id     INT              NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                              INT              NOT NULL                         COMMENT ''
 ,domain_merchant_id                        INT              NOT NULL                         COMMENT ''
 ,domain_user_id                            INT              NOT NULL                         COMMENT ''
 ,rfv_subgroup_id                           INT              NOT NULL                         COMMENT ''
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,PRIMARY KEY (di_wrk_merchant_cluster_rfv_sample_id)
 ,INDEX idx_domain_id (di_domain_id)
 ,INDEX idx_domain_user_id (domain_user_id)
) COMMENT = 'DI Work table - Intermediate table to fill DI_LKP_MERCHANT_CLUSTER table'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
;
