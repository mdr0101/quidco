DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_transaction;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_transaction (
  di_transaction_id                         INT   UNSIGNED NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key'
 ,di_domain_id                              INT            NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,di_purchase_id                            VARCHAR(100)   NOT NULL DEFAULT 'NA'            COMMENT 'Reference to DI_DIM_PURCHASE table (FK)'
 ,di_legacy_purchase_id                     INT   		  						            COMMENT 'Reference to old legacy database quidco_reporting.DI_PURCHASE.purchase_id table (FK)'
 ,domain_transaction_id                     INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific Transaction ID in the source transactional database'
 ,domain_user_id                            INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database' 
 ,domain_merchant_id                        INT            NOT NULL                         COMMENT 'Domain specific Merchant ID in the source transactional database'
 ,domain_click_id                           INT   UNSIGNED NOT NULL                         COMMENT 'Domain specific Click ID in the source transactional database'
 ,transaction_date                          DATETIME       NOT NULL                         COMMENT 'Transaction datetime'
 ,is_valid                                  TINYINT(4)     NOT NULL DEFAULT '1'             COMMENT 'Is valid flag. (0: no, 1: yes) - Rules are defined in DI_CFG_MAPPING_RULES'
 ,is_gross                                  TINYINT(4)     NOT NULL DEFAULT '0'             COMMENT 'Gross Flag'
 ,is_fast_payment                           TINYINT(4)     NOT NULL DEFAULT '0'             COMMENT 'Fast payment purchase flag'
 ,is_outlier								TINYINT(4)	   NOT NULL DEFAULT '0'				COMMENT 'Is outlier flag. (0:no, 1:yes) - Rules are defined in DI_CFG_MAPPING_RULES'
 ,is_enquiry                                TINYINT(4)     NOT NULL DEFAULT '0'             COMMENT 'Enquiry flag'
 ,is_noncashback                            TINYINT(4)     NOT NULL DEFAULT '0'             COMMENT 'Non-cashback transactions flag'
 ,domain_network                            VARCHAR(10)    NOT NULL                         COMMENT 'Domain specific Network name in the source transactional database'
 ,network_status                            VARCHAR(30)    NULL                             COMMENT 'Network status from source'
 ,spend                                     DECIMAL(10, 2) NOT NULL                         COMMENT 'Spend value'
 ,spend_imputed                             DECIMAL(10, 2) NOT NULL                         COMMENT 'Spend Imputed value'
 ,commission                                DECIMAL(10, 2) NOT NULL                         COMMENT 'Commission value'
 ,user_commission                           DECIMAL(10, 2) NOT NULL                         COMMENT 'User Commission value'
 ,status                                    INT            NOT NULL                         COMMENT 'Transaction status'
 ,payments_out_status                       INT            NOT NULL                         COMMENT 'Transaction payments out status'
 ,first_tracked_date                        DATETIME                                        COMMENT 'Date when the transaction has been tracked'
 ,first_confirmed_date                      DATETIME                                        COMMENT 'Date when the transaction has been confirmed'
 ,first_imported_date                       DATETIME                                        COMMENT 'Date when the transaction was imported'
 ,first_pending_date                        DATETIME                                        COMMENT 'Date when the transaction was in PENDING status at the first time'
 ,first_validated_date                      DATETIME                                        COMMENT 'Date when the transaction has been validated'
 ,first_declined_date                       DATETIME                                        COMMENT 'Date when the transaction has been declined'
 ,first_not_pending_or_denied_date          DATETIME                                        COMMENT 'Date when the transaction was not in PENDING or VALIDATED status at the first time'
 ,is_paypal_bonus                           TINYINT(4)     NOT NULL DEFAULT '0'             COMMENT 'Is PayPal Bonus flag (0: no, 1: yes)'
 ,is_source_deleted                         TINYINT(4)     NOT NULL DEFAULT '0'             COMMENT 'Is underlying transaction(s) is deleted (0: no, 1: yes)'
 ,di_created_date                           TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP      DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT            NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_transaction_id, di_domain_id)
 ,UNIQUE INDEX nk_di_wrk_transaction (di_domain_id, domain_transaction_id, is_noncashback)
 ,INDEX idx_di_purchase_id (di_purchase_id)
 ,INDEX idx_domain_user_id (domain_user_id)
 ,INDEX idx_di_last_updated_date (di_last_updated_date)
 ,INDEX idx_job_run_id_di_fact_transaction(job_run_id)
) COMMENT = 'DI Consolidated Transaction Fact Table. (Partitioned by domains - quidco.com, qipu.de, etc.)'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED
 PARTITION BY LIST (di_domain_id) (
    PARTITION p_quidco_com VALUES IN (1),
    PARTITION p_qipu_de VALUES IN (2),
	PARTITION p_shoop_fr VALUES IN (200)
);

