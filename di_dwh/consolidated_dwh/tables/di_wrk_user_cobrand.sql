DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_cobrand;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_cobrand (
  di_wrk_user_cobrand_id                    INT              NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                              INT              NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_user_id                            INT     UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,registration_date_id                      INT              NOT NULL                         COMMENT '(Reg/Auth) - Date of registration. Reference to DI_DIM_DATE table (FK)'
 ,is_cobrand 								TINYINT(4) 		 NOT NULL DEFAULT '0' 			  COMMENT '(Cobrand) - Is Cobrand flag (0: no, 1: yes)'
 ,cobrand_id	 							TINYINT(4) 		 NOT NULL DEFAULT '-1' 			  COMMENT 'Cobrand ID from di_lkp_cobrand'
 ,cobrand_name 								VARCHAR(100) 	 NOT NULL DEFAULT 'Unknown' 	  COMMENT 'Cobrand Name: rewards4cricket, rewards4fishing'
 ,is_new_or_updated							TINYINT(4) 		 NOT NULL DEFAULT '0' 			  COMMENT 'Row modification Flag 1 = new or updated, 0 = nothing changed '
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_wrk_user_cobrand_id, di_domain_id)
 ,UNIQUE INDEX nk_di_wrk_user_cobrand (di_domain_id, domain_user_id)
 ,INDEX idx_domain_user_id (domain_user_id)
 ,INDEX idx_registration_date_id (registration_date_id)
 ,INDEX idx_job_run_id_di_wrk_user_cobrand(job_run_id)
) COMMENT = 'DI Process table for Cobrand'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
  PARTITION BY LIST (di_domain_id) (
    PARTITION p_unknown VALUES IN (-1),
    PARTITION p_quidco_com VALUES IN (1),
    PARTITION p_qipu_de VALUES IN (2)
);