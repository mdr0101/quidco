DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_merchant_cluster_fragments;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_merchant_cluster_fragments (
  di_wrk_user_merchant_cluster_fragments_id   INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                                INT               NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_user_id                              INT     UNSIGNED  NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,tenure                                      DOUBLE            NOT NULL                         COMMENT ''
 ,freemium_type_l2_id                         INT               NOT NULL                         COMMENT ''
 ,quidco_opinions                             TINYINT(4)        NOT NULL                         COMMENT ''
 ,mobile_app                                  TINYINT(4)        NOT NULL                         COMMENT ''
 ,clicksnap_user                              TINYINT(4)        NOT NULL                         COMMENT ''
 ,last_12_month_valid_transactions            INT               NOT NULL                         COMMENT ''
 ,last_12_month_log_norm_trans                DOUBLE            NOT NULL                         COMMENT ''
 ,last_12_month_avg_cashback_per_valid_trans  DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,last_12_month_std_cashback_per_valid_trans  DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,online_instore                              TINYINT(4)        NOT NULL                         COMMENT ''
 ,cluster_1_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_2_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_3_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_4_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_5_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_6_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_7_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_8_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_9_fraction                          DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_10_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_11_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_12_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_13_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_14_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_15_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_16_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_17_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_18_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_19_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_20_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_21_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_22_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_23_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_24_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_25_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_26_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,cluster_27_fraction                         DECIMAL(10,3)     NOT NULL                         COMMENT ''
 ,di_created_date                             TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                        TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                  INT SIGNED        NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_wrk_user_merchant_cluster_fragments_id, di_domain_id)
 ,UNIQUE INDEX nk_di_wrk_user_merchant_cluster_fragments (di_domain_id, domain_user_id)
 ,INDEX idx_domain_user_id (domain_user_id)
 ,INDEX idx_job_run_id_di_wrk_user_merchant_cluster_fragments (job_run_id)
) COMMENT = 'DI Work table for user level merchant cluster fragmentation across the 27 merchant clusters'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
  PARTITION BY LIST (di_domain_id) (
    PARTITION p_unknown VALUES IN (-1),
    PARTITION p_quidco_com VALUES IN (1),
    PARTITION p_qipu_de VALUES IN (2)
)

