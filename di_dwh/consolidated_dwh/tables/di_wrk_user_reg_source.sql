DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_reg_source;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_reg_source (
  di_wrk_user_reg_source_id                 INT              NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                              INT              NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_user_id                            INT     UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,registration_method_id                    INT              NOT NULL                         COMMENT '(Reg Source) - Registration Method ID   (1: ORGANIC, 2: REFERRAL_CAMPAIGN, 3: REFER_A_FRIEND, 4: COBRAND)'
 ,registration_method_name                  VARCHAR(100)     NOT NULL                         COMMENT '(Reg Source) - Registration Method Name (1: ORGANIC, 2: REFERRAL_CAMPAIGN, 3: REFER_A_FRIEND, 4: COBRAND)'
 ,registration_method_attr_1                VARCHAR(250)     NOT NULL                         COMMENT '(Reg Source) - Registration method specific attribut nr 1'
 ,registration_method_attr_2                VARCHAR(250)     NOT NULL                         COMMENT '(Reg Source) - Registration method specific attribut nr 2'
 ,registration_method_attr_3                VARCHAR(250)     NOT NULL                         COMMENT '(Reg Source) - Registration method specific attribut nr 3'
 ,registration_source_is_migrated           TINYINT(4)       NOT NULL DEFAULT '0'             COMMENT '(Reg Source) - Multiple registration source (legacy) flag'
 ,registration_source_l1_id                 INT              NOT NULL                         COMMENT '(Reg Source) - Registration Source level 1 ID'
 ,registration_source_l1_name               VARCHAR(100)     NOT NULL                         COMMENT '(Reg Source) - Registration Source level 1 name'
 ,registration_source_l2_id                 INT              NOT NULL                         COMMENT '(Reg Source) - Registration Source level 2 ID'
 ,registration_source_l2_name               VARCHAR(100)     NOT NULL                         COMMENT '(Reg Source) - Registration Source level 2 name'
 ,registration_source_l3_id                 INT              NOT NULL                         COMMENT '(Reg Source) - Registration Source level 3 ID'
 ,registration_source_l3_name               VARCHAR(100)     NOT NULL                         COMMENT '(Reg Source) - Registration Source level 3 name'
 ,registration_source_l4_id                 INT              NOT NULL                         COMMENT '(Reg Source) - Registration Source level 4 ID'
 ,registration_source_l4_name               VARCHAR(100)     NOT NULL                         COMMENT '(Reg Source) - Registration Source level 4 name'
 ,is_new_or_updated							TINYINT(4) 		 NOT NULL DEFAULT 0 			  COMMENT 'Row modification Flag 1 = new or updated, 0 = nothing changed '
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_wrk_user_reg_source_id, di_domain_id)
 ,UNIQUE INDEX nk_di_wrk_user_main (di_domain_id, domain_user_id)
 ,INDEX idx_di_last_updated_date (di_last_updated_date)
 ,INDEX idx_job_run_id_di_dim_user(job_run_id)
) COMMENT = 'DI Work Single Registration source - Single Registration source attributes with no refs to dimension tables'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 PARTITION BY LIST (di_domain_id) (
    PARTITION p_quidco_com VALUES IN (1),
    PARTITION p_qipu_de VALUES IN (2),
	PARTITION p_shoop_fr VALUES IN (200)
);
