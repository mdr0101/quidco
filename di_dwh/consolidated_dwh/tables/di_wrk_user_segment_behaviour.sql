DROP TABLE IF EXISTS ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_segment_behaviour;
CREATE TABLE ${DB_DI_DWH_DWH_SCHEMA}.di_wrk_user_segment_behaviour (
  di_wrk_user_segment_behavioural_id        INT              NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,di_domain_id                              INT              NOT NULL                         COMMENT 'Reference to DI_DIM_DOMAIN table (FK)'
 ,domain_user_id                            INT     UNSIGNED NOT NULL                         COMMENT 'Domain specific User ID in the source transactional database'
 ,behavioural_segment_id					INT(11) 		 NOT NULL 						  COMMENT 'Lifecycle Status ID'
 ,behavioural_segment_name                  VARCHAR(50) 	 NOT NULL 						  COMMENT 'Lifecycle Status Name'
 ,is_new_or_updated							TINYINT(4) 		 NOT NULL DEFAULT '0' 			  COMMENT 'Row modification Flag 1 = new or updated, 0 = nothing changed '
 ,di_created_date                           TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,di_last_updated_date                      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row last updated date'
 ,job_run_id                                INT SIGNED       NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (di_wrk_user_segment_behavioural_id, di_domain_id)
 ,UNIQUE INDEX nk_di_wrk_user_behavioural_segment (di_domain_id, domain_user_id)
 ,INDEX idx_domain_user_id (domain_user_id)
 ,INDEX idx_job_run_id_di_wrk_user_behavioural_segment(job_run_id)
) COMMENT = 'DI Process table for User Behavioural Segment'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
  PARTITION BY LIST (di_domain_id) (
    PARTITION p_unknown VALUES IN (-1),
    PARTITION p_quidco_com VALUES IN (1),
    PARTITION p_qipu_de VALUES IN (2)
)
;
