-- -----------------------------------------------------------------------------
-- DI_DIM_DOMAIN
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_dim_domain;
INSERT INTO quidco_reporting_dev.di_dim_domain (di_domain_id
                                               ,di_domain_name
                                               ,domain_go_live_date_id
                                               ,domain_description)
 VALUES (-1, 'Unknown',          -1, 'Unknown')
       ,( 1, 'quidco.com', 20050101, 'http://www.quidco.com')
       ,( 2, 'qipu.de',    20140601, 'http://www.qipu.de')
;


-- -----------------------------------------------------------------------------
-- DI_DIM_USER
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_dim_user;
INSERT INTO quidco_reporting_dev.di_dim_user (di_user_id
                                             ,di_user_name
                                             ,di_domain_id
                                             ,domain_user_id
                                             ,registration_date_id
                                             ,is_authenticated
                                             ,signup_method_id
                                             ,signup_method_name
                                             ,is_cobrand
                                             ,was_cobrand
                                             ,first_purchase_date_id
                                             ,last_purchase_date_id
                                             ,transactions
                                             ,purchases
                                             ,first_non_pp_bonus_purchase_date_id
                                             ,last_non_pp_bonus_purchase_date_id
                                             ,non_pp_bonus_transactions
                                             ,non_pp_bonus_purchases
                                             ,rfv_group_id
                                             ,rfv_group_name
                                             ,rfv_subgroup_id
                                             ,rfv_subgroup_name
                                             ,lifecycle_status_id
                                             ,lifecycle_status_name
                                             ,age_band_id
                                             ,age_band_name
                                             ,gender_code
                                             ,gender_name
                                             ,postcode_area_code
                                             ,postcode_area_name
                                             ,region_id
                                             ,region_name
                                             ,freemium_type_l1_id
                                             ,freemium_type_l1_name
                                             ,freemium_type_l2_id
                                             ,freemium_type_l2_name
                                             ,freemium_type_l3_id
                                             ,freemium_type_l3_name
                                             ,registration_source_channel_id
                                             ,registration_source_channel_name
                                             ,registration_source_channel_attributes
                                             ,registration_source_l1_id
                                             ,registration_source_l1_name
                                             ,registration_source_l2_id
                                             ,registration_source_l2_name
                                             ,registration_source_l3_id
                                             ,registration_source_l3_name
                                             ,registration_source_l4_id
                                             ,registration_source_l4_name
                                             ,di_created_date
                                             ,di_last_updated_date
                                             ,di_job_id)
 VALUES (-1, '0 - Unknown'         , -1,       0,       -1, 0, -1, 'Unknown' , 0, 0,       -1,       -1,   0,   0,      - 1,       -1,   0,   0,  -1, 'Unknown'           , -1, 'Unknown'                             , -1, 'Unknown'               , -1, 'Unknown' , 'U', 'Unknown', '!' , 'Unknown'         , -1, 'Unknown'               , -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 , -1, 'Unknown'              , 'Unknown'                     , -1 , 'Uncategorised' , -1, 'Uncategorised', -1, 'Uncategorised'  , -1, 'Uncategorised'     , DATE(NOW()), DATE(NOW()), -1)
       ,( 1, '4411111 - quidco.com',  1, 4411111, 20140101, 0,  1, 'Normal'  , 0, 0, 20140401, 20150114,  23,  25, 20140402, 20150110,  24,  20,  50, 'Welcome and Engage', 53, 'Looking and Listening, not purchased',  1, 'Welcome'               ,  3, '18 to 24', 'M', 'Male'   , 'N' , 'London N (N)'    ,  5, 'North East'            ,  1, 'Quidco' ,  1, 'Quidco Basic'  ,  1, 'Quidco Basic Unconfirmed',  1, 'SEO'                  , 'press|Sunday Telegraph'      ,  1 , 'Above the line',  1, 'Outdoor'      ,  2, 'Digital Outdoor',  2, 'Train Volo'        , DATE(NOW()), DATE(NOW()), -1)
       ,( 2, '4411112 - quidco.com',  1, 4411112, 20140101, 1,  2, 'PayPal'  , 0, 0, 20140401, 20150112, 123, 125, 20140402, 20150116, 120, 120,  60, 'Retain and Develop', 62, 'Regular'                             ,  6, 'Retain'                ,  4, '18 to 24', 'M', 'Male'   , 'DE', 'Southampton (SO)',  9, 'South East'            ,  1, 'Quidco' ,  1, 'Quidco Basic'  ,  2, 'Quidco Basic Confirmed'  ,  1, 'SEO'                  , 'friend|Independent'          ,  1 , 'Above the line',  1, 'Outdoor'      ,  2, 'Digital Outdoor',  2, 'Train Volo'        , DATE(NOW()), DATE(NOW()), -1)
       ,( 3, '4411113 - quidco.com',  1, 4411113, 20150111, 1,  3, 'Facebook', 0, 0,       -1,       -1,   0,   0,       -1,       -1,   0,   0,  50, 'Welcome and Engage', 51, 'Receding, not purchased'             ,  1, 'Welcome'               ,  2, '25 to 34', 'F', 'Male'   , 'SO', 'London N (N)'    ,  5, 'North East'            ,  1, 'Quidco' ,  1, 'Quidco Premium',  1, 'Quidco Premium'          ,  2, 'Referral Campaign'    , 'ppc_Google Adwords Brand|abc',  2 , 'Below the line',  2, 'Admin Share'  ,  3, 'Affiliate'      ,  3, 'Tradedoubler'      , DATE(NOW()), DATE(NOW()), -1)
       ,( 4, '9911111 - qipu.de'   ,  2, 9911111, 20140601, 1,  1, 'Normal'  , 0, 0, 20140801, 20150120,  23,  25, 20140402, 20150111,  22,  20,  70, 'Recovery and Churn', 72, 'Perished'                            ,  1, 'Welcome'               ,  1, '18 to 24', 'F', 'Female' , 'YO', 'York (YO)'       , 13, 'Yorkshire & Humberside', -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 ,  0, 'None'                 , ''                            , -1 , 'Uncategorised' , -1, 'Uncategorised', -1, 'Uncategorised'  , -1, 'Uncategorised'     , DATE(NOW()), DATE(NOW()), -1)
       ,( 5, '9911112 - qipu.de'   ,  2, 9911112, 20140601, 1,  3, 'Facebook', 0, 0, 20140801, 20150103,  42,  34, 20140802, 20150103,  34,  42,  70, 'Retain and Develop', 63, 'Big Ticket'                          ,  4, 'Lapsed - Not purchased',  1, '35 to 44', 'F', 'Female' , 'M' , 'Manchester (M)'  ,  6, 'North West'            , -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 ,  3, 'RAF'                  , 'twitter|direct'              ,  2 , 'Below the line',  3, 'RAF'          ,  4, 'RAF Twitter'    ,  4, 'RAF Direct Twitter', DATE(NOW()), DATE(NOW()), -1) 
;

-- -----------------------------------------------------------------------------
-- DI_DIM_USER_HIST
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_dim_user_hist;
INSERT INTO quidco_reporting_dev.di_dim_user_hist (di_user_hist_id
                                                  ,di_user_name
                                                  ,di_domain_id
                                                  ,domain_user_id
                                                  ,registration_date_id
                                                  ,is_authenticated
                                                  ,signup_method_id
                                                  ,signup_method_name
                                                  ,is_cobrand
                                                  ,was_cobrand
                                                  ,first_purchase_date_id
                                                  ,last_purchase_date_id
                                                  ,transactions
                                                  ,purchases
                                                  ,first_non_pp_bonus_purchase_date_id
                                                  ,last_non_pp_bonus_purchase_date_id
                                                  ,non_pp_bonus_transactions
                                                  ,non_pp_bonus_purchases
                                                  ,rfv_group_id
                                                  ,rfv_group_name
                                                  ,rfv_subgroup_id
                                                  ,rfv_subgroup_name
                                                  ,lifecycle_status_id
                                                  ,lifecycle_status_name
                                                  ,freemium_type_l1_id
                                                  ,freemium_type_l1_name
                                                  ,freemium_type_l2_id
                                                  ,freemium_type_l2_name
                                                  ,freemium_type_l3_id
                                                  ,freemium_type_l3_name
                                                  ,registration_source_channel_id
                                                  ,registration_source_channel_name
                                                  ,registration_source_channel_attributes
                                                  ,registration_source_l1_id
                                                  ,registration_source_l1_name
                                                  ,registration_source_l2_id
                                                  ,registration_source_l2_name
                                                  ,registration_source_l3_id
                                                  ,registration_source_l3_name
                                                  ,registration_source_l4_id
                                                  ,registration_source_l4_name
                                                  ,valid_from
                                                  ,valid_to
                                                  ,is_latest
                                                  ,di_created_date
                                                  ,di_last_updated_date
                                                  ,di_job_id)
 VALUES (-1, '0 - Unknown'         , -1,       0,       -1, 0, -1, 'Unknown' , 0, 0,       -1,       -1,   0,   0,      - 1,       -1,   0,   0,  -1, 'Unknown'           , -1, 'Unknown'                             , -1, 'Unknown'               ,  -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 , -1, 'Unknown'              , 'Unknown'                     , -1 , 'Uncategorised' , -1, 'Uncategorised', -1, 'Uncategorised'  , -1, 'Uncategorised'     , '1900-01-01', '9999-01-01', 1, DATE(NOW()), DATE(NOW()), -1)
       ,( 1, '4411111 - quidco.com',  1, 4411111, 20140101, 0,  1, 'Normal'  , 0, 0, 20140401, 20150114,  25,  23, 20140402, 20150110,  24,  20,  50, 'Welcome and Engage', 53, 'Looking and Listening, not purchased',  1, 'Welcome'               ,   1, 'Quidco' ,  1, 'Quidco Basic'  ,  1, 'Quidco Basic Unconfirmed',  1, 'SEO'                  , 'press|Sunday Telegraph'      ,  1 , 'Above the line',  1, 'Outdoor'      ,  2, 'Digital Outdoor',  2, 'Train Volo'        , '2014-01-01', '9999-01-01', 1, DATE(NOW()), DATE(NOW()), -1)
       
       ,( 2, '4411112 - quidco.com',  1, 4411112, 20140101, 1,  2, 'PayPal'  , 1, 0, 20140401, 20150114,  10,  10, 20140402, 20140402,   9,   8,  50, 'Welcome and Engage', 54, 'Receding, purchased'                 ,  1, 'Welcome'               ,   1, 'Quidco' ,  1, 'Quidco Basic'  ,  2, 'Quidco Basic Unconfirmed',  1, 'SEO'                  , 'friend|Independent'          ,  1 , 'Above the line',  1, 'Outdoor'      ,  2, 'Digital Outdoor',  2, 'Train Volo'        , '2014-01-01', '2015-01-01', 0, DATE(NOW()), DATE(NOW()), -1)
       ,( 3, '4411112 - quidco.com',  1, 4411112, 20140101, 1,  2, 'PayPal'  , 0, 1, 20140401, 20150102,  52,  50, 20140402, 20150102,  48,  51,  50, 'Welcome and Engage', 54, 'Receding, purchased'                 ,  1, 'Welcome'               ,   1, 'Quidco' ,  1, 'Quidco Basic'  ,  1, 'Quidco Basic Unconfirmed',  1, 'SEO'                  , 'friend|Independent'          ,  1 , 'Above the line',  1, 'Outdoor'      ,  2, 'Digital Outdoor',  2, 'Train Volo'        , '2015-01-02', '2015-01-11', 0, DATE(NOW()), DATE(NOW()), -1)
       ,( 4, '4411112 - quidco.com',  1, 4411112, 20140101, 1,  2, 'PayPal'  , 0, 1, 20140401, 20150112, 125, 123, 20140402, 20150116, 120, 120,  60, 'Retain and Develop', 62, 'Regular'                             ,  6, 'Retain'                ,   1, 'Quidco' ,  1, 'Quidco Basic'  ,  1, 'Quidco Basic Confirmed'  ,  1, 'SEO'                  , 'friend|Independent'          ,  1 , 'Above the line',  1, 'Outdoor'      ,  2, 'Digital Outdoor',  2, 'Train Volo'        , '2014-01-12', '9999-01-01', 1, DATE(NOW()), DATE(NOW()), -1)
       
       ,( 5, '4411113 - quidco.com',  1, 4411113, 20150111, 0,  3, 'Facebook', 0, 0,       -1,       -1,   0,   0,       -1,       -1,   0,   0,  50, 'Welcome and Engage', 51, 'Receding, not purchased'             ,  1, 'Welcome'               ,   1, 'Quidco' ,  1, 'Quidco Premium',  1, 'Quidco Premium'          ,  2, 'Referral Campaign'    , 'ppc_Google Adwords Brand|abc',  2 , 'Below the line',  2, 'Admin Share'  ,  3, 'Affiliate'      ,  3, 'Tradedoubler'      , '2015-01-11', '2015-01-13', 0, DATE(NOW()), DATE(NOW()), -1)
       ,( 6, '4411113 - quidco.com',  1, 4411113, 20150111, 1,  3, 'Facebook', 0, 0,       -1,       -1,   0,   0,       -1,       -1,   0,   0,  50, 'Welcome and Engage', 51, 'Receding, not purchased'             ,  1, 'Welcome'               ,   1, 'Quidco' ,  1, 'Quidco Premium',  1, 'Quidco Premium'          ,  2, 'Referral Campaign'    , 'ppc_Google Adwords Brand|abc',  2 , 'Below the line',  2, 'Admin Share'  ,  3, 'Affiliate'      ,  3, 'Tradedoubler'      , '2015-01-14', '9999-01-01', 1, DATE(NOW()), DATE(NOW()), -1)
       
       ,( 7, '9911111 - qipu.de'   ,  2, 9911111, 20140601, 1,  1, 'Normal'  , 0, 0, 20140801, 20150120,  25,  23, 20140402, 20150111,  22,  20,  70, 'Recovery and Churn', 72, 'Perished'                            ,  1, 'Welcome'               ,  -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 ,  0, 'None'                 , ''                            , -1 , 'Uncategorised' , -1, 'Uncategorised', -1, 'Uncategorised'  , -1, 'Uncategorised'     , '2015-06-01', '9999-01-01', 1, DATE(NOW()), DATE(NOW()), -1)

       ,( 8, '9911112 - qipu.de'   ,  2, 9911112, 20140601, 1,  3, 'Facebook', 0, 0, 20140801, 20150103,  34,  42, 20140802, 20150103,  34,  42,  60, 'Retain and Develop', 62, 'Regular'                             ,  4, 'Lapsed - Not purchased',  -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 ,  3, 'RAF'                  , 'twitter|direct'              ,  2 , 'Below the line',  3, 'RAF'          ,  4, 'RAF Twitter'    ,  4, 'RAF Direct Twitter', '2014-06-01', '2014-11-17', 0, DATE(NOW()), DATE(NOW()), -1)
       ,( 9, '9911112 - qipu.de'   ,  2, 9911112, 20140601, 1,  3, 'Facebook', 0, 0, 20140801, 20150103,  34,  42, 20140802, 20150103,  34,  42,  60, 'Retain and Develop', 63, 'Big Ticket'                          ,  4, 'Lapsed - Not purchased',  -1, 'Unknown', -1, 'Unknown'       , -1, 'Unknown'                 ,  3, 'RAF'                  , 'twitter|direct'              ,  2 , 'Below the line',  3, 'RAF'          ,  4, 'RAF Twitter'    ,  4, 'RAF Direct Twitter', '2014-11-18', '9999-01-01', 1, DATE(NOW()), DATE(NOW()), -1)
;

-- -----------------------------------------------------------------------------
-- DI_DIM_MERCHANT
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_dim_merchant;
INSERT INTO quidco_reporting_dev.di_dim_merchant (di_merchant_id
                                                 ,di_merchant_name
                                                 ,di_domain_id
                                                 ,domain_merchant_id
                                                 ,domain_merchant_name
                                                 ,merchant_category_l4_id
                                                 ,merchant_category_l4_name
                                                 ,merchant_category_l3_id
                                                 ,merchant_category_l3_name
                                                 ,merchant_category_l2_id
                                                 ,merchant_category_l2_name
                                                 ,merchant_category_l1_id
                                                 ,merchant_category_l1_name
                                                 ,is_instore
                                                 ,instore_type_id
                                                 ,instore_type_name
                                                 ,has_instore_control_group
                                                 ,di_created_date
                                                 ,di_last_updated_date
                                                 ,di_job_id)
 VALUES (-1, 'Unknown'                                 , -1,   -1, 'Unknown'             ,  -1, 'Unknown'                   ,  -1, 'Unknown'                 ,  -1, 'Unknown'                       ,  -1, 'Unknown'     , 0, -1, 'Unknown'     , 0, NOW(), NOW(), -1)
       ,( 1, 'Debenhams (148 - quidco.com)'            ,  1,  148, 'Debenhams'           ,  71, 'L4: Department Stores'     ,  23, 'L3: General Superstores' ,  12, 'L2: General Retail'            ,   5, 'L1: Retail'  , 0,  0, 'Not In-Store', 0, NOW(), NOW(), -1)
       ,( 2, 'Debenhams (In-store) (4054 - quidco.com)',  1, 4058, 'Debenhams (In-store)',  71, 'L4: Department Stores'     ,  23, 'L3: General Superstores' ,  12, 'L2: General Retail'            ,   5, 'L1: Retail'  , 1,  0, 'In-Store'    , 0, NOW(), NOW(), -1)
       ,( 3, 'Hotels.com (244 - qipu.de)'              ,  2,  244, 'Hotels.com'          , 266, 'L4: Online Travel Agents'  ,  82, 'L3: Online Travel Agents',  14, 'L2: Flights, Holidays & Hotels',   6, 'L1: Travel'  , 0,  0, 'Not In-Store', 0, NOW(), NOW(), -1)
       ,( 4, 'Myprotein (6680 - qipu.de)'              ,  2, 6680, 'Myprotein'           , 216, 'L4: Vitamins & Supplements',  30, 'L3: Health'              ,  13, 'L2: Health & Beauty'           ,   5, 'L1: Retail'  , 0,  0, 'Not In-Store', 0, NOW(), NOW(), -1)
;

-- -----------------------------------------------------------------------------
-- DI_DIM_NETWORK
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_dim_network;
INSERT INTO quidco_reporting_dev.di_dim_network (di_network_id
                                                ,di_network_name
                                                ,di_domain_id
                                                ,domain_network
                                                ,contact
                                                ,email
                                                ,currency
                                                ,is_card_linked_network
                                                ,di_created_date
                                                ,di_last_updated_date
                                                ,di_job_id)
 VALUES (-1, 'Unknown'         , -1, 'Unknown', 'Unknown', 'Unknown'                       , 'Unknown', 0, NOW(), NOW(), -1)
       ,( 1, 'QCO (quidco.com)',  1, 'QCO'    , ''       , ''                              , 'GBP'    , 0, NOW(), NOW(), -1)
       ,( 2, 'AWq (quidco.com)',  1, 'AWq'    , ''       , 'affiliates@affiliatewindow.com', 'GBP'    , 0, NOW(), NOW(), -1)
       ,( 3, 'BB (quidco.com)' ,  1, 'BB'     , ''       , ''                              , 'GBP'    , 1, NOW(), NOW(), -1)
       ,( 4, 'QIP (qipu.de)'   ,  2, 'QIP'    , ''       , ''                              , 'EUR'    , 0, NOW(), NOW(), -1)
;

-- -----------------------------------------------------------------------------
-- DI_FACT_PURCHASE
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_fact_purchase;
INSERT INTO quidco_reporting_dev.di_fact_purchase (di_purchase_id
                                                  ,di_domain_id
                                                  ,di_user_id
                                                  ,di_user_hist_id
                                                  ,di_merchant_id
                                                  ,di_network_id
                                                  ,di_click_id
                                                  ,purchase_date_id
                                                  ,purchase_datetime
                                                  ,spend
                                                  ,spend_imputed
                                                  ,commission
                                                  ,user_commission
                                                  ,transactions
                                                  ,is_paypal_paid
                                                  ,is_valid
                                                  ,di_source_deleted_flag
                                                  ,di_created_date
                                                  ,di_last_updated_date
                                                  ,di_job_id)
 VALUES ( 1, 1, -1, 1, -1,  1, -1, 20140104, NOW(), 123, 123, 50, 48, 2, 0, 1, 0, NOW(), NOW(), -1)
       ,( 2, 1,  2, 3,  2,  2,  1, 20140105, NOW(),  11,  11, 10, 10, 1, 1, 1, 0, NOW(), NOW(), -1)
       ,( 3, 1,  2, 4,  2,  2,  2, 20150105, NOW(), 110,  85, 40, 20, 3, 0, 1, 0, NOW(), NOW(), -1)
       ,( 4, 2,  5, 8,  4,  4, -1, 20150102, NOW(),  11,  11, 10, 10, 1, 1, 1, 0, NOW(), NOW(), -1)
;

-- -----------------------------------------------------------------------------
-- DI_FACT_CLICK
-- -----------------------------------------------------------------------------
TRUNCATE TABLE quidco_reporting_dev.di_fact_click;
INSERT INTO quidco_reporting_dev.di_fact_click (di_click_id
                                               ,di_domain_id
                                               ,domain_click_id
                                               ,di_user_id
                                               ,di_user_hist_id
                                               ,di_merchant_id
                                               ,di_network_id
                                               ,ip
                                               ,click_date_id
                                               ,click_datetime
                                               ,is_valid
                                               ,di_created_date
                                               ,di_last_updated_date
                                               ,di_job_id)
VALUES (-1, -1,    -1, -1, -1, -1, -1,            '',       -1, '1900-01-01', 0, NOW(), NOW(), -1)
      ,( 1,  1, 12341,  2,  4,  3,  2, '10.10.10.10', 20150126,        NOW(), 1, NOW(), NOW(), -1)
      ,( 2,  1, 12342,  2,  4,  3,  2, '10.10.10.11', 20150127,        NOW(), 1, NOW(), NOW(), -1)
      ,( 3,  2, 98765,  5,  8,  4,  4, '80.10.10.80', 20150120,        NOW(), 1, NOW(), NOW(), -1)
      
;

-- -----------------------------------------------------------------------------
-- Test Queries
-- -----------------------------------------------------------------------------

-- Select every purchase connecting to every dimension
SELECT *
  FROM di_fact_purchase p
      ,di_dim_date      p_date
      ,di_dim_merchant  m
      ,di_dim_network   n
      ,di_dim_user      u
      ,di_dim_user_hist u_hist
      ,di_dim_domain    d
 WHERE p_date.date_id         = p.purchase_date_id
   AND m.di_merchant_id       = p.di_merchant_id
   AND n.di_network_id        = p.di_network_id
   AND u.di_user_id           = p.di_user_id      
   AND u_hist.di_user_hist_id = p.di_user_hist_id
   AND d.di_domain_id         = p.di_domain_id
;

-- Select every click connecting to every dimension
SELECT *
  FROM di_fact_click    c
      ,di_dim_date      c_date
      ,di_dim_merchant  m
      ,di_dim_network   n
      ,di_dim_user      u
      ,di_dim_user_hist u_hist
      ,di_dim_domain    d
 WHERE c_date.date_id         = c.click_date_id
   AND m.di_merchant_id       = c.di_merchant_id
   AND n.di_network_id        = c.di_network_id
   AND u.di_user_id           = c.di_user_id
   AND u_hist.di_user_hist_id = c.di_user_hist_id
   AND d.di_domain_id         = c.di_domain_id
;
