CREATE OR REPLACE VIEW ${DB_DI_DWH_DWH_SCHEMA}.vw_di_cube_dim_date
AS

-- -----------------------------------------------------------------------------
-- Load only a reduced number of dates into the cube and add date period members
-- -----------------------------------------------------------------------------
SELECT date_id
      ,date_date
      ,date_char
      ,calendar_year_int
      ,calendar_year_char
      ,calendar_half_year_int
      ,calendar_half_year_char
      ,calendar_quarter_int
      ,calendar_quarter_char
      ,calendar_month_int
      ,calendar_month_char
      ,week_commencing_int
      ,week_commencing_char
      ,week_commencing_year_int
      ,week_commencing_year_char
      ,fiscal_year_int
      ,fiscal_year_char
      ,fiscal_quarter_int
      ,fiscal_quarter_char
      ,fiscal_week_commencing_int
      ,fiscal_week_commencing_char
      ,day_of_week_int
      ,day_of_week_char
      ,day_of_week_sort_order
      ,CASE
         WHEN date_date BETWEEN (NOW() + INTERVAL  -8 DAY) AND NOW() + INTERVAL -1 DAY THEN 1
         WHEN date_date BETWEEN (NOW() + INTERVAL -15 DAY) AND NOW() + INTERVAL -8 DAY THEN 2
         WHEN date_date BETWEEN (NOW() + INTERVAL  -1 YEAR + INTERVAL -8 DAY) AND NOW() + INTERVAL -1 YEAR + INTERVAL -1 DAY THEN 3
         WHEN date_date BETWEEN (NOW() + INTERVAL  -1 YEAR + INTERVAL -15 DAY) AND NOW() + INTERVAL -1 YEAR + INTERVAL -8 DAY THEN 4
         ELSE 0
       END analytical_period_id
      ,CASE
         WHEN date_date BETWEEN (NOW() + INTERVAL  -8 DAY) AND NOW() + INTERVAL -1 DAY THEN 'Last 7 Days'
         WHEN date_date BETWEEN (NOW() + INTERVAL -15 DAY) AND NOW() + INTERVAL -8 DAY THEN 'Previous 7 Days'
         WHEN date_date BETWEEN (NOW() + INTERVAL  -1 YEAR + INTERVAL -8 DAY) AND NOW() + INTERVAL -1 YEAR + INTERVAL -1 DAY THEN 'Last 7 Days YoY'
         WHEN date_date BETWEEN (NOW() + INTERVAL  -1 YEAR + INTERVAL -15 DAY) AND NOW() + INTERVAL -1 YEAR + INTERVAL -8 DAY THEN 'Previous 7 Days YoY'
         ELSE 'Neutral'
       END analytical_period_name
  FROM di_dim_date
 WHERE date_id <= DATE_FORMAT(NOW(), '%Y%m%d')
    OR date_id = -1;
