CREATE OR REPLACE VIEW ${DB_DI_DWH_DWH_SCHEMA}.vw_di_cube_dim_user
AS

-- -----------------------------------------------------------------------------
--
-- -----------------------------------------------------------------------------
SELECT di_user_id
      ,di_user_name
      ,di_domain_id
      ,domain_user_id
      ,registration_date_id
      ,registration_time_id
      ,registration_datetime
      ,email
      ,is_authenticated
      ,is_facebook_linked
      ,is_paypal_linked
      ,is_quidco_opinions_linked
      ,quidco_opinions_register_date_id
      ,is_mailable
      ,signup_method_id
      ,signup_method_name
      ,is_cobrand
      -- -----------------------------------------------------------------------
      -- Move it into DI_DIM_USER and load by talend
      -- -----------------------------------------------------------------------
      ,CASE
         WHEN     u.registration_date_id != -1
              AND u.first_valid_purchase_date_id != -1
              AND SUBSTR(u.registration_date_id,1,6) = SUBSTR(u.first_valid_purchase_date_id,1,6) THEN 1
         ELSE 0
       END                          is_in_month_actived
      ,CASE
         WHEN valid_transactions > 0 THEN 1
         ELSE 0
       END is_active
      ,cobrand_id
      ,cobrand_name
      ,was_cobrand
      ,CASE
         WHEN first_purchase_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN first_purchase_date_id < 20050101 THEN -1
         ELSE first_purchase_date_id 
       END first_purchase_date_id
      ,CASE
         WHEN last_purchase_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN last_purchase_date_id < 20050101 THEN -1
         ELSE last_purchase_date_id 
       END last_purchase_date_id
      ,transactions
      ,valid_transactions
      ,purchases
      ,valid_purchases
      -- -----------------------------------------------------------------------
      -- Replace date ids which not in the cube date dim (VW_DI_CUBE_DIM_DATE)
      -- -----------------------------------------------------------------------
      ,CASE
         WHEN first_valid_purchase_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN first_valid_purchase_date_id < 20050101 THEN -1
         ELSE first_valid_purchase_date_id 
       END first_valid_purchase_date_id
      ,CASE
         WHEN last_valid_purchase_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN last_valid_purchase_date_id < 20050101 THEN -1
         ELSE last_valid_purchase_date_id 
       END last_valid_purchase_date_id
      ,first_non_pp_bonus_purchase_date_id
      ,last_non_pp_bonus_purchase_date_id
      ,non_pp_bonus_transactions
      ,non_pp_bonus_purchases
      ,rfv_group_id
      ,rfv_group_name
      ,rfv_subgroup_id
      ,rfv_subgroup_name
      ,lifecycle_status_id
      ,lifecycle_status_name
      ,behavioural_segment_id
      ,behavioural_segment_name
      ,freemium_type_l1_id
      ,freemium_type_l1_name
      ,freemium_type_l2_id
      ,freemium_type_l2_name
      ,freemium_type_l3_id
      ,freemium_type_l3_name
      ,registration_method_id
      ,registration_method_name
      ,registration_method_attr_1
      ,registration_method_attr_2
      ,registration_method_attr_3
      ,registration_source_is_migrated
      ,registration_source_l1_id
      ,registration_source_l1_name
      ,registration_source_l2_id
      ,registration_source_l2_name
      ,registration_source_l3_id
      ,registration_source_l3_name
      ,registration_source_l4_id
      ,registration_source_l4_name
      ,dob_id
      ,age
      ,age_band_id
      ,age_band_name
      ,gender
      ,income_band_id
      ,income_band_name
      ,occupation_code
      ,occupation_name
      ,postcode
      ,area_code
      ,area_name
      ,region_code
      ,region_name
      ,nr_of_children
      ,di_created_date
      ,di_last_updated_date
      ,job_run_id
  FROM di_dim_user u
 ;
