CREATE OR REPLACE VIEW ${DB_DI_DWH_DWH_SCHEMA}.vw_di_cube_fact_click
AS

-- -----------------------------------------------------------------------------
-- Load only a reduced number of clicks into the cube
-- -----------------------------------------------------------------------------
SELECT di_click_id
      ,di_domain_id
      ,domain_user_id
      ,domain_merchant_id
      ,domain_network
      ,domain_click_id
      ,di_user_id
      ,di_user_hist_id
      ,di_merchant_id
      ,di_merchant_hist_id
      ,di_network_id
      ,ip
      ,click_date_id
      ,click_time_id
      ,click_datetime
      ,is_valid
      ,di_created_date
      ,di_last_updated_date
      ,job_run_id
  FROM di_fact_click
 WHERE click_date_id >= 20130801
   AND click_date_id <= DATE_FORMAT(NOW(), '%Y%m%d');
