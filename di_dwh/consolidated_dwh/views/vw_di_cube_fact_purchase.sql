CREATE OR REPLACE VIEW ${DB_DI_DWH_DWH_SCHEMA}.vw_di_cube_fact_purchase
AS

-- -----------------------------------------------------------------------------
-- Load only a reduced number of purchase into the cube and add
--  IS_FIRST_PURCHASE calculated column
-- -----------------------------------------------------------------------------
SELECT p.di_purchase_id
      ,p.di_domain_id
      ,p.di_user_id
      ,p.di_user_hist_id
      ,p.di_merchant_id
      ,p.di_merchant_hist_id
      ,p.di_network_id
      ,p.domain_user_id
      ,p.domain_merchant_id
      ,p.domain_network
      ,p.domain_click_id
      ,p.purchase_date_id
      ,p.purchase_time_id
      ,p.purchase_datetime
      -- -----------------------------------------------------------------------
      -- Replace date ids which not in the cube date dim (VW_DI_CUBE_DIM_DATE)
      -- -----------------------------------------------------------------------
      ,CASE
         WHEN p.first_imported_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN p.first_imported_date_id < 20050101 THEN -1
         ELSE p.first_imported_date_id
       END first_imported_date_id
      ,CASE
         WHEN p.first_validated_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN p.first_validated_date_id < 20050101 THEN -1
         ELSE p.first_validated_date_id
       END first_validated_date_id
      ,CASE
         WHEN p.first_not_pending_or_denied_date_id > DATE_FORMAT(NOW(), '%Y%m%d') THEN -1
         WHEN p.first_not_pending_or_denied_date_id < 20050101 THEN -1
         ELSE p.first_not_pending_or_denied_date_id
       END first_not_pending_or_denied_date_id
      ,p.spend
      ,p.spend_imputed
      ,p.commission
      ,p.user_commission
      ,p.di_purchase_status_id
      ,p.di_payments_out_status_id
      ,p.transactions
 	  ,(CASE 
		 WHEN (`u`.`registration_date_id` = -(1)) THEN -(1) 
		 WHEN isnull((to_days(`p`.`purchase_date_id`) - to_days(cast(`u`.`registration_date_id` as date)))) THEN -(1) 
		 WHEN ((to_days(`p`.`purchase_date_id`) - to_days(cast(`u`.`registration_date_id` as date))) <= -(1)) THEN -(1) 
	     ELSE ((to_days(`p`.`purchase_date_id`) - to_days(cast(`u`.`registration_date_id` as date))) + 1) END) 
	   AS registration_to_purchase_duration_id
      ,p.is_paypal_paid
      ,p.is_valid
      ,p.is_gross
      ,p.is_outlier
      ,p.is_enquiry
      ,p.is_noncashback
      ,p.is_fast_payment
      ,p.is_source_deleted
      ,p.di_created_date
      ,p.di_last_updated_date
      ,p.job_run_id
      ,CASE
         WHEN p.purchase_date_id = u.first_purchase_date_id THEN 1
         ELSE 0
        END is_first_purchase
      ,u.registration_date_id user_registration_date_id 
  FROM di_fact_purchase p
      ,di_dim_user u
 WHERE u.di_user_id = p.di_user_id
   AND p.purchase_date_id >= 20130801
   AND p.purchase_date_id <= DATE_FORMAT(NOW(), '%Y%m%d');
