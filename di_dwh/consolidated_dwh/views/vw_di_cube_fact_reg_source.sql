CREATE OR REPLACE VIEW ${DB_DI_DWH_DWH_SCHEMA}.vw_di_cube_fact_reg_source
AS

-- -----------------------------------------------------------------------------
-- This view is to display the Registered and Actives users side by side
-- in Excel OLAP pivots (Reg Source Fact)
-- -----------------------------------------------------------------------------
SELECT u.di_domain_id               di_domain_id
      ,CASE
         WHEN u.registration_date_id < 20050101 THEN -1
         ELSE u.registration_date_id
       END                          report_date_id
      ,1                            is_registered
      ,CASE
         WHEN SUBSTR(u.registration_date_id,1,6) = SUBSTR(u.first_valid_purchase_date_id,1,6) THEN 1
         ELSE 0
       END                          is_active
      ,u.di_user_id                 di_user_id
 FROM di_dim_user u
WHERE u.registration_date_id <= DATE_FORMAT(NOW(), '%Y%m%d')
;
