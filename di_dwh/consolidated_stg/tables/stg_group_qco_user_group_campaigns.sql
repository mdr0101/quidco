DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_group_campaigns;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_group_campaigns (
  stg_group_campaign_id           INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'  
 ,group_campaign_id               INT                                                COMMENT ''
 ,group_campaign_name             VARCHAR(100)                                       COMMENT ''
 ,group_campaign_description      VARCHAR(500)                                       COMMENT ''
 ,group_programme_id              INT                                                COMMENT ''
 ,last_modified_datetime          DATETIME                                           COMMENT ''
 ,last_modified_by                VARCHAR(100)                                       COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_group_campaign_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_group_qco_user_group_campaigns (job_run_id)
) COMMENT = 'Staging table for group campaigns from the Groups server'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
