DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_group_programmes;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_group_programmes (
  stg_group_programmes_id         INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,group_programme_id              INT                                                COMMENT ''
 ,group_programme_name            VARCHAR(100)                                       COMMENT ''
 ,group_programme_description     VARCHAR(500)                                       COMMENT ''
 ,billing_model                   VARCHAR(100)                                       COMMENT ''
 ,last_modified_datetime          DATETIME                                           COMMENT ''
 ,last_modified_by                VARCHAR(100)                                       COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_group_programmes_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_group_qco_user_group_programmes (job_run_id)
) COMMENT = 'Staging table for group programmes from the Groups server'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
