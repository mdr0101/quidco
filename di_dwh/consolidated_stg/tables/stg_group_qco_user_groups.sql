DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_groups;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_groups (
  stg_group_id                    INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'  
 ,group_id                        INT                                                COMMENT ''
 ,group_campaign_id               INT                                                COMMENT ''
 ,group_name                      VARCHAR(100)                                       COMMENT ''
 ,description                     VARCHAR(500)                                       COMMENT ''
 ,serialized_data                 TEXT                                               COMMENT ''
 ,scheduled                       TINYINT                                            COMMENT ''
 ,frequency                       VARCHAR(100)                                       COMMENT ''
 ,last_generated                  DATETIME                                           COMMENT ''
 ,active                          TINYINT                                            COMMENT ''
 ,end_date                        DATETIME                                           COMMENT ''
 ,max_days_in_group               INT                                                COMMENT ''
 ,current_users                   INT                                                COMMENT ''
 ,adhoc_run                       TINYINT                                            COMMENT ''
 ,parent_id                       INT                                                COMMENT ''
 ,control_group_params            TEXT                                               COMMENT ''
 ,include_cobrand                 TINYINT                                            COMMENT ''
 ,type                            VARCHAR(100)                                       COMMENT ''
 ,new_user_action                 VARCHAR(100)                                       COMMENT ''
 ,edit_user_action                VARCHAR(100)                                       COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_group_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_group_qco_user_groups (job_run_id)
) COMMENT = 'Staging table for user groups from the Groups server'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
