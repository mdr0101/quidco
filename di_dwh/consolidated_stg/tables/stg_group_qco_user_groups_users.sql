DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_groups_users;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_group_qco_user_groups_users (
  stg_group_user_id               INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'  
 ,user_groups_users_id            INT                                                COMMENT ''
 ,group_id                        INT                                                COMMENT ''
 ,user_id                         INT      UNSIGNED DEFAULT NULL                     COMMENT ''
 ,start_date                      DATETIME                                           COMMENT ''
 ,end_date                        DATETIME                                           COMMENT ''
 ,last_modified                   TIMESTAMP                                          COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_group_user_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_group_qco_user_groups_users (job_run_id)
) COMMENT = 'Staging table for user groups users from the Groups server'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED;
