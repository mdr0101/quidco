DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_categories;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_categories (
  stg_category_id                           INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,category_id                               VARCHAR(500)                                       COMMENT ''
 ,name                                      VARCHAR(500)                                       COMMENT ''
 ,description                               TEXT                                               COMMENT ''
 ,live                                      VARCHAR(500)                                       COMMENT ''
 ,sectionid                                 VARCHAR(500)                                       COMMENT ''
 ,catid                                     VARCHAR(500)                                       COMMENT ''
 ,stg_load_date                             TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_category_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_categories (job_run_id)
) COMMENT = 'Staging table for the network dimension from the qipu.de domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;