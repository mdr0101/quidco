DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_category_relationships;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_category_relationships (
  stg_cat_rel_id                            INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,parent_id                                 VARCHAR(500)                                       COMMENT ''
 ,child_id                                  VARCHAR(500)                                       COMMENT ''
 ,stg_load_date                             TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_cat_rel_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_category_relationships (job_run_id)
) COMMENT = 'Staging table for the network dimension from the qipu.de domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;