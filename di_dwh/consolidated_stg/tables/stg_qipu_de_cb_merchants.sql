DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_merchants;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_merchants (
  stg_merchant_id                           INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,merchant_id                               INT                                                COMMENT ''
 ,name                                      VARCHAR(500)                                       COMMENT ''
 ,intro_text                                TEXT                                               COMMENT ''
 ,description                               TEXT                                               COMMENT ''
 ,extra_text                                TEXT                                               COMMENT ''
 ,admin_notes                               TEXT                                               COMMENT ''
 ,enquiry_message                           TEXT                                               COMMENT ''
 ,allow_enquiries                           VARCHAR(500)                                       COMMENT ''
 ,meta                                      VARCHAR(500)                                       COMMENT ''
 ,adult                                     VARCHAR(500)                                       COMMENT ''
 ,min_enquiry_days                          INT                                                COMMENT ''
 ,max_enquiry_weeks                         INT                                                COMMENT ''
 ,max_denied_weeks                          INT                                                COMMENT ''
 ,site_url                                  VARCHAR(500)                                       COMMENT ''
 ,country                                   VARCHAR(500)                                       COMMENT ''
 ,image                                     VARCHAR(500)                                       COMMENT ''
 ,url_name                                  VARCHAR(500)                                       COMMENT ''
 ,rep_id                                    INT                                                COMMENT ''
 ,tips                                      TEXT                                               COMMENT ''
 ,shop_host                                 VARCHAR(500)                                       COMMENT ''
 ,shop_path                                 VARCHAR(500)                                       COMMENT ''
 ,restricted                                INT                                                COMMENT ''
 ,agency_id                                 VARCHAR(500)                                       COMMENT ''
 ,transaction_ref_label                     VARCHAR(500)                                       COMMENT ''
 ,customer_ref_label                        VARCHAR(500)                                       COMMENT ''
 ,detail_label                              VARCHAR(500)                                       COMMENT ''
 ,search_keywords                           VARCHAR(4000)                                      COMMENT ''
 ,transaction_limit                         INT                                                COMMENT ''
 ,transaction_limit_days                    INT                                                COMMENT ''
 ,title_seo                                 VARCHAR(500)                                       COMMENT ''
 ,stg_load_date                             TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_merchant_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_merchants (job_run_id)
) COMMENT = 'Staging table for the merchant dimension from the qipu.de domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;