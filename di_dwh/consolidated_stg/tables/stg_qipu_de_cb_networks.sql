DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_networks;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_networks (
  stg_network_id                            INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,name                                      VARCHAR(500)                                       COMMENT ''
 ,user_id_param                             VARCHAR(500)                                       COMMENT ''
 ,contact                                   VARCHAR(500)                                       COMMENT ''
 ,email                                     VARCHAR(500)                                       COMMENT ''
 ,currency                                  VARCHAR(500)                                       COMMENT ''
 ,allow_deep_links                          VARCHAR(500)                                       COMMENT ''
 ,stg_load_date                             TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_network_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_networks (job_run_id)
) COMMENT = 'Staging table for the network dimension from the qipu.de domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;