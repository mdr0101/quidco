/* Create staging table for cb_transaction_states_validation */
DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_transaction_states_validation;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_transaction_states_validation (
  stg_transaction_states_validation_id        	INT               NOT NULL AUTO_INCREMENT       COMMENT 'Primary Key/Surrogate Key'
 ,transaction_id							  	INT(11)											COMMENT ''
 ,validation_state_id						  	INT(11)											COMMENT ''
 ,validation_state_created						DATETIME										COMMENT ''
 ,blocked_state_id								INT(11)											COMMENT ''
 ,blocked_state_created							DATETIME										COMMENT ''
 ,claimed_state_id								INT(11)											COMMENT ''
 ,claimed_state_created							DATETIME										COMMENT ''
 ,stg_load_date                   				TIMESTAMP         DEFAULT CURRENT_TIMESTAMP     COMMENT 'Row creation date'
 ,job_run_id                      				INT SIGNED    	  NOT NULL             			COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_transaction_states_validation_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_transaction_states_validation (job_run_id)
) COMMENT = 'Staging table for transaction states validation from qipu.de domain'
;