DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_transactions;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_transactions (
  stg_transaction_id              INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,transaction_id                  INT                                                COMMENT ''
 ,transaction_sequence            INT                                                COMMENT ''
 ,network                         VARCHAR(250)                                       COMMENT ''
 ,network_transaction_id          VARCHAR(250)                                       COMMENT ''
 ,merchant_id                     INT                                                COMMENT ''
 ,network_merchant_id             VARCHAR(250)                                       COMMENT ''
 ,network_reference               VARCHAR(300)                                       COMMENT ''
 ,user_id                         INT      UNSIGNED                                  COMMENT ''
 ,percentage_id                   INT                                                COMMENT ''
 ,click_id                        INT      UNSIGNED                                  COMMENT ''
 ,date                            DATETIME                                           COMMENT ''
 ,amount                          DECIMAL(10, 2)                                     COMMENT ''
 ,commission                      DECIMAL(10, 2)                                     COMMENT ''
 ,user_commission                 DECIMAL(10, 2)                                     COMMENT ''
 ,status                          VARCHAR(250)                                       COMMENT ''
 ,payment_out_id                  INT                                                COMMENT ''
 ,imported                        VARCHAR(250)                                       COMMENT ''
 ,post_cutoff                     VARCHAR(250)                                       COMMENT ''
 ,created                         DATETIME                                           COMMENT ''
 ,last_imported                   TIMESTAMP                                          COMMENT ''
 ,copy_to_state                   VARCHAR(250)                                       COMMENT ''
 ,last_modified                   TIMESTAMP                                          COMMENT ''
 ,enquiry_id                      INT                                                COMMENT ''
 ,date_id                         INT                                                COMMENT ''
 ,comment                         VARCHAR(300)                                       COMMENT ''
 ,network_status                  VARCHAR(255)                                       COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_transaction_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_transactions (job_run_id)
) COMMENT = 'Staging table for transaction/purchase fact from qipu.de domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
