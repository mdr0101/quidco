DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_users;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_users (
  stg_user_id                 INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_id                     VARCHAR(250)                                       COMMENT ''
 ,name                        VARCHAR(250)                                       COMMENT ''
 ,username                    VARCHAR(250)                                       COMMENT ''
 ,email                       VARCHAR(250)                                       COMMENT ''
 ,unauthenticated_email       VARCHAR(250)                                       COMMENT ''
 ,password                    VARCHAR(250)                                       COMMENT ''
 ,usertype                    VARCHAR(250)                                       COMMENT ''
 ,block                       VARCHAR(250)                                       COMMENT ''
 ,sendEmail                   VARCHAR(250)                                       COMMENT ''
 ,gid                         VARCHAR(250)                                       COMMENT ''
 ,registerDate                TIMESTAMP                                          COMMENT ''
 ,lastvisitDate               TIMESTAMP                                          COMMENT ''
 ,activation                  VARCHAR(250)                                       COMMENT ''
 ,params                      TEXT                                               COMMENT ''
 ,payment_method              VARCHAR(250)                                       COMMENT ''
 ,payablename                 VARCHAR(250)                                       COMMENT ''
 ,paypal_verified             VARCHAR(250)                                       COMMENT ''
 ,bank_accountholder          VARCHAR(250)                                       COMMENT ''
 ,bank_account_number         VARCHAR(250)                                       COMMENT ''
 ,bank_clearingnumber         VARCHAR(250)                                       COMMENT ''
 ,minimum_payout              FLOAT(10,2)                                        COMMENT ''
 ,payment_cap                 FLOAT(10,2)                                        COMMENT ''
 ,hold_payments_till          DATETIME                                           COMMENT ''
 ,to_be_paid                  VARCHAR(250)                                       COMMENT ''
 ,do_not_pay                  VARCHAR(250)                                       COMMENT ''
 ,admin_notes                 TEXT                                               COMMENT ''
 ,ignore_country              VARCHAR(250)                                       COMMENT ''
 ,hide_gambling               VARCHAR(250)                                       COMMENT ''
 ,hide_adult                  VARCHAR(250)                                       COMMENT ''
 ,chat_privileges             VARCHAR(250)                                       COMMENT ''
 ,cobrand_child_id            VARCHAR(250)                                       COMMENT ''
 ,street_address              VARCHAR(250)                                       COMMENT ''
 ,zip_code                    VARCHAR(250)                                       COMMENT ''
 ,city                        VARCHAR(255)                                       COMMENT ''
 ,state                       VARCHAR(250)                                       COMMENT ''
 ,country                     VARCHAR(250)                                       COMMENT ''
 ,first_visit                 VARCHAR(250)                                       COMMENT ''
 ,referer_id                  VARCHAR(250)                                       COMMENT ''
 ,knowsusfrom                 VARCHAR(250)                                       COMMENT ''
 ,bitcoin_address             VARCHAR(250)                                       COMMENT ''
 ,paypal_account_holder       VARCHAR(250)                                       COMMENT ''
 ,payment_automatic           VARCHAR(250)                                       COMMENT ''
 ,ads_accepted                VARCHAR(250)                                       COMMENT ''
 ,stg_load_date               TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                  INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_user_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_cb_users (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;