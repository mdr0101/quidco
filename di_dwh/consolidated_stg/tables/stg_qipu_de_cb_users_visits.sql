DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_users_visits;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_cb_users_visits (
  user_id int(10) unsigned NOT NULL DEFAULT '0',
  last_visit timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  stg_load_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Row creation date',
  job_run_id int(11) NOT NULL COMMENT 'Modified to reference di_etl_job_log',
  PRIMARY KEY (user_id),
  KEY last_visit (last_visit)
) COMMENT = 'Staging table for the user visits for qipu domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;