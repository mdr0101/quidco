DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_qipu_referer;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_qipu_de_qipu_referer (
  stg_user_id                 INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,referer_id                  VARCHAR(250)      NOT NULL                         COMMENT ''
 ,campaign                    VARCHAR(250)      NOT NULL                         COMMENT ''
 ,http_referer                VARCHAR(250)      NOT NULL                         COMMENT ''
 ,first_seen                  VARCHAR(250)      NOT NULL                         COMMENT ''
 ,stg_load_date               TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                  INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_user_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_qipu_de_qipu_referer (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;