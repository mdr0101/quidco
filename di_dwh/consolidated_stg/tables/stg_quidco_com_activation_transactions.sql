/* New stage table: stg_quidco_com_activation_transactions */
DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_activation_transactions;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_activation_transactions (
  stg_activation_transaction_id   INT               					NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,activation_id                   INT      			UNSIGNED                                  			 COMMENT ''
 ,transaction_id                  INT      			UNSIGNED                                  			 COMMENT ''
 ,user_id                         INT                                     					 			 COMMENT ''
 ,activation_rule_id              INT                                                					 COMMENT ''
 ,source                          VARCHAR(250)                                       					 COMMENT ''
 ,latitude                        DECIMAL(10, 6)                                     					 COMMENT ''
 ,longitude                       DECIMAL(10, 6)                                     					 COMMENT ''
 ,merchant_id                     INT                                                					 COMMENT ''
 ,merchant_store_id               INT                                                					 COMMENT ''
 ,created                         DATETIME                                           					 COMMENT ''
 ,stg_load_date                   TIMESTAMP         					DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	  NOT NULL                         					 COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_activation_transaction_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_instore_activations (job_run_id)
) COMMENT = 'Staging table for DI_FACT_ACTIVATION_TRANSACTION fact from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;