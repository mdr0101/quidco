/* New Stage table: stg_quidco_com_cint_profile_answers */ 
DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_cint_profile_answers;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_cint_profile_answers (
  stg_cint_profile_answers_id           INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,id 									INT												   COMMENT ''
 ,question_id							INT												   COMMENT ''
 ,position 								INT												   COMMENT ''
 ,text 									VARCHAR(500)									   COMMENT ''
 ,enabled 								VARCHAR(10)										   COMMENT ''
 ,created 								DATETIME										   COMMENT ''
 ,last_modified 						DATETIME										   COMMENT ''
 ,stg_load_date                         TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                            INT SIGNED        NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_cint_profile_answers_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_cint_profile_answers (job_run_id)
) COMMENT = 'Staging table for Cint profile answers from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;