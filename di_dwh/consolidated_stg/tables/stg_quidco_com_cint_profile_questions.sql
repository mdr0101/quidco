DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_cint_profile_questions;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_cint_profile_questions (
  stg_cint_profile_questions_id           INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,id 									  INT												 COMMENT ''
 ,category_id 							  INT												 COMMENT ''
 ,position 								  INT												 COMMENT ''
 ,type 									  VARCHAR(500) 										 COMMENT ''
 ,text 									  VARCHAR(500) 										 COMMENT ''
 ,enabled 								  VARCHAR(10)										 COMMENT ''
 ,answer_type 							  VARCHAR(15)										 COMMENT ''
 ,created 								  DATETIME											 COMMENT ''
 ,last_modified 						  DATETIME 											 COMMENT '' 
 ,stg_load_date                           TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                              INT SIGNED        NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_cint_profile_questions_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_cint_profile_questions (job_run_id)
) COMMENT = 'Staging table for Cint profile questions from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ; 