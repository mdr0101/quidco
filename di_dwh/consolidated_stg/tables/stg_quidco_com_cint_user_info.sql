/* New Stage table: stg_quidco_com_cint_user_info */
DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_cint_user_info;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_cint_user_info (
  stg_cint_user_info_id 				INT 						NOT NULL AUTO_INCREMENT COMMENT 'Primary Key/Surrogate Key'
 ,user_id 								INT													COMMENT ''
 ,DOB 									VARCHAR(8)											COMMENT ''
 ,postcode 								VARCHAR(200)										COMMENT '' 
 ,stg_load_date 						timestamp 		NOT NULL DEFAULT CURRENT_TIMESTAMP 	COMMENT 'Row creation date'
 ,job_run_id 							INT 						NOT NULL 				COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_cint_user_info_id)
 ,KEY idx_stg_load_date (stg_load_date)
 ,KEY idx_job_run_id_stg_quidco_com_cint_user_info (job_run_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Staging table for the Cint demographics from the quidco.com domain'
;