DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_clicks;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_clicks (
  stg_click_id                        INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,click_id                            INT                                                COMMENT ''
 ,user_id                             INT                                                COMMENT ''
 ,merchant_id                         INT                                                COMMENT ''
 ,network                             VARCHAR(500)                                       COMMENT ''
 ,ip                                  VARCHAR(500)                                       COMMENT '' 
 ,timestamp                           TIMESTAMP                                          COMMENT ''
 ,stg_load_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                          INT SIGNED        NOT NULL                         COMMENT 'ETL job ID'
 ,PRIMARY KEY (stg_click_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_stg_job_id (job_run_id)
) COMMENT = 'Staging table for the DI_FACT_CLICK fact table from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;