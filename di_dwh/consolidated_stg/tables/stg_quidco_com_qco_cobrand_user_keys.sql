DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_cobrand_user_keys;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_cobrand_user_keys (
  stg_cobrand_user_key_id             INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,cobrand_user_key_id                 INT                                                COMMENT ''
 ,cobrand_prefix                      VARCHAR(250)                                       COMMENT ''
 ,user_id                             INT                                                COMMENT ''
 ,user_key                            VARCHAR(250)                                       COMMENT ''
 ,token                               VARCHAR(250)                                       COMMENT ''
 ,token_created                       DATETIME                                           COMMENT ''
 ,stg_load_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                          INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_cobrand_user_key_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_cobrand_user_keys (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;