DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_link_clicks;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_link_clicks (
  stg_link_click_id                        INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,link_click_id                            INT                                                COMMENT ''
 ,parent_id                                INT                                                COMMENT ''
 ,parent_type                              VARCHAR(500)                                       COMMENT ''
 ,clicked                                  TIMESTAMP                                          COMMENT ''
 ,user_id                                  INT                                                COMMENT ''
 ,logged_in_user_id                        INT                                                COMMENT ''
 ,merchant_id                              INT                                                COMMENT ''
 ,network                                  VARCHAR(500)                                       COMMENT ''
 ,click_id                                 INT                                                COMMENT ''
 ,referrer_id                              INT                                                COMMENT ''
 ,referrer_type_id                         INT                                                COMMENT ''
 ,stg_load_date                            TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                               INT SIGNED        NOT NULL                         COMMENT 'ETL job ID'
 ,PRIMARY KEY (stg_link_click_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_stg_job_id (job_run_id)
) COMMENT = 'Staging table for the DI_FACT_CLICK fact table from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;