DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_mailer_user_preference;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_mailer_user_preference (
  stg_mailer_user_preference_id   INT               NOT NULL AUTO_INCREMENT         COMMENT 'Primary Key/Surrogate Key'
 ,mailer_user_preference_id       INT                                               COMMENT ''
 ,user_id                         INT UNSIGNED                                      COMMENT ''
 ,mailer_message_type_id          INT UNSIGNED                                      COMMENT ''
 ,wants_messages                  VARCHAR(100)                                      COMMENT ''
 ,send_frequency                  VARCHAR(100)                                      COMMENT ''
 ,last_modified 						      DATETIME										                      COMMENT ''
 ,last_modified_by                VARCHAR(100)                                      COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP       COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED        NOT NULL                        COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_mailer_user_preference_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_mailer_user_preference (job_run_id)
) COMMENT = 'Staging table for Mailer User Preferences for IS_MAILABLE into DI_DIM_USER(_HIST) tables'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;
