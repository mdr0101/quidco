DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_mobile_users;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_mobile_users (
  stg_mobile_users_id                 INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_id                             INT                                                COMMENT ''
 ,device_type                         VARCHAR(100)                                       COMMENT ''
 ,app_release                         TINYINT                                            COMMENT ''
 ,app_major                           TINYINT                                            COMMENT ''
 ,app_minor                           TINYINT                                            COMMENT ''
 ,updated                             DATETIME                                           COMMENT ''
 ,prompt_for_review                   TINYINT                                            COMMENT ''
 ,stg_load_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                          INT SIGNED        NOT NULL                         COMMENT 'ETL job ID'
 ,PRIMARY KEY (stg_mobile_users_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_stg_job_id (job_run_id)
) COMMENT = 'Staging table for IS_MOBILE_USER column in DI_DIM_USER table from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;