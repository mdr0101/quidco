DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_noncashback_transactions;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_noncashback_transactions (
  stg_noncashback_transactions_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key/Surrogate Key'
 ,noncashback_transaction_id  int(11) DEFAULT NULL
 ,network varchar(5) DEFAULT NULL
 ,network_transaction_id varchar(100) DEFAULT NULL
 ,merchant_id INT(11) unsigned DEFAULT NULL
 ,network_merchant_id varchar(100) DEFAULT NULL
 ,user_id int(11) DEFAULT NULL
 ,click_id int(10) DEFAULT NULL
 ,date datetime DEFAULT NULL
 ,amount decimal(10,2) DEFAULT NULL
 ,processed_amount decimal(10,2) DEFAULT NULL
 ,network_commission decimal(10,2) DEFAULT NULL
 ,processed_commission decimal(10,2) DEFAULT NULL
 ,network_user_commission decimal(10,2) DEFAULT NULL
 ,processed_user_commission decimal(10,2) DEFAULT NULL
 ,status varchar(20)
 ,payments_out_status varchar(20)
 ,do_not_process int(11)
 ,stg_load_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Row creation date'
 ,job_run_id int(11) NOT NULL COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_noncashback_transactions_id)
 ,KEY idx_stg_load_date (stg_load_date)
 ,KEY idx_job_run_id_stg_quidco_com_qco_noncashback_transactions (job_run_id)
) COMMENT = 'Staging table for noncashback transactions from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ,ROW_FORMAT=COMPRESSED;
