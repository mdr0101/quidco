DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_payments_on_status_programs;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_payments_on_status_programs (
   stg_payments_on_status_programs_id				INT(11) 				NOT NULL AUTO_INCREMENT 						COMMENT 'Primary Key/Surrogate Key'
  ,program_id 										INT(11) unsigned 		NOT NULL 
  ,merchant_id 										SMALLINT(5) unsigned 	DEFAULT NULL
  ,pay_on_status 									VARCHAR(25)				DEFAULT NULL
  ,start_date 										DATETIME 				DEFAULT NULL
  ,end_date 										DATETIME 				DEFAULT NULL
  ,group_id 										INT(11) 				DEFAULT NULL
  ,hide_on_front_end 								TINYINT(1) 				DEFAULT NULL
  ,delay_days 										INT(11) 				DEFAULT NULL
  ,stg_load_date 									TIMESTAMP 				NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Row creation date'
  ,job_run_id 										INT(11) 				NOT NULL COMMENT 'Modified to reference di_etl_job_log'
  ,PRIMARY KEY (stg_payments_on_status_programs_id)
  ,KEY idx_merchant_startdt_enddt_pay_on_status (merchant_id, start_date, end_date, pay_on_status)
  ,KEY idx_stg_load_date(stg_load_date)
  ,KEY idx_job_run_id_stg_quidco_com_payments_on_status_program (job_run_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Staging table for Payments Status Programs from quidco.com domain'
;