DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_referral_campaigns;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_referral_campaigns (
  stg_referral_campaign_id                    INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,referral_campaign_id                        INT                                                COMMENT ''  
 ,channel                                     VARCHAR(250)                                       COMMENT ''
 ,channel_id                                  INT                                                COMMENT ''
 ,referrer_id                                 INT                                                COMMENT ''
 ,campaign                                    VARCHAR(250)                                       COMMENT ''
 ,hide_gambling                               VARCHAR(250)                                       COMMENT ''
 ,hide_adult                                  VARCHAR(250)                                       COMMENT ''
 ,cookie_expire_offset                        INT                                                COMMENT ''
 ,start_date                                  TIMESTAMP                                          COMMENT ''
 ,end_date                                    TIMESTAMP                                          COMMENT ''
 ,bonus_option                                VARCHAR(250)                                       COMMENT ''
 ,bonus_value                                 DOUBLE(4, 2)                                       COMMENT ''
 ,redirect_url                                TEXT                                               COMMENT ''
 ,do_not_redirect_to_mobile_app_landing_page  INT                                                COMMENT ''
 ,extra_fields                                VARCHAR(250)                                       COMMENT ''
 ,referral_api_type                           VARCHAR(250)                                       COMMENT ''
 ,referral_api_data                           TEXT                                               COMMENT ''
 ,activity                                    VARCHAR(250)                                       COMMENT ''
 ,banner                                      VARCHAR(250)                                       COMMENT ''
 ,placement                                   VARCHAR(250)                                       COMMENT ''
 ,reg_source_l3                               INT                                                COMMENT ''
 ,stg_load_date                               TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                  INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_referral_campaign_id)
 ,INDEX idx_user_id (referral_campaign_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_referral_campaigns (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;