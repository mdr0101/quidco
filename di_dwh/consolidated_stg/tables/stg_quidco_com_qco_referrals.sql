DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_referrals;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_referrals (
  stg_referrals_id    INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,referral_id         INT                                                COMMENT ''
 ,referral_date       TIMESTAMP                                          COMMENT ''
 ,user_id             INT                                                COMMENT ''
 ,campaign_id         INT                                                COMMENT ''
 ,custom_1            VARCHAR(250)                                       COMMENT ''
 ,custom_2            VARCHAR(250)                                       COMMENT ''
 ,banner              VARCHAR(250)                                       COMMENT ''
 ,is_recorded         INT                                                COMMENT ''
 ,extra_fields        VARCHAR(250)                                       COMMENT ''
 ,unique_id           VARCHAR(250)                                       COMMENT ''
 ,stg_load_date       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id          INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_referrals_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_referral_id (referral_id)
 ,INDEX idx_user_id (user_id)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_referrals (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
