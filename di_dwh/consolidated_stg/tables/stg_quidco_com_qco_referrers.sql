DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_referrers;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_referrers (
  stg_referrer_id    INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,referrer_id        INT                                                COMMENT ''
 ,referrer           VARCHAR(250)                                       COMMENT ''  
 ,api_client_id      INT                                                COMMENT ''
 ,email_logo         VARCHAR(250)                                       COMMENT ''
 ,reg_source_l3      INT                                                COMMENT ''
 ,stg_load_date      TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id         INT SIGNED    	   NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_referrer_id)
 ,INDEX idx_referrer_id (referrer_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_referrers (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;