DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_rtv_cards;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_rtv_cards (
  stg_rtv_card_id                     INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,card_id                             INT                                                COMMENT ''
 ,user_id                             INT                                                COMMENT ''
 ,recorded                            DATETIME                                           COMMENT ''
 ,activation_email                    TIMESTAMP                                          COMMENT ''
 ,birdback_token                      VARCHAR(500)                                       COMMENT ''
 ,card_created_at                   	TIMESTAMP                                          COMMENT ''
 ,pan_card_id	                        INT                                                COMMENT ''
 ,nickname	                          VARCHAR(500)                                       COMMENT ''
 ,expiry                              VARCHAR(500)                                       COMMENT ''
 ,card_type	                          VARCHAR(500)                                       COMMENT ''
 ,start_digits                        VARCHAR(500)                                       COMMENT ''
 ,last_digits	                        VARCHAR(500)                                       COMMENT ''
 ,stg_load_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                          INT SIGNED        NOT NULL                         COMMENT 'ETL job ID'
 ,PRIMARY KEY (stg_rtv_card_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_stg_job_id (job_run_id)
) COMMENT = 'Staging table for IS_CARD_LINKED column in DI_DIM_USER table from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;