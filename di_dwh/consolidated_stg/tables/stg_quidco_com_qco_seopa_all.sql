DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_seopa_all;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_seopa_all (
  stg_seopa_all_id                		  INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,quidco_user_id                          INT		                                         COMMENT ''
 ,birth_date                              DATETIME                                           COMMENT ''
 ,postcode                                varchar(10)                                        COMMENT ''
 ,no_of_kids                              INT                                                COMMENT ''
 ,quote_date							  DATETIME											 COMMENT '' 
 ,stg_load_date                           TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                              INT SIGNED        NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_seopa_all_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_seopa_all (job_run_id)
) COMMENT = 'Staging table for the Seopa demographics - car, home, travel from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;