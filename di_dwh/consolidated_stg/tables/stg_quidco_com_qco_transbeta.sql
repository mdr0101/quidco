DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_transbeta;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_transbeta (
  stg_transbeta_id                INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,transaction_id                  INT      UNSIGNED                                  COMMENT ''
 ,transaction_sequence            INT                                                COMMENT ''
 ,network                         VARCHAR(250)                                       COMMENT ''
 ,network_transaction_id          VARCHAR(250)                                       COMMENT ''
 ,merchant_id                     INT                                                COMMENT ''
 ,network_merchant_id             VARCHAR(250)                                       COMMENT ''
 ,network_reference               VARCHAR(300)                                       COMMENT ''
 ,user_id                         INT      UNSIGNED                                  COMMENT ''
 ,click_id                        INT      UNSIGNED                                  COMMENT ''
 ,date                            DATETIME                                           COMMENT ''
 ,amount                          DECIMAL(10, 2)                                     COMMENT ''
 ,commission                      DECIMAL(10, 2)                                     COMMENT ''
 ,user_commission                 DECIMAL(10, 2)                                     COMMENT ''
 ,status                          VARCHAR(250)                                       COMMENT ''
 ,payments_out_status             VARCHAR(250)                                       COMMENT ''
 ,import_type                     VARCHAR(250)                                       COMMENT ''
 ,import_rule_id                  INT                                                COMMENT ''
 ,import_event_id                 INT      UNSIGNED                                  COMMENT ''
 ,payment_out_id                  INT      UNSIGNED                                  COMMENT ''
 ,imported                        VARCHAR(250)                                       COMMENT ''
 ,post_cutoff                     VARCHAR(250)                                       COMMENT ''
 ,created                         DATETIME                                           COMMENT ''
 ,last_imported                   DATETIME                                           COMMENT ''
 ,copy_to_state                   VARCHAR(250)                                       COMMENT ''
 ,last_modified                   DATETIME                                           COMMENT ''
 ,enquiry_id                      INT                                                COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_transbeta_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_transbeta (job_run_id)
) COMMENT = 'Staging table for transaction/purchase fact from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;