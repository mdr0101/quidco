DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_user_demographics;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_user_demographics (
  stg_user_demographics_id                INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_id                                 VARCHAR(250)                                       COMMENT ''
 ,post_code                               VARCHAR(250)                                       COMMENT ''
 ,gender                                  VARCHAR(250)                                       COMMENT ''
 ,birth_date                              DATE                                               COMMENT ''
 ,auto_insurance_renewal                  INT                                                COMMENT ''
 ,home_insurance_renewal                  INT                                                COMMENT ''
 ,mobile_contract_renewal                 INT                                                COMMENT ''
 ,auto_insurance_renewal_day              INT                                                COMMENT ''
 ,home_insurance_renewal_day              INT                                                COMMENT ''
 ,mobile_contract_renewal_day             INT                                                COMMENT ''
 ,auto_insurance_renewal_last_modified    DATETIME                                           COMMENT ''
 ,home_insurance_renewal_last_modified    DATETIME                                           COMMENT ''
 ,mobile_contract_renewal_last_modified   DATETIME                                           COMMENT ''
 ,seopa_price_comparison                  VARCHAR(250)                                       COMMENT ''
 ,registration_source                     VARCHAR(250)                                       COMMENT ''
 ,registration_sub_source                 VARCHAR(250)                                       COMMENT ''
 ,join_prize_draw                         INT                                                COMMENT ''
 ,join_source                             INT                                                COMMENT ''
 ,mixpanel_id                             VARCHAR(250)                                       COMMENT ''
 ,join_page_version                       VARCHAR(250)                                       COMMENT ''
 ,stg_load_date                           TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                              INT SIGNED        NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_user_demographics_id)
 ,INDEX idx_user_id (user_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_user_demographics (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
