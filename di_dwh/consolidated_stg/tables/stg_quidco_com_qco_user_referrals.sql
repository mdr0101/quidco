DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_user_referrals;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_user_referrals (
  stg_user_referrals_id           INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_referral_id                INT                                                COMMENT ''
 ,user_referral_campaign_id       INT                                                COMMENT ''
 ,user_id                         BIGINT                                             COMMENT ''
 ,referred_user_id                INT                                                COMMENT ''
 ,merchant_id                     INT                                                COMMENT ''
 ,referral_type                   VARCHAR(250)                                       COMMENT ''
 ,banner_id                       INT                                                COMMENT ''
 ,transaction_id                  INT                                                COMMENT ''
 ,referred                        TIMESTAMP                                          COMMENT ''
 ,reminded                        INT                                                COMMENT ''
 ,unauth_user_id                  INT                                                COMMENT ''
 ,validated                       TIMESTAMP                                          COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_user_referrals_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_user_referral_id (user_referral_id)
 ,INDEX idx_referred_user_id (referred_user_id)
 ,INDEX idx_unauth_user_id (unauth_user_id)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_user_referrals (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
