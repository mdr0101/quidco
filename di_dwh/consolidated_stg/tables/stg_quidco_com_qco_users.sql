DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users (
  stg_user_id                     INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_id                         INT                                                COMMENT ''
 ,first_name                      VARCHAR(250)                                       COMMENT ''
 ,last_name                       VARCHAR(250)                                       COMMENT ''
 ,username                        VARCHAR(250)                                       COMMENT ''
 ,email                           VARCHAR(250)                                       COMMENT ''
 ,password                        VARCHAR(250)                                       COMMENT ''
 ,password_hash                   VARCHAR(250)                                       COMMENT ''
 ,user_type                       INT                                                COMMENT ''
 ,register_date                   TIMESTAMP                                          COMMENT ''
 ,hide_gambling                   VARCHAR(250)                                       COMMENT ''
 ,hide_adult                      VARCHAR(250)                                       COMMENT ''
 ,chat_privileges                 VARCHAR(250)                                       COMMENT ''
 ,last_modified                   TIMESTAMP                                          COMMENT ''
 ,accept_terms                    INT                                                COMMENT ''
 ,third_party_cookies             INT                                                COMMENT ''
 ,assistant_complete_stage        VARCHAR(250)                                       COMMENT ''
 ,vip                             INT                                                COMMENT ''
 ,non_assistant                   VARCHAR(250)                                       COMMENT ''
 ,facebook_linked                 INT                                                COMMENT ''
 ,facebook_email                  VARCHAR(250)                                       COMMENT ''
 ,has_clicked                     INT                                                COMMENT ''
 ,phone                           VARCHAR(250)                                       COMMENT ''
 ,premium_autorenewal             INT                                                COMMENT ''
 ,quidco_opinions_status          VARCHAR(250)                                       COMMENT ''
 ,quidco_opinions_register_date   TIMESTAMP   NULL DEFAULT NULL                      COMMENT ''
 ,facebook_id                     BIGINT                                             COMMENT ''
 ,is_active                       INT                                                COMMENT ''
 ,mobile                          VARCHAR(250)                                       COMMENT ''
 ,paypal_linked                   INT                                                COMMENT ''
 ,paypal_id                       VARCHAR(250)                                       COMMENT ''
 ,paypal_primary_email            VARCHAR(250)                                       COMMENT ''
 ,cint_register_date              TIMESTAMP   NULL DEFAULT NULL                      COMMENT ''
 ,cint_panelist_url               VARCHAR(250)                                       COMMENT ''
 ,cint_panelist_id                INT                                                COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_user_id)
 ,INDEX idx_user_id (user_id)
 ,INDEX idx_last_modified (last_modified)
 ,INDEX idx_register_date (register_date)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_users (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;
