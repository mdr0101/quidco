DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users_access_rights;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users_access_rights (
  stg_qco_users_access_rights_id  INT               NOT NULL AUTO_INCREMENT         COMMENT 'Primary Key/Surrogate Key'
 ,user_id                         INT UNSIGNED                                      COMMENT ''
 ,block                           TINYINT(4)                                        COMMENT ''
 ,block_date                      TIMESTAMP                                         COMMENT ''
 ,block_type                      VARCHAR(100)                                      COMMENT ''
 ,do_not_pay                      VARCHAR(100)                                      COMMENT ''
 ,do_not_pay_date                 TIMESTAMP                                         COMMENT ''
 ,send_email                      TINYINT(4)                                        COMMENT ''
 ,admin_notes                     TEXT                                              COMMENT ''
 ,deleted                         TINYINT(4)                                        COMMENT ''
 ,deleted_date                    DATETIME                                          COMMENT ''
 ,blocked_by_user                 INT                                               COMMENT ''
 ,blocked_by_admin                INT                                               COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP       COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED        NOT NULL                        COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_qco_users_access_rights_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_users_access_rights(job_run_id)
) COMMENT = 'Staging table for User Access Rights for IS_MAILABLE into DI_DIM_USER(_HIST) tables'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;