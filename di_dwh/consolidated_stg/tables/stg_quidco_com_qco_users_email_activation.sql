DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users_email_activation;

 
 CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users_email_activation (
  stg_user_email_activation_id    INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_id                         INT                                                COMMENT ''
 ,unauthenticated_email           VARCHAR(250)     NOT NULL                          COMMENT ''
 ,activation                      VARCHAR(250)                                       COMMENT ''
 ,reminder_1_date                 TIMESTAMP      NULL DEFAULT NULL                   COMMENT ''
 ,reminder_2_date                 TIMESTAMP      NULL DEFAULT NULL                   COMMENT ''
 ,reminder_3_date                 TIMESTAMP      NULL DEFAULT NULL                   COMMENT ''
 ,original_activation             VARCHAR(250)                                       COMMENT ''
 ,activated_date                  TIMESTAMP      NULL DEFAULT NULL                   COMMENT ''
 ,reset_password_date             TIMESTAMP      NOT NULL DEFAULT '0000-00-00 00:00:00'         COMMENT ''
 ,reset_password_control          VARCHAR(500)                                       COMMENT ''
 ,stg_load_date                   TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                      INT SIGNED    	NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_user_email_activation_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_users_email_activation (job_run_id)
) COMMENT = 'Staging table for the user dimension from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;