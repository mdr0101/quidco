DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users_visits;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_qco_users_visits (
  stg_users_visits_id                 INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,user_id                             INT                                                COMMENT ''
 ,last_visit                          TIMESTAMP                                          COMMENT ''
 ,agent_id                            INT                                                COMMENT ''
 ,stg_load_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                          INT SIGNED        NOT NULL                         COMMENT 'ETL job ID'
 ,PRIMARY KEY (stg_users_visits_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_stg_job_id (job_run_id)
) COMMENT = 'Staging table for LAST_VISIT_DATE_ID column in DI_DIM_USER table from quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;