DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_scientia_name_address_quidco;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_quidco_com_scientia_name_address_quidco (
   stg_scientia_name_address_id 					INT(11) 			NOT NULL AUTO_INCREMENT 						COMMENT 'Primary Key/Surrogate Key'
  ,client_urn 										varchar(200) 		NOT NULL
  ,user_id 											int(11) 			DEFAULT NULL
  ,postcode 										varchar(200) 		DEFAULT NULL
  ,region 											varchar(200) 		DEFAULT NULL
  ,tv_region_border 								tinyint(4) 			DEFAULT NULL
  ,tv_region_scottish 								tinyint(4) 			DEFAULT NULL
  ,tv_region_anglia 								tinyint(4) 			DEFAULT NULL
  ,tv_region_granada 								tinyint(4) 			DEFAULT NULL
  ,tv_region_carlton 								tinyint(4) 			DEFAULT NULL
  ,tv_region_central 								tinyint(4) 			DEFAULT NULL
  ,tv_region_tynetees 								tinyint(4) 			DEFAULT NULL
  ,tv_region_grampian 								tinyint(4) 			DEFAULT NULL
  ,tv_region_westctry 								tinyint(4) 			DEFAULT NULL
  ,tv_region_meridien 								tinyint(4) 			DEFAULT NULL
  ,tv_region_htv 									tinyint(4) 			DEFAULT NULL
  ,tv_region_yorks 									tinyint(4) 			DEFAULT NULL
  ,tv_region_ulster 								tinyint(4) 			DEFAULT NULL
  ,stg_load_date 									timestamp 			NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Row creation date'
  ,job_run_id 										int(11) 			NOT NULL COMMENT 'Modified to reference di_etl_job_log'
  ,PRIMARY KEY (stg_scientia_name_address_id)
  ,KEY idx_stg_load_date(stg_load_date)
  ,KEY idx_job_run_id_stg_quidco_com_scientia_demographics (job_run_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Staging table for Scentia demographics from quidco.com domain'
;