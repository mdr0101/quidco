DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_content_networks;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_content_networks (
  stg_network_id                            INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,id                                        INT UNSIGNED                                       COMMENT ''
 ,domain_id                                 INT UNSIGNED                                       COMMENT ''
 ,tracking_id                               VARCHAR(500)                                       COMMENT ''
 ,name                                      VARCHAR(500)                                       COMMENT ''
 ,last_import_at                            DATETIME                                           COMMENT ''
 ,created_at                                TIMESTAMP                                          COMMENT ''
 ,updated_at                                TIMESTAMP                                          COMMENT ''
 ,deleted_at                                TIMESTAMP         NULL DEFAULT NULL                COMMENT ''
 ,stg_load_date                             TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                                INT SIGNED    	  NOT NULL                         COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_network_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_quidco_com_qco_networks (job_run_id)
) COMMENT = 'Staging table for the network dimension from the shoop.fr domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;