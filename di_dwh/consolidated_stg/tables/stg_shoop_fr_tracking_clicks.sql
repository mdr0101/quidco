DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_tracking_clicks;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_tracking_clicks (
  stg_tracking_clicks_id		INT(10) 			UNSIGNED NOT NULL AUTO_INCREMENT						COMMENT 'Primary Key/Surrogate Key'
 ,id							INT(10)				UNSIGNED												COMMENT ''
 ,domain_id						INT(10) 																	COMMENT ''
 ,user_id 						INT(10)																		COMMENT ''
 ,merchant_id					INT(10)																		COMMENT ''
 ,program_id					INT(10)																		COMMENT ''
 ,ip							VARCHAR(32)																	COMMENT ''
 ,created_at					TIMESTAMP																	COMMENT ''
 ,stg_load_date					TIMESTAMP			DEFAULT CURRENT_TIMESTAMP								COMMENT 'Row creation date'
 ,job_run_id					INT SIGNED			NOT NULL												COMMENT 'Talend job_run_id'
 ,PRIMARY KEY (stg_tracking_clicks_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_shoop_fr_tracking_clicks (job_run_id)
) COMMENT = 'Staging table for the clicks dimension from the shoop.fr domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;