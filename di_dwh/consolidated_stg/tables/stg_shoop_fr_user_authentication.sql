DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_authentication;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_authentication (
  stg_user_authentication_id          INT               NOT NULL AUTO_INCREMENT          COMMENT 'Primary Key/Surrogate Key'
 ,email                               VARCHAR(500)                                       COMMENT ''
 ,type                                VARCHAR(500)                                       COMMENT ''
 ,email_authenticated_at              TIMESTAMP         NULL DEFAULT NULL                COMMENT ''
 ,stg_load_date                       TIMESTAMP         DEFAULT CURRENT_TIMESTAMP        COMMENT 'Row creation date'
 ,job_run_id                          INT SIGNED        NOT NULL                         COMMENT 'ETL job ID'
 ,PRIMARY KEY (stg_user_authentication_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_stg_job_id (job_run_id)
) COMMENT = 'Staging table for the DI_FACT_CLICK fact table from the quidco.com domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci;