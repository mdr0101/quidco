DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_profiles;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_profiles (
  stg_user_profiles_id			INT(10) 			UNSIGNED NOT NULL AUTO_INCREMENT						COMMENT 'Primary Key/Surrogate Key'
 ,user_id						INT(10)				UNSIGNED												COMMENT ''
 ,birth_date					DATE																		COMMENT ''
 ,post_code						VARCHAR(15)																	COMMENT ''
 ,gender						VARCHAR(15)																	COMMENT ''
 ,marketing_emails				TINYINT(1)																	COMMENT ''
 ,fraud_type					VARCHAR(15)																	COMMENT ''
 ,fraud_score_type				VARCHAR(15)																	COMMENT ''
 ,fraud_score					INT(11)																		COMMENT ''
 ,do_not_pay					TINYINT(1)																	COMMENT ''
 ,stg_load_date					TIMESTAMP			DEFAULT CURRENT_TIMESTAMP								COMMENT 'Row creation date'
 ,job_run_id					INT SIGNED			NOT NULL												COMMENT 'Talend job_run_id'
 ,PRIMARY KEY (stg_user_profiles_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_shoop_fr_user_profiles (job_run_id)
) COMMENT = 'Staging table for the user dimension from the shoop.fr domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;