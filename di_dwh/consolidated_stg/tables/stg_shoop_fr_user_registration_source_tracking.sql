DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_registration_source_tracking;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_registration_source_tracking (
  stg_user_registration_source_tracking_id			INT(10) 			UNSIGNED NOT NULL AUTO_INCREMENT						COMMENT 'Primary Key/Surrogate Key'
 ,id												INT(10) 			UNSIGNED												COMMENT ''
 ,user_id											INT(10)				UNSIGNED												COMMENT ''
 ,utm_cid											VARCHAR(255)																COMMENT ''
 ,utm_source										VARCHAR(255)																COMMENT ''
 ,utm_medium										VARCHAR(255)																COMMENT ''
 ,utm_campaign										VARCHAR(255)																COMMENT ''
 ,utm_term											VARCHAR(255)																COMMENT ''
 ,utm_content										VARCHAR(255)																COMMENT ''  
 ,stg_load_date					TIMESTAMP			DEFAULT CURRENT_TIMESTAMP								COMMENT 'Row creation date'
 ,job_run_id					INT SIGNED			NOT NULL												COMMENT 'Talend job_run_id'
 ,PRIMARY KEY (stg_user_registration_source_tracking_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_shoop_fr_user_registration_source_tracking (job_run_id)
) COMMENT = 'Staging table for the user dimension from the shoop.fr domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;