/* Creating stage table for user_users: stg_shoop_fr_user_users */
DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_users;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_shoop_fr_user_users (
  stg_user_users_id				INT(10) 			UNSIGNED NOT NULL AUTO_INCREMENT						COMMENT 'Primary Key/Surrogate Key'
 ,id							INT(10)				UNSIGNED												COMMENT ''
 ,domain_id						INT(10) 																	COMMENT ''
 ,email							VARCHAR(100)																COMMENT ''
 ,username						VARCHAR(100)																COMMENT ''
 ,password						VARCHAR(255)																COMMENT ''
 ,first_name					VARCHAR(50)																	COMMENT ''
 ,last_name						VARCHAR(50)																	COMMENT ''
 ,account_type					VARCHAR(25)																	COMMENT ''
 ,created_at					TIMESTAMP																	COMMENT ''
 ,updated_at					TIMESTAMP																	COMMENT ''
 ,deleted_at					TIMESTAMP																	COMMENT ''
 ,status						VARCHAR(25)																	COMMENT ''
 ,last_visit					DATETIME																	COMMENT ''
 ,stg_load_date					TIMESTAMP			DEFAULT CURRENT_TIMESTAMP								COMMENT 'Row creation date'
 ,job_run_id					INT SIGNED			NOT NULL												COMMENT 'Talend job_run_id'
 ,PRIMARY KEY (stg_user_users_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_shoop_fr_user_users (job_run_id)
) COMMENT = 'Staging table for the users dimension from the shoop.fr domain'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
 ;