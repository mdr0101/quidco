DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_wallet_user_balances;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_wallet_user_balances (
  stg_id                    INT               				NOT NULL AUTO_INCREMENT         COMMENT 'Primary Key/Surrogate Key'  
 ,id 						INT(10) 		UNSIGNED										COMMENT ''
 ,domain_id 				INT(10) 		UNSIGNED										COMMENT ''
 ,user_id 					INT(10) 		UNSIGNED										COMMENT ''
 ,updated_at 				TIMESTAMP 														COMMENT ''
 ,balance 					DECIMAL(10,2) 													COMMENT ''
 ,stg_load_date             TIMESTAMP         				DEFAULT CURRENT_TIMESTAMP       COMMENT 'Row creation date'
 ,job_run_id                INT SIGNED    	  				NOT NULL                        COMMENT 'Modified to reference di_etl_job_log'
 ,PRIMARY KEY (stg_id)
 ,INDEX idx_stg_load_date (stg_load_date)
 ,INDEX idx_job_run_id_stg_wallet_user_balances (job_run_id)
) COMMENT = 'Staging table for user groups from the Groups server'
 ,ENGINE=InnoDB
 ,CHARACTER SET utf8 COLLATE utf8_general_ci
;