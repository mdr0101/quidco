DROP TABLE IF EXISTS ${DB_DI_DWH_STG_SCHEMA}.stg_wallet_user_ledgers;
CREATE TABLE ${DB_DI_DWH_STG_SCHEMA}.stg_wallet_user_ledgers (
  stg_user_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key/Surrogate Key',
  id int(10) unsigned NOT NULL,
  domain_id int(10) unsigned NOT NULL,
  user_id int(10) unsigned NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  reference_code varchar(10) NOT NULL,
  reference_identifier varchar(50) NOT NULL,
  debit decimal(10,2) DEFAULT NULL,
  credit decimal(10,2) DEFAULT NULL,
  balance decimal(10,2) NOT NULL,
  description varchar(200) DEFAULT NULL,
  is_proprietary tinyint(3) unsigned NOT NULL DEFAULT '0',
  stg_load_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Row creation date',
  job_run_id int(11) NOT NULL COMMENT 'Modified to reference di_etl_job_log',
  PRIMARY KEY (stg_user_id),
  INDEX idx_id (id),
  INDEX idx_domain_id (domain_id),
  INDEX idx_user_id (user_id),
  INDEX idx_stg_load_date (stg_load_date),
  INDEX idx_job_run_id_stg_wallet_user_ledgers (job_run_id)
)COMMENT='Staging table for the Wallet User Ledger for all domains'
  ,ENGINE=InnoDB
  ,CHARACTER SET utf8 COLLATE utf8_general_ci;
  
  
  