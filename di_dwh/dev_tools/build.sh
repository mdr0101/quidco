#!/bin/bash

VCS_ROOT=../
MYSQL=mysql
AWK=awk
GREP=grep

# Usage function
function usage {
    echo "------------------------------"
    echo "DI Data Warehouse build script"
    echo "------------------------------"
    echo "Usage:"
    echo "   build.sh -c <config.properties path>"
}

# Log function
function log {
LOG_LEVEL=$1
MESSAGE=$2

    echo `date +"%Y-%m-%d %H:%M:%S,%3N"`: [$LOG_LEVEL] - $MESSAGE
}

# Call it when build success
function build_success {
    log INFO "Build finished successfully."
    exit 0
}

# Call it when build failed
function build_failed {
    log ERROR "Build failed."
    exit 1
}

function drop_tables_in_schema {
HOST=$1
PORT=$2
USER=$3
PASSWORD=$4
SCHEMA=$5

    TABLES=$($MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e 'show tables' | $AWK '{ print $1}' | $GREP -v '^Tables' )

    for t in $TABLES
    do
        echo "   Deleting $t table from $SCHEMA schema..."
        $MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "drop table $t"
        RET_VAL=$?

        if [ $RET_VAL -ne 0 ]; then
            log ERROR "Table failed to drop. [$RET_VAL]"
            build_failed
        fi
    done
}

function drop_views_in_schema {
HOST=$1
PORT=$2
USER=$3
PASSWORD=$4
SCHEMA=$5

    VIEWS=$($MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "select table_name from information_schema.views where table_schema = '$SCHEMA'" | $AWK '{ print $1}' | $GREP -v 'name' )

    for t in $TABLES
    do
        echo "   Deleting $t view from $SCHEMA schema..."
        $MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "drop view $t"
        RET_VAL=$?

        if [ $RET_VAL -ne 0 ]; then
            log ERROR "View failed to drop. [$RET_VAL]"
            build_failed
        fi
    done
}

function drop_procedures_in_schema {
HOST=$1
PORT=$2
USER=$3
PASSWORD=$4
SCHEMA=$5

    echo "Deleting every procedure in $SCHEMA schema..." 
 
    PROCEDURES=$($MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "select name from mysql.proc where type = 'PROCEDURE' and db = '$SCHEMA'" | $AWK '{ print $1}' | $GREP -v 'name' )

    for p in $PROCEDURES
    do
        echo "   Deleting $p procedure from $SCHEMA schema..."
        $MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "drop procedure $p"
        RET_VAL=$?

        if [ $RET_VAL -ne 0 ]; then
            log ERROR "Procedure failed to drop. [$RET_VAL]"
            build_failed
        fi
    done
}

function drop_functions_in_schema {
HOST=$1
PORT=$2
USER=$3
PASSWORD=$4
SCHEMA=$5

    echo "Deleting every function in $SCHEMA schema..."  
 
    FUNCTIONS=$($MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "select name from mysql.proc where type = 'FUNCTION' and db = '$SCHEMA'"| $AWK '{ print $1}' | $GREP -v 'name' )

    for f in $FUNCTIONS
    do
        echo "   Deleting $f function from $SCHEMA schema..."
        $MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA -e "drop function $f"
        RET_VAL=$?
    
        if [ $RET_VAL -ne 0 ]; then
            log ERROR "Function failed to drop. [$RET_VAL]"
            build_failed
        fi
    done
}

function clean_schema {
REF_SCHEMA=$1

    case "$REF_SCHEMA" in
        STAGING)
            HOST=$DB_DI_DWH_STG_HOST
            PORT=$DB_DI_DWH_STG_PORT
            USER=$DB_DI_DWH_STG_USER
            PASSWORD=$DB_DI_DWH_STG_PASSWORD
            SCHEMA=$DB_DI_DWH_STG_SCHEMA
            ;;

        WAREHOUSE)
            HOST=$DB_DI_DWH_DWH_HOST
            PORT=$DB_DI_DWH_DWH_PORT
            USER=$DB_DI_DWH_DWH_USER
            PASSWORD=$DB_DI_DWH_DWH_PASSWORD
            SCHEMA=$DB_DI_DWH_DWH_SCHEMA
            ;;

        *) log ERROR "Invalid schema to run SQL [$SCHEMA]"
            build_failed
            ;;
    esac

    log INFO "Target schema to clean is $REF_SCHEMA"
    log INFO "HOST:     [$HOST]"
    log INFO "PORT:     [$PORT]"
    log INFO "USER:     [$USER]"
    log INFO "PASSWORD: [*******************]"
    log INFO "SCHEMA:   [$SCHEMA]"

    drop_tables_in_schema $HOST $PORT $USER $PASSWORD $SCHEMA
    drop_views_in_schema $HOST $PORT $USER $PASSWORD $SCHEMA
    drop_procedures_in_schema $HOST $PORT $USER $PASSWORD $SCHEMA
    drop_functions_in_schema $HOST $PORT $USER $PASSWORD $SCHEMA
}

# Execute sql file
function run_sql_file {
REF_SCHEMA=$1
SQL_FILE=$2

    case "$REF_SCHEMA" in
        STAGING)
            HOST=$DB_DI_DWH_STG_HOST
            PORT=$DB_DI_DWH_STG_PORT
            USER=$DB_DI_DWH_STG_USER
            PASSWORD=$DB_DI_DWH_STG_PASSWORD
            SCHEMA=$DB_DI_DWH_STG_SCHEMA
            ;;

        WAREHOUSE)
            HOST=$DB_DI_DWH_DWH_HOST
            PORT=$DB_DI_DWH_DWH_PORT
            USER=$DB_DI_DWH_DWH_USER
            PASSWORD=$DB_DI_DWH_DWH_PASSWORD
            SCHEMA=$DB_DI_DWH_DWH_SCHEMA
            ;;

        *) log ERROR "Invalid schema to run SQL [$SCHEMA]"
            build_failed
            ;;
    esac

    log INFO "Target schema is $REF_SCHEMA"
    log INFO "HOST:     [$HOST]"
    log INFO "PORT:     [$PORT]"
    log INFO "USER:     [$USER]"
    log INFO "PASSWORD: [*******************]"
    log INFO "SCHEMA:   [$SCHEMA]"


    BUILD_SQL_FILE=/tmp/di_build.sql

    # Replace schema variables
    sed    s/\${DB_DI_DWH_STG_SCHEMA}/$DB_DI_DWH_STG_SCHEMA/g $SQL_FILE > $BUILD_SQL_FILE
    sed -i s/\${DB_DI_DWH_DWH_SCHEMA}/$DB_DI_DWH_DWH_SCHEMA/g $BUILD_SQL_FILE

    # $MYSQL --protocol=TCP --host=$HOST --port=$PORT --user=$USER --password=$PASSWORD $SCHEMA < $BUILD_SQL_FILE
    $MYSQL --host=$HOST --user=$USER --password=$PASSWORD $SCHEMA < $BUILD_SQL_FILE
    
    RET_VAL=$?
    
    if [ $RET_VAL -ne 0 ]; then
        log ERROR "SQL script failed. [$RET_VAL]"
        cat $BUILD_SQL_FILE
        echo

        build_failed
    fi
}

# Execute every file from a directory with .sql extension 
function run_sql_files_from_dir {
REF_SCHEMA=$1
SQL_FILES_DIR=$2

    for SQL_FILE in `ls ${SQL_FILES_DIR}/*.sql`; do
        log INFO "Executing sql file: $SQL_FILE"
        run_sql_file $REF_SCHEMA $SQL_FILE
    done
}

# Parse command line arguments
while getopts ":c:" opt; do
    case $opt in
        c) CONFIG_PROPERTIES=$OPTARG
        ;;
    \?)
        log ERROR "Invalid option: -$OPTARG"
        usage
        build_failed
        ;;
    esac
done

# Check if the config file is not empty
if [ -z $CONFIG_PROPERTIES ]; then
    log ERROR "Config file is not defined"
    usage
    build_failed
fi

# Check if the config file exists
if [ ! -s $CONFIG_PROPERTIES ]; then
        log ERROR "Config file not found or empty [$CONFIG_PROPERTIES]"
        usage
        build_failed
fi

# Load every property from the config file
source $CONFIG_PROPERTIES
log INFO "Config file has been loaded at $CONFIG_PROPERTIES"



# ------------------------------------------------------------------------------
# Step 1 Clean up Staging schema
# ------------------------------------------------------------------------------
#log INFO "Cleaning Staging schema..."
#clean_schema STAGING

# ------------------------------------------------------------------------------
# Step 2 Clean up the Data Warehouse schema
# ------------------------------------------------------------------------------
#log INFO "Cleaning Warehouse schema..."
#clean_schema WAREHOUSE

# ------------------------------------------------------------------------------
# Step 3 Deploy every object into the Staging schema
# ------------------------------------------------------------------------------
log INFO "Create tables in Staging schema..."
run_sql_files_from_dir STAGING di_dwh/consolidated_stg/tables

log INFO "Create stored procedures in Staging schema..."
run_sql_files_from_dir STAGING di_dwh/consolidated_stg/stored_procedures

# ------------------------------------------------------------------------------
# Step 4 Deploy every object into the Data Warehouse schema
# ------------------------------------------------------------------------------
log INFO "Create tables in Warehouse schema..."
run_sql_files_from_dir WAREHOUSE di_dwh/consolidated_dwh/tables

log INFO "Create views in Warehouse schema..."
run_sql_files_from_dir WAREHOUSE di_dwh/consolidated_dwh/views

log INFO "Create stored procedures in Warehouse schema..."
run_sql_files_from_dir WAREHOUSE di_dwh/consolidated_dwh/stored_procedures

log INFO "Load static dataset..."
run_sql_files_from_dir WAREHOUSE di_dwh/consolidated_dwh/static_dataset
# $VCS_ROOT

# Exit as build success
build_success


