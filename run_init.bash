service mysql start
#mysql -h localhost -u root -e "CREATE DATABASE AV;"

mysql -h localhost -u root -e "CREATE USER di_dev_user@localhost IDENTIFIED BY 'P@ssword';"
mysql -h localhost -u root -e "GRANT ALL PRIVILEGES ON *.* TO di_dev_user@localhost;"

mysql -h localhost -u root -e "CREATE DATABASE di_staging; GRANT ALL PRIVILEGES ON di_staging.* to di_staging@localhost identified by 'di_staging';"
echo "Finished Creating di_staging DATABASE."

mysql -h localhost -u root -e "CREATE DATABASE di_warehouse; GRANT ALL PRIVILEGES ON di_warehouse.* to di_staging@localhost identified by 'di_warehouse';"
echo "Finished Creating di_staging DATABASE."

mysql -h localhost -u root -e "SHOW DATABASES;"